<?php

/**
 * parsing of data for service format (utils)
 *
 * @author Pievis
 */
class ServiceView {

	static function get_billboard_types() {
		$params = [];
		$list = H::db()->selectView('id, nome, icona', 'tipo_cartello', $params)->listItems();
		return $list;
	}

	//alis: fornitore
	static function get_providers() {
		$params = [];
		$list = H::db()->selectView('id, nome, descrizione', 'fornitore', $params)->listItems();
		return $list;
	}

	//select based on your profile type
	static function get_customers($user) {
		$profile = $user['profile'];
		$customers = array();
		$params = [
		    'abilitato' => 1
		];
		switch ($profile) {
			case 1000:
			case 10:
				break;
			case 9:
				//added 03/11/16
				$params['id'] = H::db()->selectViewById('id_cliente', 'utente_cliente',
					$user['id'], 'id_utente')->listItemsByKey('id_cliente');
				break;
			case 8:
			case 6:
				$params['id'] = $user['id_cliente'];
				break;
		}
		$customers = H::db()->selectView('id, nome, logo', 'cliente', $params)->listItems();

		foreach ($customers as &$customer) {
			$customer['concorrenti'] = self::get_competitors($customer['id']);
			$customer['negozi'] = self::get_stores($user, $customer['id']);
			$customer['cartelli'] = self::get_billboards($user, $customer['id']);
		}

		return $customers;
	}

	static function get_billboards($user, $id_cliente) {
		$params = [
		    'c.recapito_id_provincia:_' => 'p.id',
//		    'c.abilitato:_' => 1
		];
		$params['c.id_cliente'] = $id_cliente;
		if ($user['profile'] == 1) {
			//select only a few stores
			$ids_negozio = H::db()->selectById('utente_negozio', $user['id'], 'id_utente')->listItemsByKey('id_negozio');
			$params['c.id_negozio'] = $ids_negozio;
		}

		$list = H::db()->selectView('c.*, p.nome as nome_provincia', 'cartello c, provincia p', $params)->listItems();
		H::lib('Formatter');
		foreach ($list as &$item) {
//			unset($item['abilitato']);
			if ($item['gallery']) {
				$item['gallery'] = unserialize($item['gallery']);
			}
			unset($item['num_contratto']);
			unset($item['data_inserimento']);
		}
		return $list;
	}

	//alias rivali, concorrente
	static function get_competitors($id_cliente) {
		$params = [
		    'c.recapito_id_provincia:_' => 'p.id'
		];

		$params['c.id_cliente'] = $id_cliente;
		$list = H::db()->selectView('c.id, c.nome, c.logo,'
				. 'c.descrizione,'
				. 'recapito_via, recapito_num_civico, recapito_comune, recapito_cap'
				. ', p.nome as nome_provincia, recapito_latitudine, recapito_longitudine', 'concorrente c, provincia p', $params)->listItems();

		return $list;
	}

	static function get_stores($user, $id_cliente) {

		$params = [
		    'n.recapito_id_provincia:_' => 'p.id'
		];

		$params['n.id_cliente'] = $id_cliente;
		if ($user['profile'] == 1) {
			//select only a few stores
			$ids_negozio = H::db()->selectById('utente_negozio', $user['id'], 'id_utente')->listItemsByKey('id_negozio');
			$params['n.id'] = $ids_negozio;
		}

		$list = H::db()->selectView('n.id, n.nome, n.logo,'
				. 'n.descrizione,'
				. 'recapito_via, recapito_num_civico, recapito_comune, recapito_cap'
				. ', p.nome as nome_provincia, recapito_latitudine, recapito_longitudine', 'negozio n, provincia p', $params)->listItems();

		//aggiungi i rispettivi competitors
		foreach ($list as &$item) {
			$concorrenti = H::db()->selectById('concorrente_negozio', $item['id'], 'id_negozio')->listItemsByKey('id_concorrente');
			$item['concorrenti'] = $concorrenti;
		}
		return $list;
	}

	//deprecated
	static function format_address(&$row) {
		if (!key_exists('nome_provincia', $row)) {
			$province = H::db()->selectOne('provincia', $row['recapito_id_provincia'])->item()['nome'];
		} else {
			$province = $row['nome_provincia'];
		}
		$address = array();
		$address['street'] = $row['recapito_via'];
		$address['civic'] = $row['recapito_num_civico'];
		$address['city'] = $row['recapito_comune'];
		$address['province'] = $province;
		$address['cap'] = $row['recapito_cap'];
		$row['address'] = $address;
		//clean
		unset($row['recapito_via']);
		unset($row['recapito_num_civico']);
		unset($row['recapito_cap']);
		unset($row['recapito_id_provincia']);
		unset($row['recapito_comune']);
		unset($row['nome_provincia']);
	}

	static function change_key(&$item, $key1, $key2) {
		if (key_exists($key1, $item)) {
			$item[$key2] = $item[$key1];
			unset($item[$key1]);
		}
	}

}
