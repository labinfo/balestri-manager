<?php
class Utils {
    
        //ritorna percorso alla cartella con i file csv per l'import
        public static function getSharedFileFolder(){
            return H::config('import_folder', '');
        }
        
        public static function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
        '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}
    
	public static function check_uso($id, &$list) {
		foreach ($list as $key => $value) {	
			$item = H::db()->selectOne($key, $id, $value)->item();
			if ($item != null) {
				return true;
			}
		}
		return false;
	}
	
	public static function check_download() {
		return null;
	}
	
	public static function &add_file() {
		$file = H::input('file', '');
		$file_name = H::input('file_name', '');
		if ($file == '') {
			$file = null;
			return $file;
		}
		if ($file_name == '') {
			$file = H::db()->selectOne('risorsa_file', $file)->item();
			if ($file != null) {
				$file = [
					'id' => $file['id'],
					'nome' => $file['nome'],
					'estensione' => $file['estensione'],
				];
			}
			return $file;
		}
		$php_info = pathinfo($file_name);
		$estensione = strtolower($php_info['extension']);
		
		$params = [
			'data_inserimento:_' => 'now()',
			'estensione' => $estensione,
			'nome' => $file_name,
		];
		$id = H::db()->insert('risorsa_file', $params);
		$new_file = HSystem::path('files', true) . '/' . $id;
		$path_file = HSystem::path('temp/file', true) . '/' . $file;
		if (!file_exists($path_file)) {
			$file = null;
			return $file;
		}
		rename($path_file, $new_file);	

		$file = [
			'id' => $id,
			'nome' => $file_name,
			'estensione' => $estensione,
		];
		return $file;
	}
	
	public static function add_sessionid_to_url($url) {
		if ($url == null) {
			$url = '';
		}
		if (HString::indexOf($url, '?') >= 0) {
			$url .= '&';
		}
		else {
			$url .= '?';
		}
		$sessionid = H::session()->sessionId();
		$password_check = H::config('check_password_back', '');
		$url .= 'sessionid=' . $sessionid .
				'&check=' . md5($sessionid . $password_check);
		return $url;
	}
	
	public static function update_settings($list, $extra = null) {
		$listItems = [];
		foreach($list as $key) {
			$listItems[$key] = H::input($key, '');
		}
		if ($extra != null) {
			foreach($extra as $key => $value) {
				$listItems[$key] = $value;
			}		
		}
		H::settings()->updateSettingsFile('*', $listItems);
	}
	
	public static function auth_violation() {
		$id = 0;
		Log::error('auth_violation', '-', $id);
		
		$params = [
				'messaggio' => 'Rilevato tentativo di utilizzo non autorizzato di un servizio da parte dell\'utente:',
				'utente' => H::session('user'),
				'activity' => H::context()->requestActivity()
			];
		H::mail()->send(H::config('email_notifiche_accessi'), 
				'tentativo uso servizio', 
				'notifica_super_accesso', 
				$params);
		
		H::forward('invalid-authentication');
	}
	
	public static function find_string(&$list, $max = 500) {
		if (is_array($list)) {
			$find = HString::normalize(implode(' ', $list));
		}
		else {
			$find = $list;
		}
		$find = implode(' ', array_unique(explode(' ', $find)));
		if (strlen($find) > $max) {
			$find = substr(0, $max);
		}
		return $find;
	}
	
	public static function no_carattere($str, $check) {
		for ($i = 0, $len = strlen($str); $i < $len; $i++) {
			if (HString::indexOf($check, $str{$i}) < 0) {
				return true;
			}
		}
		return false;
	}

	public static function is_carattere($str, $check) {
		for ($i = 0, $len = strlen($str); $i < $len; $i++) {
			if (HString::indexOf($check, $str{$i}) >= 0) {
				return true;
			}
		}
		return false;
	}

	public static function check_password($password) {
		if ($password == null || strlen($password) < 8) {
			return false;
		}
		
		if (!self::is_carattere($password, 'ABCDEFGHILMNOPQRSTUVZXWYJK') ||
			!self::is_carattere($password, 'abcdefghilmnopqrstuvzxwyjk') ||
			!self::is_carattere($password, '0123456789')) {
			return false;
		}
		return true;
	}
	
	public static function delete_entita($id_entita, $tipo_entita) {
		$qryList = [
			'DELETE FROM entita_gallery WHERE id_entita IN ([:id]) AND tipo_entita = :tipo_entita',
			'DELETE FROM entita_documento WHERE id_entita IN ([:id]) AND tipo_entita = :tipo_entita',
		];
		$params = [
			':id' => $id_entita,
			':tipo_entita' => $tipo_entita
		];
		H::db()->queryList($qryList, $params);
	}
	public static function delete_cartello($id) {
		if ($id == null || (is_array($id) && count($id) == 0)) {
			return;
		}
		Utils::delete_entita($id, 'cartello');
		
		$qryList = [
			'DELETE FROM cartello_preferito WHERE id_cartello IN ([:id])',
			'DELETE FROM cartello WHERE id IN ([:id])',
		];
		$params = [
			':id' => $id
		];
		H::db()->queryList($qryList, $params);
	}
}