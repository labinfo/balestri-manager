<?php

/**
 * Handles common operation of image data structure
 * */
class ImageManager {
    /*
      R40x30,
      R80x60,
      R160x120,
      R320x240,
      R640x480,
      R1280x960
     */

    const WWW_IMG_DIR = "/images/";

    public static $imageDim = array(
        '0' => ["width" => "320",
            "height" => "240",
            "type" => "4"
        ],
        '1' => ["width" => "80",
            "height" => "80",
            "type" => "1"
        ],
        '2' => ["width" => "640",
            "height" => "480"
        ],
        '3' => ["width" => "1024",
            "height" => "768"
        ],
        '4' => ["width" => "1280",
            "height" => "960"
        ],
        '5' => ["width" => "200",
            "height" => "200",
            "type" => "4"
        ]
    );
    public static $previewDimens = array(
        "1024x768",
        "320x240",
        "80x80"
    );

    public static function toImageArray($file, $alt = null) {
        $image = array();
        $image['alt'] = $alt;
        $image['file'] = $file;
        self::addPreviewUrl($image);
        return $image;
    }

    public static function addPreviewUrl(&$image) {
        $url = array();
        $file = $image['file'];
        for ($i = 0; $i < count(self::$previewDimens); $i++) {
            $url["p$i"] = self::WWW_IMG_DIR . self::$previewDimens[$i] . "/" . $file;
        }
        $image['url'] = $url;
    }

    public static function imageFormJson($json, $addPreviewUrl = false) {
        $image = json_decode($json, true);
        if ($addPreviewUrl) {
            self::addPreviewUrl($image);
        }
        return $image;
    }

    public static function toJson($image) {
        if (key_exists("url", $image)) {
            unset($image['url']);
        }
        return json_encode($image);
    }

}
