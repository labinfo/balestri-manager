<?php
class Formatter {	
	public static function dateFormat($date, $f) {
		if ($date == null || strtotime($date) == 0) {
			return '';
		}
		return date($f, strtotime($date));
	}
	
	public static function dateSimple($date) {
		if ($date == '0000-00-00') {
			return '';
		}
		return self::dateFormat($date, 'd-m-Y');
	}
	
	public static function dateTime($date) {
		return self::dateFormat($date, 'd-m-Y H:i');
	}
	
	public static function timeSimple($date) {
		return self::dateFormat($date, 'H:i');
	}
}