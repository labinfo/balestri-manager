<?php

class MyActivityFromRequest implements HActivityFromRequest
{
	public function parse($url)
	{
		$list = explode('/', $url);

		if (count($list) > 0) {
			switch ($list[0]) {
				case 'my':
					if (count($list) == 4 && $list[1] == 'img') {
						H::input()->put('dim', $list[2]);
						H::input()->put('image', $list[3]);
						return 'my/image';
					}
					if (count($list) == 5 && $list[1] == 'img') {
						H::input()->put('sessionid', $list[2]);
						H::input()->put('dim', $list[3]);
						H::input()->put('image', $list[4]);
						return 'my/image';
					}
					break;
				case 'reset-password':
					if (count($list) == 3) {
						$activity = $list[0];

						$ref_id = $list[1];
						$ref_token = $list[2];
						$c = new HCryptNumber();
						$id = $c->decode($ref_id);
						if ($id != null && strlen($ref_token) > 10) {
							$random = substr($ref_token, -10, 7);
							$random2 = substr($ref_token, -3);
							$check_token = sha1($activity . $id . $ref_id . H::config('password_enc', '') . $random) . $random . $random2;

							if ($ref_token == $check_token) {
								H::input()->put('ref_id', $ref_id);
								H::input()->put('ref_token', $ref_token);
								return $list[0];
							}
						}
					}
			}
		}

		return null;
	}
}