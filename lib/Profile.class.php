<?php

class Profile
{
    public static function update_user_profile($id)
    {
        $params = [
            'id_utente:>' => 1,
            'id_utente' => $id,
        ];
        H::db()->selectView('sessionid',
            'utente_session',
            $params)->
        listItems(function (&$row, &$list) {
            H::session()->invalidate($row['sessionid']);
        });
    }

    public static function is($key, $name)
    {
        return self::toKey($name) == $key;
    }

    public static function toKey($name)
    {
        switch ($name) {
            case 'super':
                return 1000;
            case 'admin':
                return 10;
            case 'operatore_clienti': //gestisce più clienti
                return 9;
            case 'operatore_cliente':    //operatore TOP (un solo cliente)
                return 8;
            case 'operatore':    //operatore zona
                return 6;
            case 'cliente':        //unused
                return 2;
            case 'cliente_semplice':    //unused
                return 1;
        }
        return 0;
    }

    public static function toName($key)
    {
        switch ($key) {
            case 1000:
                return 'super';
            case 10:
                return 'admin';
            case 9:
                return 'operatore_clienti';
            case 8:
                return 'operatore_cliente';
            case 6:
                return 'operatore';
            case 2:
                return 'cliente';
            case 1:
                return 'cliente_semplice';
        }
        return '';
    }
}