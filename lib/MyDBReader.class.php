<?php
H::lib('Formatter');

class MyDBReader implements HDBReader {
	public static function &get($type, &$row) {
		return self::$type($row);
	}
	public static function &log_risorsa(&$row) {
		return self::info($row);
	}
	public static function &tipo_cartello(&$row) {
		return $row;
	}	
	public static function &cliente(&$row) {
		return $row;
	}
	public static function &negozio(&$row) {
		return $row;
	}	
        public static function &concorrente(&$row) {
		return $row;
	}
	public static function &entita_gallery(&$row) {
		return $row;
	}
        public static function &fornitore(&$row) {
		return $row;
	}
	public static function &cartello(&$row) {
		$row['data_dal'] = Formatter::dateSimple($row['data_dal']);
		$row['data_al'] = Formatter::dateSimple($row['data_al']);
		$row['data_disinstallazione'] = Formatter::dateSimple($row['data_disinstallazione']);
		return $row;
	}
        //clean
        public static function &cartello_c(&$row) {
			$row['gallery'] = unserialize($row['gallery']);
			$row['data_dal'] = Formatter::dateSimple($row['data_dal']);
			$row['data_al'] = Formatter::dateSimple($row['data_al']);
		return $row;
	}
	public static function &richiesta_contatto(&$row) {
		$dettaglio = null;
		if (!is_null($row['dettaglio'])) {
			$dettaglio = unserialize($row['dettaglio']);
		}
		$row['dettaglio'] = $dettaglio;	
		$row['data_inserimento'] = Formatter::dateTime($row['data_inserimento']);
		$row['data_risposta'] = Formatter::dateTime($row['data_risposta']);
		return $row;
	}
	public static function &info(&$row) {
		$row['utente'] = unserialize($row['utente']);
		$row['data_operazione'] = Formatter::dateTime($row['data_operazione']);
		return $row;
	}	
	public static function &blocco_ip(&$row) {
		return self::ip($row);
	}
	public static function &ip(&$row) {
		$row['data_inserimento'] = Formatter::dateTime($row['data_inserimento']);
		return $row;
	}		
	public static function &entita_documento(&$row) {
		$row['data_inserimento'] = Formatter::dateTime($row['data_inserimento']);
		return $row;
	}	
	public static function &utente(&$row) {
		unset($row['codice_recupero_password']);
		unset($row['data_recupero_password']);
		unset($row['find']);
		$row['ultimo_accesso'] = Formatter::dateTime($row['ultimo_accesso']);
		$row['data_inserimento'] = Formatter::dateTime($row['data_inserimento']);
		return $row;
	}
	public static function &log_utente(&$row) {
		$row['utente'] = unserialize($row['utente']);
		$row['data_operazione'] = Formatter::dateTime($row['data_operazione']);
		return $row;
	}
	
	public static function &log_accesso(&$row) {
		$row['utente'] = unserialize($row['utente']);
		$row['data_operazione'] = Formatter::dateTime($row['data_operazione']);
		return $row;
	}	
}