<?php
define('LOG_STATO_EDIT', 2);
define('LOG_STATO_INFO', 1);
define('LOG_STATO_ERROR', 0);

class Log {			
	public static function risorsa($tipo_risorsa,
			&$id_risorsa,
			$azione, $data = null) {
		$utente = self::load_utente();
		if ($utente == null) {
			$utente = ['id' => 0, 'nome' => '-'];
		}
		$ip = HUtil::get_client_ip();
		$serialize_utente = serialize($utente);
		
		if (is_array($id_risorsa)) {
			$list = $id_risorsa;
		}
		else {
			$list = [$id_risorsa];
		}
		foreach ($list as $id) {
			H::track('risorsa')->write(
				'ip: ' . $ip . ', ' .
				'azione: ' . $azione . ', ' .
				'utente: ' . $utente['id'] . ', ' .
				'tipo_risorsa: ' . $tipo_risorsa . ', ' .
				'id_risorsa: ' . $id
			);
			
			$params = [
				'ultimo' => 0
			];
			$where = [
				'tipo_risorsa' => $tipo_risorsa,
				'id_risorsa' => $id
			];
			H::db()->update('log_risorsa', $params, $where);
			
			$params = [
				'id_utente' => $utente['id'],
				'utente' => $serialize_utente,
				'tipo_operazione' => $azione,
				'tipo_risorsa' => $tipo_risorsa,
				'id_risorsa' => $id,
				'data_operazione:_' => 'now()',
				'ultimo' => 1,
				'ip' => $ip
			];

			if($data != null){
				$serialize_data = serialize($data);
				$params['dati'] = $serialize_data;
			}
			H::db()->insert('log_risorsa', $params);
		}
	}
	
	private static function &load_utente($id = 0) {
		$user = null;
		$row = null;
		if (H::session()->isAuth()) {
			$row = H::session('user');
		}
		else {
			if ($id > 0) {
				$row = H::db()->selectOne('utente', $id)->item(); 
			}
		}
		if ($row != null) {
			$user = [
				'id' => $row['id'],
				'profile' => $row['profile'],
				'e_mail' => $row['e_mail'],
				'nome' => $row['nome'],
				'cognome' => $row['cognome'],
			];
		}
		return $user;
	}
	
	public static function accesso($azione, $id_utente = 0) {
		$utente = self::load_utente($id_utente);
		if ($utente == null) {
			return;
		}
		$ip = HUtil::get_client_ip();

		H::track('accesso')->write(
			'ip: ' . $ip . ', ' .
			'azione: ' . $azione . ', ' .
			'utente: ' . $id_utente . ', ' .
			'e_mail: ' . $utente['e_mail']
		);
		
		H::lib('Utils');
		$find_utente = Utils::find_string($utente);
		
		$params = [
			'data_operazione:_' => 'now()',
			'tipo_operazione' => $azione,
			'id_utente' => $utente['id'],
			'e_mail' => @$utente['e_mail'],
			'utente' => serialize($utente),
			'find_utente' => $find_utente,
			'ip' => $ip		
		];
		H::db()->insert('log_accesso', $params);
	}
	
	public static function info($azione,
			$tipo_risorsa,
			&$id_risorsa,
			$note = '',
			$id_utente = 0) {
		self::write(LOG_STATO_INFO, $azione, $tipo_risorsa, $id_risorsa, $note, $id_utente);	
	}
	public static function error($azione,
			$tipo_risorsa,
			&$id_risorsa,
			$note = '',
			$id_utente = 0) {
		self::write(LOG_STATO_ERROR, $azione, $tipo_risorsa, $id_risorsa, $note, $id_utente);	
	}
	public static function edit($azione,
			$tipo_risorsa,
			&$id_risorsa,
			$note = '',
			$id_utente = 0) {
		self::write(LOG_STATO_EDIT, $azione, $tipo_risorsa, $id_risorsa, $note, $id_utente);
	}
	public static function write($stato, $azione, 
			$tipo_risorsa,
			&$id_risorsa,
			$note = '',
			$id_utente = 0) {

		$utente = self::load_utente($id_utente);
		if ($utente == null) {
			$utente = ['id' => 0, 'nome' => '-'];
		}
		$serialize_utente = serialize($utente);
		$ip = HUtil::get_client_ip();
		H::lib('Utils');
		$find_utente = Utils::find_string($utente);
		
		if (is_array($id_risorsa)) {
			$list = $id_risorsa;
		}
		else {
			$list = [$id_risorsa];
		}
		foreach ($list as $id) {
			H::track('generic')->write(
				'ip: ' . $ip . ', ' .
				'stato: ' . $stato . ', ' .
				'azione: ' . $azione . ', ' .
				'utente: ' . $id_utente . ', ' .
				'tipo_risorsa: ' . $tipo_risorsa . ', ' .
				'id_risorsa: ' . $id . ', ' .
				'activity: ' . H::context()->requestActivity() . ', ' .
				'note: ' . $note
			);
								
			$params = [
				'data_operazione:_' => 'now()',
				'request_activity' => H::context()->requestActivity(),
				'dettaglio' => $note,
				'tipo_operazione' => $azione,
				'id_utente' => $utente['id'],
				'utente' => $serialize_utente,
				'find_utente' => $find_utente,
				'tipo_risorsa' => $tipo_risorsa,
				'id_risorsa' => $id,
				'stato' => $stato,
				'ip' => $ip
			];
			H::db()->insert('log_utente', $params);
		}
	}

	//## Extension to save edit data
	public static function edit_update($tipo, &$id, $nuovo, $data = null) {
		$azione = $nuovo ? 'new' : 'update';
		self::edit_azione($tipo, $id, $azione, $data);
	}
	public static function edit_abilita($tipo, &$id, $abilita) {
		$azione = $abilita ? 'enabled' : 'disabled';
		self::edit_azione($tipo, $id, $azione);
	}
	public static function edit_restore($tipo, &$id) {
		self::edit_azione($tipo, $id, 'restore');
	}
	public static function edit_delete($tipo, &$id) {
		self::edit_azione($tipo, $id, 'delete');
	}	
	public static function edit_response($tipo, &$id) {
		self::edit_azione($tipo, $id, 'response');
	}
	public static function edit_delete_real($tipo, &$id) {
		self::edit('delete_real', $tipo, $id);
	}
	public static function edit_azione($tipo, &$id, $azione, $data = null) {
		self::risorsa($tipo, $id, $azione, $data);
		self::edit($azione,
			$tipo,
			$id);
	}
}