<?php
class Utente {
	private $id = 0;
	private $password = '';
	private $e_mail = '';
	private $nome = '';
	private $cognome = '';
	private $abilitato = true;
	private $imposta_password = true;
	private $id_cliente = null;
	private $id_cliente_array = null;
	private $id_negozio = null;
	private $id_cliente_profile = 0;
	private $profile = 0;
	private $sola_lettura = 0;
	
	public function loadFromInput() {
		$this->setId(H::input('id', 0));
		$this->setEMail(H::input('e_mail', ''));
		$this->setNome(H::input('nome', ''));
		$this->setCognome(H::input('cognome', ''));
		$this->setAbilitato(H::input('abilitato', 0));		
		$this->setImpostaPassword(H::input('imposta_password', 0));		
		$this->setPassword(H::input('password', ''));		
		$this->setProfile(H::input('profile', 0));
		$this->id_cliente = H::input('id_cliente', 0);
		$this->id_negozio = H::input()->getArrayClean('id_negozio');			
		$this->id_cliente_array = H::input()->getArrayClean('id_cliente_array', null);
		$this->id_cliente_profile = H::input('id_cliente_profile', 0);
		$this->sola_lettura = H::input('sola_lettura', 0);
	}
	public function setId($value) {
		$this->id = $value;
	}
	public function setEMail($value) {
		$this->e_mail = strtolower($value);
	}
	public function setNome($value) {
		$this->nome = $value;
	}
	public function setCognome($value) {
		$this->cognome = $value;
	}
	public function setAbilitato($value) {
		$this->abilitato = $value;
	}
	public function setPassword($value) {
		$this->password = $value;
	}
	public function setProfile($value) {
		$this->profile = $value;
	}	
	public function setImpostaPassword($value) {
		$this->imposta_password = $value;
	}	

	public function getId() {
		return $this->id;
	}
	public function isAbilitato() {
		return $this->abilitato;
	}
	public function isImpostaPassword() {
		return $this->imposta_password;
	}	

	public function getProfile() {
		return $this->profile;
	}
	public function getPassword() {
		return $this->password;
	}
	public function getEMail() {
		return $this->e_mail;
	}
	public function getNome() {
		return $this->nome;
	}
	public function getCognome() {
		return $this->cognome;
	}

	public function save() {
		$nuovo = $this->id <= 0;
		
		$find = [
			$this->getEMail(),
			$this->getNome(),
			$this->getCognome(),
		];
		H::lib('Utils');
		$find = Utils::find_string($find);
		if($this->getProfile() == 8 || $this->getProfile() == 6){
        	$this->sola_lettura = 1;
		}
		$params = [
			'e_mail' => $this->getEMail(),
			'nome' => $this->getNome(),
			'cognome' => $this->getCognome(),
			'abilitato' => $this->isAbilitato() ? 1 : 0,
			'profile' => $this->getProfile(),
			'find' => $find,
			'sola_lettura' => $this->sola_lettura,
            'id_cliente' => $this->id_cliente
		];

		$profilo_precedente = '';
		if ($nuovo) {	
			$profilo_precedente = $this->getProfile();
			$params['imposta_password'] = $this->isImpostaPassword() ? 1 : 0;
			$params['data_inserimento:_'] = 'now()';
			$this->id = H::db()->insert('utente', $params);
		}
		else {
			$row = H::db()->selectOne('utente', $this->id)->item();
			if ($row) {
				$profilo_precedente = $row['profile'];
			}
			if ($profilo_precedente == 1) {
				$params['profile'] = 1;
			}
			H::db()->updateById('utente', $params, $this->id);
		}

		if ($nuovo) {
			self::imposta_password($this->id, $this->getPassword(), $this->isImpostaPassword());
//			$params = [
//				'id_utente' => $this->id,
//				'id_cliente' => $this->id_cliente
//			];
//			H::db()->insert('utente_cliente', $params);
		}

		$params = [
			'id_utente' => $this->id,
		];
		H::db()->delete('utente_negozio', $params);
		H::db()->delete('utente_cliente', $params);

		if ($this->getProfile() == 6) {
			if ($this->id_negozio != null && count($this->id_negozio) > 0) {
				foreach ($this->id_negozio as $ids) {
					$params['id_negozio'] = $ids;
					H::db()->insert('utente_negozio', $params);
				}
			}
		}
		if ($this->getProfile() == 9) {
			if ($this->id_cliente_array != null && count($this->id_cliente_array) > 0) {
				foreach ($this->id_cliente_array as $ids) {
					$params['id_cliente'] = $ids;
					H::db()->insert('utente_cliente', $params);
				}
			}
		}
		
		Log::edit_update('utente', $this->id, $nuovo);
		if (!$nuovo) {
			if (!$this->isAbilitato() || $profilo_precedente != $this->getProfile()) {
				Profile::update_user_profile($this->id);
			}
		}
		return $this->id;
	}
	
	public static function imposta_password($id, $password, $imposta_password_avvio = false) {
		$check_string = HUtil::randomCode(30);
		$password = sha1($id . $password . $check_string);
		
		$params = [
			'codice_recupero_password' => null,
			'tentativi_accesso' => 0,
			'imposta_password' => $imposta_password_avvio ? 1 : 0,
		];
		H::db()->updateById('utente', 
			$params, 
			$id);
		
		$qryList = [
			'DELETE FROM utente_accesso_check WHERE id_utente > 1 AND id_utente = :id',
			'DELETE FROM utente_accesso WHERE id_utente > 1 AND id_utente = :id',		
		];
		$params = [
			':id' => $id,
		];
		H::db()->queryList($qryList, $params);
		
		$params = [
			'id_utente' => $id,
			'check_string' => $check_string
		];
		H::db()->insert('utente_accesso_check', $params);					

		$params = [
			'id_utente' => $id,
			'password' => $password
		];						
		H::db()->insert('utente_accesso', $params);	
	}
	
	public static function disabilita(&$list) {
		if (count($list) == 0) {
			return;
		}
		$params = [
			'abilitato' => 0
		];
		$where = [
			'id:>' => 1,
			'id' => $list
		];
		H::db()->update('utente', $params, $where);
		
		self::bloccaAccount($list);
		Log::edit_abilita('utente', $list, false);
	}
	
	public static function abilita(&$list) {
		if (count($list) == 0) {
			return;
		}	
		$params = [
			'abilitato' => 1
		];
		$where = [
			'id:>' => 1,
			'id' => $list
		];
		H::db()->update('utente', $params, $where);
		
		Log::edit_abilita('utente', $list, true);
	}
	
	private static function bloccaAccount(&$list) {
		H::lib('Profile');
		Profile::update_user_profile($list);

		$where = [
			'id_utente:>' => 1,
			'id_utente' => $list
		];
		H::db()->delete('utente_session', $where);
	}
}