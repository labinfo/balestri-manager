<?php

/**
 * Description of Logger
 * Simple logger to file.
 * @author Pievis
 */
class Logger {

    private $filePath;

    public function __construct($filePath) {
        if (!file_exists($filePath)) {
            //Create file
            if (!touch($filePath)) {
                throw new Exception('Log file ' . $filePath . ' cannot be created');
            }
        }
        $this->filePath = $filePath;
    }

    public function log($str) {
        $logline = '[' . date('Y-m-d H:i:s') . '] ' . $str . "\n";
        file_put_contents($this->filePath, $logline, FILE_APPEND | LOCK_EX);
    }
    
    public static function main(){
        $path = HSystem::path('logs') . '/main.txt';
        return new Logger($path);
    }

}
