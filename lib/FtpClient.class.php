<?php

/**
 * Description of FtpClient
 * 
 * @author Pier
 */
class FtpClient {
    
    //Should be moved somewhere else
    const SERVER = 'balestri.labinfo.net';
    const USER = 'wizard';
    const PASSWORD = 'Wizard_2016!';
    
    private $username = 'wizard';
    private $password = 'Wizard_2016!';
    private $hots = 'balestri.labinfo.net';
    
    
    private $conn_id;
    var $localDir = 'import/';

    public function __construct($ftp_server) {
        $this->host = $ftp_server;
    }

    public function login($username, $password) {
        $this->username = $username;
        $this->password = $password;
        $this->conn_id = ftp_connect($this->hots);
        return ftp_login($this->conn_id, $username, $password);
    }

    public function setLocalFolder($dir) {
        $this->localDir = $dir;
    }

    /*
     * Download a file from the specified ftp server
     */
    public function download($file, $close_connection = false) {
        $success = false;
        $local_file = $this->localDir . $file;
        if (ftp_get($this->conn_id, $local_file, $file, FTP_BINARY)) {
            $success = true;
        }
        if($close_connection){
            ftp_close($this->conn_id);
        }
        return $success;
    }
    
    public function upload($file_local, $file_remote, $close_connection = false){
        $success = false;
        $file_local = $this->localDir . $file_local;
        if (ftp_put($this->conn_id, $file_remote, $file_local, FTP_BINARY)) {
            $success = true;
        }
        if($close_connection){
            ftp_close($this->conn_id);
        }
        return $success;
    }
    
    public function close_connection(){
        ftp_close($this->conn_id);
    }

    /////////////////////////////////////////////////////////////////////////
    public static function test() {
        $ftp = new FtpClient(FtpClient::SERVER);
        if (!$ftp->login(FtpClient::USER, FtpClient::PASSWORD)) {
            Logger::main()->log('errore login');
            return;
        }
        $ftp->setLocalFolder(HSystem::path(Utils::getSharedFileFolder() . '/'));
        Logger::main()->log($ftp->localDir);
        if (!$ftp->download('2.jpg')) {
            Logger::main()->log('errore download');
            return;
        }
        Logger::main()->log('test end');
    }

}
