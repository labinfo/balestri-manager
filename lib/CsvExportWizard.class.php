<?php

/**
 * Classe che esporta i file csv necessari per la sinconizzazione con il gestionale
 * balestri e balestri.
 * @author Pievis
 */
class CsvExportWizard
{

    const FTP_SERVER = 'balestri.labinfo.net';
    const FTP_USER = 'wizard';
    const FTP_PASSWORD = 'Wizard_2016!';
    const DIR_EXPORT = 'export';
    const LOGGER_NAME = 'wizard_export';

    //
    var $ftp;

    static function getExportDir()
    {
        $export_dir = H::config('export_folder', HSystem::path(self::DIR_EXPORT));
        return $export_dir;
    }

    function __construct()
    {
        H::lib('FtpClient');
        if (!file_exists(self::getExportDir())) {
            mkdir(self::getExportDir());
        }
        $this->ftp = new FtpClient(self::FTP_SERVER);
    }

    //id,nome,id cliente
    //1,Unieuro Cesena,0000022
    function gen_csv_negozi()
    {
        $negozi = $this->get_negozi();
        $list = array();
        foreach ($negozi as $negozio) {
            $list[] = array($negozio['id'],
                $negozio['nome'],
                $negozio['id_cliente']
            );
        }
        $this->gen_csv(self::getExportDir() . '/export_negozi.csv', $list);
    }

    function get_negozi()
    {
        $params = [
            'n.id_cliente:_' => 'c.id'
        ];
        $list = H::db()->selectView('n.id, n.nome, c.export_id as id_cliente',
            'negozio n, cliente c', $params)->listItems();
        foreach ($list as &$item) {
            $item['id_cliente'] = self::format_id($item['id_cliente']);
        }
        return $list;
    }

    //codice,id cliente,id negozio,codice tipo cartello,id fornitore,ubicazione
    //7,000001,1,89CP,0000300,Forlì SS67 km. 167+460
    function gen_csv_cartelli()
    {
        //$cartelli = $this->get_cartelli();
        $cartelli = $this->get_all_cartelli();
        $list = array();
        foreach ($cartelli as $cartello) {
            $list[] = array(
                $cartello['codice'],
                $cartello['id_cliente'],
                $cartello['id_negozio'],
                '"' . $cartello['codice_tipo_cartello'] . '"',
                $cartello['id_fornitore'],
                '"' . $cartello['ubicazione'] . '"',
                $cartello['abilitato']
            );
        }
        H::log('d')->debug(json_encode($list));
        $this->gen_csv(self::getExportDir() . '/export_cartelli.csv', $list);
    }

    function get_cartelli()
    {
        $params = [
            'c.id_tipo_cartello:_' => 'tc.id',
            'c.abilitato:_' => 1,    //non fare l'export dei cartelli temporanei
            'c.id_cliente:_' => 'cl.id'
        ];
        //## 14/04 id cliente è sostituito da export_id
        $list = H::db()->selectView('c.codice, cl.export_id as id_cliente, c.id_negozio,
                                    tc.codice as codice_tipo_cartello, c.id_fornitore,
                                    c.recapito_ubicazione as ubicazione',
            'cartello c, tipo_cartello tc, cliente cl', $params)->listItems();
        foreach ($list as &$item) {
            $item['codice'] = self::format_id($item['codice'], 10);
            $item['id_cliente'] = self::format_id($item['id_cliente']);
            $item['id_fornitore'] = self::format_id($item['id_fornitore']);
            $item['ubicazione'] = str_replace(["\r", "\n", "\t"],
                ' ', $item['ubicazione']);
        }
        return $list;
    }

    function get_all_cartelli() {
        $params = [
            'c.id_tipo_cartello:_' => 'tc.id',
            'c.id_cliente:_' => 'cl.id'
        ];

        $view = ['c.codice',
            'cl.export_id as id_cliente',
            'c.id_negozio',
            'c.abilitato',
            'c.disinstallato',
            'tc.codice as codice_tipo_cartello',
            'c.id_fornitore',
            'c.recapito_ubicazione as ubicazione',
        ];
            $tables = [
                'cartello c',
                'tipo_cartello tc',
                'cliente cl'
            ];
        $list = H::db()->selectView($view,$tables,$params)->listItems();
        foreach ($list as &$item) {
            if ($item['disinstallato']) {
                $item['abilitato'] = 1;
            }
            $item['codice'] = self::format_id($item['codice'], 10);
            $item['id_cliente'] = self::format_id($item['id_cliente']);
            $item['id_fornitore'] = self::format_id($item['id_fornitore']);
            $item['ubicazione'] = str_replace(["\r", "\n", "\t"], ' ', $item['ubicazione']);
        }
        return $list;
    }

    function gen_csv($file, $list)
    {
        if (file_exists($file)) {
            unlink($file);
        }
        if (!touch($file)) {
            H::log(self::LOGGER_NAME)->error("Errore durante la creazione del file: $file");
            return;
        }
        $fp = fopen($file, 'w');
        if ($fp === false) {
            H::log(self::LOGGER_NAME)->error("Errore durante la creazione del file: $file");
            return;
        }
        foreach ($list as $fields) {
            $str = self::arrayToCsv($fields, ';', '"', true) . "\n";
            fwrite($fp, $str);
//            fputcsv($fp, $fields, ';');
        }
        fclose($fp);
        H::log(self::LOGGER_NAME)->debug("$file salvato");
    }

    /**
     * Formats a line (passed as a fields  array) as CSV and returns the CSV as a string.
     * Adapted from http://us3.php.net/manual/en/function.fputcsv.php#87120
     */
    static function arrayToCsv(array &$fields, $delimiter = ';', $enclosure = '"',
                               $encloseAll = false, $nullToMysqlNull = false)
    {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ($fields as $field) {
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }

            $not_num = is_numeric($field) ? false : (is_string($field) ? true : false);
            $enc_length = strlen($enclosure);
            $closed = substr($field, 0, $enc_length) === $enclosure
                && substr($field, strlen($field) - $enc_length, $enc_length) === $enclosure;
            // Enclose fields containing $delimiter, $enclosure or whitespace
            if ($closed) {
                $output[] = $field;
            } else {
                if (($encloseAll && $not_num)
                    || preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field)
                ) {
                    $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
                } else {
                    $output[] = $field;
                }
            }
        }
        return implode($delimiter, $output);
    }

    function ftp_upload()
    {
        $ftp = $this->ftp;
        H::log(self::LOGGER_NAME)->debug('upload ftp start');
        if (!$ftp->login(FtpClient::USER, FtpClient::PASSWORD)) {
            H::log(self::LOGGER_NAME)->error('errore login');
            return;
        }
        $ftp->setLocalFolder(HSystem::path('export/'));
        H::log(self::LOGGER_NAME)->debug($ftp->localDir);
        $file = 'export_cartelli.csv';
        if (!$ftp->upload($file, $file)) {
            H::log(self::LOGGER_NAME)->error('errore upload ' . $file);
            return;
        }
        $file = 'export_negozi.csv';
        if (!$ftp->upload($file, $file)) {
            H::log(self::LOGGER_NAME)->error('errore upload ' . $file);
            return;
        }
        H::log(self::LOGGER_NAME)->debug('upload ftp end');
        $this->ftp->close_connection();
    }

    static function format_id($value, $length = 7)
    {
        return str_pad($value, $length, '0', STR_PAD_LEFT);
    }

    //
    public static function run()
    {
        $w = new CsvExportWizard();
        $w->gen_csv_negozi();
        $w->gen_csv_cartelli();
//        Disattivato in locale
//        $w->ftp_upload();
    }

}
