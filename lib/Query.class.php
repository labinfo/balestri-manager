<?php
class Query {	
	private static function date_input_params($key, &$params) {
		$value = H::input()->getTimestamp($key . '_dal');
		if ($value != null && $value != '') {
			$params[$key . ':>='] = $value;
		}
		$value = H::input()->getTimestamp($key . '_al');
		if ($value != null && $value != '') {
			$params[$key . ':<='] = $value;
		}	
	}
	
	public static function input_filter_params(&$list, &$params) {
		if ($list == null) {
			return;
		}
		if ($params == null) {
			$params = [];
		}
		foreach ($list as $key) {
			if (HString::indexOf($key, '.') > 0) {
				$input_key = substr($key, HString::indexOf($key, '.') + 1);
			}
			else {
				$input_key = $key;
			}
			switch($input_key) {		
			case 'abilitati':
				if (H::input('abilitati', '') != '') {
					$params['abilitato'] = 1;
				}
				if (H::input('non_abilitati', '') != '') {
					$params['abilitato'] = 0;
				}			
			case 'eliminati':
				if (H::input('nascondi_eliminati', '') != '') {
					$params['eliminato'] = 0;
				}
				break;			
			case 'data_inserimento':
			case 'data_modifica':
				self::date_input_params($key, $params);
				break;
			case 'ip':
				$value = H::input($input_key);
				if ($value != '') {
					$params[$key . ':like'] = HString::normalize($value) . '%';
				}
				break;
			case 'codice':
			case 'find':
				$value = H::input($input_key);
				if ($value != '') {
					$str = HString::normalize($value);
					$str = explode (' ', $str);
					$str_list = [];
					foreach ($str as $s) {
						$s = trim($s);
						if ($s != '') {
							$str_list[] = '%' . $s . '%';
						}
					}
					if (count($str_list) > 0) {
						$params[$key . ':like'] = $str_list;
					}
				}
				break;
			default: 
				$value = H::input()->getArrayClean($input_key, null);
				if ($value != null) {
					$params[$key] = $value;
				}
			}
		}
	}
	
	public static function list_items($_,
		&$list_filter, 
		$table, 
		$def_order = '', 
		&$order_auth = null, 
		&$params = null) {
		self::list_items_type($_, $list_filter, '*', $table, $table, $def_order, $order_auth, $params);
	}

	public static function list_items_type($_,
		&$list_filter,
		$view,
		$table,
		$type,
		$def_order = '',
		&$order_auth = null,
		&$params = null) {
		$list = self::list_items_type_return($_, $list_filter, $view,
			$table, $type, $def_order, $order_auth,
			$params);
		H::view('list', $list);
	}

	public static function list_items_type_return($_,
		&$list_filter, 
		$view, 
		$table, 
		$type, 
		$def_order = '', 
		&$order_auth = null, 
		&$params = null) {
		self::input_filter_params($list_filter, $params);
		
		$order = H::input('order', $def_order);

		$orderBy = null;
		$order_list = explode(',', $order);
		if (count($order_list) > 0) {
			foreach ($order_list as $order) {
				$check = explode(':', $order);
				if (count($check) == 2 &&
					($check[1] == 'asc' || $check[1] == 'desc') &&
					($order_auth == null || in_array($check[0], $order_auth))) {
					if ($orderBy == null) {
						$orderBy = [];
					}
					//case specific conversion
					self::order_filter($order);
					$orderBy[] = str_replace(':', ' ', $order);
				}
			}

		}
		$val = $_('all', false);

		if ($val == 'true') {
			$list = H::db()->selectView($view, $table, $params, $orderBy)->listItems($type);
		}
		else {
			$start = H::input('start', 0);
			$number = H::config('num_list_items', 50);
			
			$total_items = H::db()->queryCount($table, $params);
			H::view('list_total_items', $total_items);

			$list = H::db()->selectView($view, $table, $params, $orderBy, $start, $number)->listItems($type);
		}
		return $list;
	}

	public static function order_filter(&$order){
		$check = explode(':', $order);
		$field = $check[0];
		$order = $check[1];
		if($field === 'codice_int'){
			$field = 'cast(e.codice as unsigned)';
		}
		$order = $field .':'. $order;
	}
}