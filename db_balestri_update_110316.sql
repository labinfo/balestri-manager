/*25/02 update per la configurazione con il gestionale */
ALTER TABLE `tipo_cartello` 
ADD COLUMN `codice` VARCHAR(150) NULL AFTER `icona`;

/* 29/02 gestione preferiti */
CREATE TABLE IF NOT EXISTS `entita_preferito` (
  `id_utente` INT(10) UNSIGNED NOT NULL,
  `id_entita` INT(10) UNSIGNED NOT NULL,
  `tipo` VARCHAR(50) NOT NULL,
  FOREIGN KEY (id_utente) REFERENCES utente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY (`id_utente`, `id_entita`, `tipo`));
  
/* 09/03 inserimento valori per cartelli temporanei */
INSERT INTO `fornitore` (`id`, `nome`, `descrizione`) VALUES ('0', 'NESSUN FORNITORE', '');
INSERT INTO `tipo_cartello` (`id`, `nome`) VALUES ('0', 'NESSUN TIPO');
INSERT INTO `provincia` (`id`, `nome`) VALUES ('0', 'NESSUNA PROVINCIA');

/* fix decimali per lat lon */
ALTER TABLE `cartello` 
CHANGE COLUMN `recapito_latitudine` `recapito_latitudine` FLOAT(10,7) NOT NULL DEFAULT '0' ;
CHANGE COLUMN `recapito_longitudine` `recapito_longitudine` FLOAT(10,7) NOT NULL DEFAULT '0' ;

ALTER TABLE `negozio` 
CHANGE COLUMN `recapito_latitudine` `recapito_latitudine` FLOAT(10,7) NOT NULL DEFAULT '0' ;
CHANGE COLUMN `recapito_longitudine` `recapito_longitudine` FLOAT(10,7) NOT NULL DEFAULT '0' ;

/* updated profile values */
set sql_safe_updates = 0; /*check first*/
UPDATE utente set utente.profile = 2 where utente.profile = 8;
UPDATE utente set utente.profile = 1 where utente.profile = 6;