(function( $ ){
	$.fn.download = function(opt) {
		if (opt == null) {
			opt = $(this).actionParams();
		}
		var id = H.id();
		var cont = $('<div style="display: none;" class="download-wrapper"/>');
		var form = $('<form action="' + opt.url + '" target="' + id + '" method="post"/>');
		cont.append('<iframe name="' + id + '"/>')
			.append(form);
		if (opt.paramsFrom && $(opt.paramsFrom).length) {
			var params = $(opt.paramsFrom).actionParams('data-load-params');
			if (opt.params) {
				opt.params = $.extend(opt.params, params);
			}
			else {
				opt.params = params;
			}
		}
		if (opt.params) {
			for (var key in opt.params) {
				if (opt.params.hasOwnProperty(key)) {
					if (typeof opt.params[key] === 'object') {
						for (var i = 0; i < opt.params[key].length; i++) {
							if (opt.params[key][i] != '') {
								form.append('<input type="hidden" name="' + key + '" value="' + opt.params[key][i] + '"/>');
							}
						}
					}
					else {
						form.append('<input type="hidden" name="' + key + '" value="' + opt.params[key] + '"/>');
					}
				}
			}
		}
		$('body').append(cont);
		form.submit();
	};
})( jQuery );

$(function(){
	$('body').on('click', '.action-ip-bloccati', function() {
		$('<div/>').box({
				load: {
					loadTemplate: {
						template: 'system/ip/index', 
					}, 
					loadData: {
						url: 'system/ip/list'
					}
				},
				overlay: true,
				classCss: 'layer-small-edit'
			});
	});
	
	$('body').on('click', '.action-reset-log', function() {
		H.confirm({
			text: 'Procedere con la cancellazione dei log?',
			onConfirm: function() {
				H.loading.show();
				$.xajax('/my/data/log/generic/ex-delete', {
						successAndCallbackOk: function() {
							$('#list_log_generici [data-merge-content] tr').remove();
						}
					});			
			}
		});	
	});
	
	$('body').on('click', '.action-filtro-load-data', function() {
		var $this = $(this);
		$this.closest('.window').addClass('window-filter-active');
		var opt = $this.actionParams();
		var form = $this.closest('form');
		var params = {};
		if (form.length > 0) {
			if (H.validator.checkIsValidForm(form)) {
				params = form.serializeObject();
			}
			else {
				return;
			}
		}
		if (typeof opt.load === 'string') {
			opt.load = [opt.load];
		}
		for (var i = 0; i < opt.load.length; i++) {
			var cont = $(opt.load[i]);
			cont.actionParams('data-load-params', params, true);
			cont.loadData({empty: true});
		}
	});
		
	$('body').on('click', '.action-download', function() {
            //modifica by pier per accettare i filtri
            $form = $(this).closest('form');
            if($form.length){
                var opt = $(this).actionParams();
                if(opt.urls.length){
                    for(i=0; i < opt.urls.length; i++)
                    $(this).download({
                        url: opt.urls[i],
                        params: $form.serializeObject()
                    });
                }else{
                    $(this).download({
                        url: opt.url,
                        params: $form.serializeObject()
                    });
                }
            }else{
		$(this).download();
            }
	});
		
	$('body').on('click', '.action-delete-item-simple', function() {
		$(this).fadeOut(200, function() {
			$(this).remove();
		});
	});
	
	$('body').on('click', '.action-reset-filtro', function() {
		var cont = $(this).closest('.window');
		var form = cont.find('.window-filter form');
		cont.removeClass('window-filter-active');
		form.reset();
		var opt = $(this).actionParams();
		if (opt.load) {
			var params = form.serializeObject();
			if (typeof opt.load === 'string') {
				opt.load = [opt.load];
			}
			for (var i = 0; i < opt.load.length; i++) {
				cont = $(opt.load[i]);
				cont.actionParams('data-load-params', params, true);
				cont.loadData({empty: true});
			}
		}
	});
	
	$('body').on('click', '.action-attiva-filtro', function() {
		var cont = $(this).closest('.window');
		if (cont.hasClass('window-show-filter')) {
			cont.removeClass('window-show-filter');
		}	
		else {
			cont.addClass('window-show-filter');
		}
	});
	
	$('body').on('click', '.action-item', function(e) {
		var $this = $(this);
		var isShow = $this.hasClass('show');
		$('body').trigger('hide-on-click');
		if (isShow) {
			return;
		}
		$(this).addClass('show');
		var opt = $this.actionParams();
		var id = $this.closest('tr[data-id]').attr('data-id');
		var params = {
			menu: true, 
			parent: $this.parent(),
			content: H.content(opt.options),
			showIf: opt.showIf,
			onClose: function() {
				$this.removeClass('show');
			}
		};
		var box = $('<div/>').box(params);
		if ($(this).closest('td').length > 0 || opt.position == 'right') {
			box.css({right: '10px', top: '30px'});
		}
		else {
			box.css({left: ($(this).offset().left - $(this).width()) + 'px', top: '30px'});
		}
		box.dataLoaded();
	});
	
	$('body').on('click', '.button-expand', function() {
		$('body').trigger('hide-on-click');
		var buttons = $('#content_menu .menu-wrapper .button');

		var $this = $(this);
		if ($('#content_menu').hasClass('menu-implode')) {
			buttons.find('span').fadeOut(200);
			buttons.animate({
				marginLeft: '-15px'
			});
			$('#content_menu').animate({
				width: '35px'
			}, function() {
				$(this).removeClass('menu-implode')
					.addClass('menu-expand');
			});
			$('#content').animate({
				left: '35px'
			});
			H.cookie.set('menu_bar', 'close');
		}
		else {
			buttons.animate({
				marginLeft: '0px'
			});
			$('#content_menu').animate({
				width: '235px'
			}, function() {
				buttons.find('span').fadeIn(200);
				$(this).removeClass('menu-expand')
					.addClass('menu-implode');
			});
			$('#content').animate({
				left: '235px'
			});
			H.cookie.set('menu_bar', 'open');
		}
	});
	
	function impostaSize() {
		$('body').trigger('hide-on-click');
		if ($(window).width() <= 760) {
			if ($('#content_menu').hasClass('menu-implode')) {
				var buttons = $('#content_menu .menu-wrapper .button');
				buttons.find('span').css({display: 'none'});
				buttons.css({
					marginLeft: '-15px'
				});
				$('#content_menu').css({
						width: '35px'
					})
					.removeClass('menu-implode')
					.addClass('menu-expand');
			}
			$('.button-expand').css({display: 'none'});
			if ($(window).width() <= 560) {
				$('#content').css({
					left: '0px'
				});			
			}
			else {
				$('#content').css({
					left: '35px'
				});				
			}
		}
		else {
			$('.button-expand').css({display: 'block'});
		}
	}
		
	$('[data-section]').on('click', function(e, rewriteHash) {
		var rewriteHash = rewriteHash == null ? true : rewriteHash;
		$('.download-wrapper').remove();
		$('body').trigger('hide-on-click');
		$('[data-section].selected').removeClass('selected');
		var cont = $(this).closest('.button-list-section');
		if (cont.length > 0) {
			cont.css({display: 'block'});
		}
		$(this).addClass('selected');
		var opt = $(this).actionParams();
		if (rewriteHash) {
			var hash = $(this).attr('data-section');
			H.utils.location.setHash(hash);
		}
		$('#content').loading()
			.loadTemplateAndData(opt);
		$(this).blur();
	});	
	
	$('body').on('click', '.action-open-in-content', function() {
		var opt = $(this).actionParams();
		$('#content').loading()
			.loadTemplateAndData(opt);
	});
	
	$('.action-section-win').on('click', function() {
		var opt = $(this).actionParams();
		opt.css = opt.css == null ? 'layer-small-edit' : opt.css;
		$('<div/>').box({
				load: opt,
				overlay: true,
				classCss: opt.css 
			});
		$(this).blur();
	});
	
	$('.action-section-list').on('click', function() {
		$(this).next().toggle('linear');
		$(this).blur();
	});	
	
	function currentSection() {
		var hash = H.utils.location.getHash();
		if (hash == null || hash == '') {
			hash = 'dashboard';
		}
		$('[data-section=' + hash + ']').click();
	}
	currentSection();	

	$('.action-section-win, .action-section-list').each(function() {
		$(this).attr('href', 'javascript:void(0);');
	});
	
	if (H.cookie.get('menu_bar') == 'close') {
		var buttons = $('#content_menu .menu-wrapper .button');
		buttons.find('span').css({display: 'none'});
		buttons.css({
			marginLeft: '-15px'
		});
		$('#content_menu').css({
				width: '35px'
			})
			.removeClass('menu-implode')
			.addClass('menu-expand');
		$('#content').css({
			left: '35px'
		});
	}
	
	impostaSize();
	$(window).resize(function() {
		impostaSize();
	});
	
	H.loading.hide();
});