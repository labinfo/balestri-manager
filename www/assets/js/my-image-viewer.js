/* 
 * A simple image gallery, with arrows and image preview to show it's content.
 */

;(function ($, window, document, undefined) {
    "use strict";

    // Create the defaults once
    var pluginName = "imageViewer",
        defaults = {
            useAnimations: true,
            debug: true,
            imgRef: [],
            useFullUrl: false,
            highResImgPath: 'img/1024x768/',
            lowResImgPath: 'img/200x200/',
            useImgList: true
        };

    // The actual plugin constructor
    function GalleryPlugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.data = {
            imageIndex: 0
        };
        this.layout = {
            imgBox: null,
            rightArrow: null,
            leftArrow: null,
            imageList: null
        };
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(GalleryPlugin.prototype, {
        init: function () {
            this.log('init called');
            this.setupUI();
            if(this.settings.imgRef.length > 0){
                this.updateImage(0);
            }
            if(this.settings.useImgList){
                this.initImgList();
            }else{
                $(this.layout.imageList).hide();
            }
        },
        setupUI: function () {
            this.layout.imgBox = $(this.element).find('.image-box');
            this.layout.rightArrow = $(this.element).find('.arrow-right');
            this.layout.leftArrow = $(this.element).find('.arrow-left');
            this.layout.imageList = $(this.element).find('.image-list');

            var that = this;
            $(this.layout.rightArrow).click(function () {
                if (that.data.imageIndex < that.settings.imgRef.length - 1)
                    that.updateImage(++that.data.imageIndex)
            });
            $(that.layout.leftArrow).click(function () {
                if (that.data.imageIndex > 0)
                    that.updateImage(--that.data.imageIndex)
            });
        },
        initImgList: function (){
            var $list = $(this.layout.imageList);
            var imgRef = this.settings.imgRef;
            $list.empty();
            var that = this;
            for(var i = 0; i < imgRef.length; i++){
                var url = this.settings.lowResImgPath + imgRef[i];
                var $e = $('<img>')
                    .attr('src', url)
                    .attr('data-id', i);
                if(i == this.data.imageIndex){
                    $e.addClass('selected');
                }
                $list.append($e);
                $e.on('click', function () {
                    that.onImgListClick(that, this);
                });
            }
        },
        onImgListClick: function (elem, selected){
            var index = $(selected).attr('data-id');
            elem.data.imageIndex = index;
            elem.log('index ' + index);
            elem.updateImage(index);
            //elem.updateImgList(elem, selected);
        },
        updateImgList: function (elem, selected){
            if(selected == null){
                selected = $(elem.layout.imageList)
                    .find('[data-id=' + elem.data.imageIndex + ']');
            }
            $(elem.layout.imageList).find('.selected')
                .removeClass('selected');
            $(selected).addClass('selected');
        },
        log: function (text) {
            if (this.settings.debug) {
                console.log(pluginName + '] ' + text);
            }
        },
        printElement: function (obj) {
            if (this.settings.debug) {
                if($(obj).length > 0)
                    var text = $(obj)[0].outerHTML;
                else
                    var text = $(obj).outerHTML;
                console.log(text);
            }
        },
        getFullUrl: function (imgRef) {
            if (this.settings.useFullUrl)
                return imgRef;
            return this.settings.highResImgPath + imgRef;
        },
        updateImage: function (index) {
            if (index >= this.settings.imgRef.length
                || index < 0)
                return;

            var frame = $(this.layout.imgBox).find('img.image-frame');
            var imgRef = this.getFullUrl(this.settings.imgRef[index]);
            this.log('update to: ' + imgRef);
            this.loading(true);
            var that = this;
            $(frame).attr('src', imgRef); //start loading the image
            $(frame).one('load', function () {
                //loading function
                that.loading(false);
            }).each(function () {
                //after load callback
                if (this.complete)
                    $(this).load();
            });
            this.updateArrows(index);
            if(this.settings.useImgList){
                this.updateImgList(this);
            }
        },
        loading: function (start) {
            if (start)
                $(this.layout.imgBox)
                    .find('.loading-frame').show();
            else
                $(this.layout.imgBox)
                    .find('.loading-frame').hide();
        },
        updateArrows: function (index) {
            var length = this.settings.imgRef.length;
            if (index == length - 1){
                $(this.layout.rightArrow).hide();
            }
            else
                $(this.layout.rightArrow).show();
            if (index == 0)
                $(this.layout.leftArrow).hide();
            else
                $(this.layout.leftArrow).show();
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" +
                    pluginName, new GalleryPlugin(this, options));
            }
        });
    };

})(jQuery, window, document);