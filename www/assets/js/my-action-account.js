(function( $ ){

})( jQuery );

$(function(){
	$('body').on('modifica-dati-utente', function(e, data) {
		if (data.item) {
			$('.dati-current-utente').html(data.item.nome + ' ' + data.item.cognome);
		}
	});
	
	$('body').on('click', '.action-utente-modifica-password', function() {
		$('<div/>').box({
				load: {
					loadTemplate: {
						template: 'account/modifica_password', 
						render: true
					}
				},
				overlay: true,
				classCss: 'layer-modifica-password'
			});
	});
	
	$('body').on('click', '.action-utente-modifica-dati', function() {
		$('<div/>').box({
				load: {
					loadTemplate: {
						template: 'account/modifica_dati', 
					}, 
					loadData: {
						url: 'account/load-current'
					}
				},
				overlay: true,
				classCss: 'layer-modifica-dati'
			});
	});
	
	$('body').on('click', '.action-utente-scrivi', function() {
		var opt = $(this).actionParams();
		if (opt && opt.id) {
			id = opt.id;
		}
		else {
			var cont = $(this).closest('tr[data-id-utente]');
			if (cont.length == 0) {
				H.message.error('Impossibile inviare il messaggio, riferimento non presente');
				return;
			}
			var id = parseInt(cont.attr('data-id-utente'));
			if (id == 0) {
				H.message.error('Utente non definito');
				return;
			}
		}
		if (id == 1) {
			$('<div/>').box({
					load: {
						loadTemplate: {
							template: 'account/message_super'
						}
					},
					overlay: true,
					classCss: 'layer-message'
				});

		}		
		else {
			$('<div/>').box({
					load: {
						loadTemplate: {
							template: 'account/message_simple'
						}, 
						loadData: {
							url: 'account/load-items',
							params: {id: id},
							template: 'list-simple',
							dataType: 'list-simple'
						}
					},
					overlay: true,
					classCss: 'layer-message'
				});
		}
	});
	
	$('body').on('click', '.action-utente-messaggio', function() {
		var id = $(this).tableSelectedId();
		if (id == null) {
			return;
		}
		$('<div/>').box({
				load: {
					loadTemplate: {
						template: 'account/message'
					}, 
					loadData: {
						url: 'account/load-items',
						params: {id: id},
						template: 'list-simple',
						dataType: 'list-simple'
					}
				},
				overlay: true,
				classCss: 'layer-message'
			});
	});	
	
	$('body').on('click', '.action-cliente-punto-vendita', function() {
		var id = $(this).tableSelectedId();
		if (id == null) {
			return;
		}
		$('<div/>').box({
				load: {
					loadTemplate: {
						template: 'account/puntovendita/index'
					}, 
					loadData: {
						url: 'account/puntovendita/list',
						params: {id: id},
						template: 'list',
						dataType: 'list'
					}
				},
				overlay: true,
				classCss: 'layer-big'
			});
	});
});