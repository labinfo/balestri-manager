/* 
 * Template-handling of images for forms etc.
 */

(function ($) {

})(jQuery);

var wwwImgPath = "/my/img/200x200/";
//var wwwImgPath2 = "/images/80x80/";
var dimens = ["1024x768", "320x240", "80x80"];

$(function () {
    $('body').on('upload-image-complete', function (e, data) {
        //Object {file: "21.png", forza_inserimento: ""}
        //console.log("filename: " +  data.file);
        var imageUrl = wwwImgPath + data.file;
//        console.log(imageUrl);
        $('#img_name').html(data.file);
        $('#img_frame').css('background-image', 'url(' + imageUrl + ')');
        $('#preview_list').html('');
        for (var i = 0; i < dimens.length; i++) {
            var content = '<li><a href="/my/img/' + dimens[i] + '/' + data.file + '">' + dimens[i] + '</a></li>';
//            console.log(content);
            $('#preview_list').append(content);
        }
        //set hidden input
        $("input[name='filename']").val(data.file);
//        $("#icon_small").attr("src", imageUrl);
    });

    $('body').on('upload-image-complete-multi', function (e, data) {
        //Object {file: "21.png", forza_inserimento: ""}
//        console.log("filename: " +  data.file);
        var imageUrl = wwwImgPath + data.file;
//        console.log(imageUrl);
        var imageFrame = getImageFrame(data.file, imageUrl);
        $("#imgs_container").append(imageFrame);
        imageFrame.click(selectImage);
        updateListImmagini();
    });

    $('body').on('click', '[data-tab-rif]', function () {
        //for image init
        $init = $("#imgs_container").hasClass('processed');
        //read saved info
        if (!$init) {
            var selected = $("input[name='immagine_principale']").val();
            //salvate in formato: img1.jpg,img2.png,etc..
            var str_imgs = $("input[name='immagini']").first().val();
            if(str_imgs == undefined || str_imgs == '')
                return;
            //console.log("json: " + str_imgs);
            $.each(str_imgs.split(','), function (index, value) {
                var imageUrl = wwwImgPath + value;
//                console.log(imageUrl);
                var imageFrame = getImageFrame(value, imageUrl);
                $("#imgs_container").append(imageFrame);
                imageFrame.click(selectImage);
                if (value === selected) {
                    imageFrame.addClass('selected');
                }
            });
            updateListImmagini();
            $('.button-remove').click(removeSelected);
            $("#imgs_container").addClass('processed');
        }
    });

//    <span id="img_frame" filename="" class="field-anteprima-immagine"
//                  style="background-image: url('[[?item.image]]
//                  [[=item.image.url.p1 || '']]
//                  [[?]]')">
//            </span>

    function getImageFrame(filename, imageUrl) {
        return $("<span></span>").attr('filename', filename)
                .addClass("field-anteprima-immagine")
                .css('background-image', 'url(' + imageUrl + ')');
    }

    function selectImage() {
        //remove selected attribute to everyone
        $("#imgs_container").children('.field-anteprima-immagine').each(function () {
            $(this).removeClass('selected');
        });
        $(this).addClass('selected');
        var filename = $(this).attr('filename');
//        console.log('selected ' + filename);
        updatePrincipale(filename);
    }

    function updateListImmagini() {
        var list = new Array();
        $("#imgs_container").children('.field-anteprima-immagine').each(function () {
            var filename = $(this).attr('filename');
            list.push(filename);
        });
        list = list.join(',');
//        console.log(list);
        $("input[name='immagini']").val(list);
    }

    function updatePrincipale(filename) {
        $("input[name='immagine_principale']").val(filename);
    }

    function removeSelected() {
        $("#imgs_container").children('.field-anteprima-immagine.selected').each(function () {
            $(this).remove();
            updateListImmagini();
            updatePrincipale(); //poiché il selezionato è anche la principale
        });
    }

});



