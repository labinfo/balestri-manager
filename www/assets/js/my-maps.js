var map = null;

(function ($) {
    $.fn.gmap = function () {
        var params = {
            draggable: false,
            zoom: 15
        };
        var thisElem = $(this);
        thisElem.addClass('map-init');
        //update params
        if ($(this).attr('data-draggable') != null
            && $(this).attr('data-draggable') != '') {
            params.draggable = true;
        }
        if ($(this).attr('data-zoom') != null && $(this).attr('data-zoom') != '') {
            params.zoom = parseInt($(this).attr('data-zoom'));
        }
        //
        map = thisElem;
        //*** Map object ***
        var Maps = {
            marker: null,
            updatePositionCoord: function (lat, lng) {
                thisElem.attr('data-latitudine', lat);
                thisElem.attr('data-longitudine', lng);
                $('#fields-container').trigger('update-coordinates', [lat, lng]);
            },
            setMarker: function (mrk) {
                if (Maps.marker != null) {
                    Maps.marker.setMap(null);
                }
                var pos = new google.maps.LatLng(mrk.latitudine, mrk.longitudine);
                Maps.bounds.extend(pos);
                Maps.marker = new google.maps.Marker({
                    position: pos,
                    map: Maps.map,
                    draggable: params.draggable
                });

                google.maps.event.addListener(Maps.marker, 'dragend', function () {
                    Maps.updatePositionCoord(this.getPosition().lat(), this.getPosition().lng());
                });
            },
            fitBounds: function () {
                Maps.map.fitBounds(Maps.bounds);
                setTimeout(function () {
                    Maps.map.setZoom(params.zoom);
                }, 200);
            },
            updateFromAdress: function (geocodeMe, isDefault) {
                //console.log('update form address: ' + geocodeMe);
                isDefault = isDefault == null ? false : isDefault;
                if (geocodeMe == null || geocodeMe == '') {
                    geocodeMe = 'Italia';
                    isDefault = true;
                }
                Maps.geocoder.geocode({'address': geocodeMe}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        Maps.updateFromCoord(results[0].geometry.location);
                        thisElem.trigger('update-map-from-address');
                    } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
                        console.log('error: can\'t find an address');
                    } else if (status == google.maps.GeocoderStatus.REQUEST_DENIED) {
                        console.log('error: request denied');
                    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                        console.log('error: over query limit');
                    } else {
                        console.log('error');
                        thisElem.trigger('update-map-from-address-error');
                    }
                });
            },
            updateFromCoord: function (position) {
                if (Maps.marker != null) {
                    Maps.marker.setMap(null);
                }
                Maps.map.setCenter(position);
                Maps.marker = new google.maps.Marker({
                    map: Maps.map,
                    draggable: params.draggable,
                    position: position
                });
                google.maps.event.addListener(Maps.marker, 'dragend', function () {
                    Maps.updatePositionCoord(this.getPosition().lat(), this.getPosition().lng());
                });
                Maps.updatePositionCoord(position.lat(), position.lng());
            }
        };

        var myOptions = {
            zoom: params.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };

        Maps.map = new google.maps.Map($(this)[0], myOptions);
        Maps.infowindow = new google.maps.InfoWindow();
        Maps.bounds = new google.maps.LatLngBounds();
        Maps.geocoder = new google.maps.Geocoder();

        //setup trigger handlers
        thisElem.on('update-map', function (e, opt, isDefault) {
            isDefault = isDefault == null ? false : isDefault;
            if (opt.latitudine && opt.longitudine) {
                var pos = new google.maps.LatLng(opt.latitudine, opt.longitudine);
                Maps.updateFromCoord(pos);
            } else {
                Maps.updateFromAdress(opt, isDefault);
            }
        });

        $('#fields-container').on('update-coordinates', function (e, lat, lng) {
            $("input[data-latitudine]").val(lat);
            $("input[data-longitude]").val(lng);
            setInputInit($("input[name='latitudine_fix']")).val(lat);
            setInputInit($("input[name='longitudine_fix']")).val(lng);
        });

        //update init data
        if (thisElem.attr('data-latitudine') != null) {
            thisElem.trigger('update-map', [{
                latitudine: thisElem.attr('data-latitudine'),
                longitudine: thisElem.attr('data-longitudine')
            }]);
        } else if (thisElem.attr('data-address') != null) {
            thisElem.trigger('update-map', [thisElem.attr('data-address')]);
        }
        thisElem.attr('data-init', 'true');

        //update if default value is present
        var lat = $("[data-latitudine]").first().val();
        var lon = $("[data-longitude]").first().val();
        if (lat && lon && lat !== "") {
            var coord = {
                'latitudine': lat,
                'longitudine': lon
            };
            thisElem.trigger('update-map', [coord, true]);
        } else {
            thisElem.trigger('update-map', ['Italia', true]);
        }
    };
})(jQuery);

$(function () {

    $('body').on('click', '#submit-address', function () {
        var text = $("input[name=address]").val();
        if ($.trim(text).length === 0) {
            text = getFormAddress();
        }
        map.trigger('update-map', [text, true]);
    });

    $('body').on('click', '#update-latlon', function () {
        var latitudine = $("input[name=latitudine_fix]").val();
        var longitudine = $("input[name=longitudine_fix]").val();
        var coord = {
            'latitudine': latitudine,
            'longitudine': longitudine
        };
        map.trigger('update-map', [coord, true]);
    });

    $('body').on('blur', '.autoupdate', function () {
        var lat = $("input[name=latitudine_fix]").val();
        var lng = $("input[name=longitudine_fix]").val();
        var coord = {
            'latitudine': lat,
            'longitudine': lng
        };
        if (map != null)
            map.trigger('update-map', [coord, true]);
        else
            $('#fields-container').trigger('update-coordinates', [lat, lng]);
    });

    //Search for this tag when data-tab is pressed
    $('body').on('click', '[data-tab-rif="posizione"]', function () {
        if ($('.autoload-google-map').length > 0) {
            if (!$('.autoload-google-map').hasClass('map-init'))
                $('.autoload-google-map').gmap();
            //center map if region is selected/present
            var text = $.trim(getFormAddress());
            var id = $("#item-id").val();
            var mapInit = $('#fields-container').hasClass('position-init');
            if (!id && text.length != 0 && !mapInit) {
                //console.log('update-map triggered');
                map.trigger('update-map', [text, true]);
                $('#fields-container').addClass('position-init');
            }
        }
    });

    $('body').on('click', '.action-submit-position-alert', function () {
        if ($(this).closest('form').length == 0) {
            return;
        }
        var $this = $(this);
        var check = $("input[data-longitude]").val() && $("input[data-latitudine]").val();
        if (check) {
            submitData();
        } else {
            var opt = {
                text: "Nessuna posizione specificata sulla mappa, continuare?"
                + " </br> <em>In caso di conferma, sarà utilizzata una posizione di default.</em>",
                onConfirm: function () {
                    submitData();
                }
            };
            H.confirm(opt);
        }
        //
        function submitData() {
            $this.closest('form')
                .submitFormData($this.actionParams());
        }
    });
});

//
function getFormAddress() {
    var text = '';
    var via = $("input[name='recapito_via']").val();
    var civico = $("input[name='recapito_num_civico']").val();
    var provincia = $("select[name='recapito_id_provincia'] option:selected").text();
    var comune = $("input[name='recapito_comune']").val();
    if ($.trim(comune) === '' && $.trim(provincia) != '')
        provincia = provincia.split('-')[0];
    if (!via && $.trim(via) != '')
        text += via;
    if ($.trim(civico) != '')
        text += ' ' + civico;
    if ($.trim(comune) != '')
        text += ' ' + comune;
    if ($.trim(provincia) != '')
        text += ' ' + provincia;
    //console.log(text);
    return text;
}

function setInputInit(elem) {
    $elem = $(elem);
    elem.parent().addClass('keypress');
    return $elem;
}