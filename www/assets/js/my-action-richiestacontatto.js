(function( $ ){

})( jQuery );

$(function(){	
	$('body').on('click', '.action-richiestacontatto-messaggio', function() {
		var opt = $(this).actionParams();
		var id = opt.id;
		if (id == null) {
			id = $(this).tableSelectedId();
			if (id == null) {
				return;
			}
		}
		$('<div/>').box({
				load: {
					loadTemplate: {
						template: 'richiestacontatto/message'
					}, 
					loadData: {
						url: 'richiestacontatto/load',
						params: {id: id},
						template: 'item',
						dataType: 'item'
					}
				},
				overlay: true,
				classCss: 'layer-message'
			});
	});
});