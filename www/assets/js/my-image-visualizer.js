/* 
 * simple view that shows a list of images
 */

var images = [];
var index = 0;

(function ($) {
	//extending jquery
	$.fn.image_visualizer = function (list, keepUrl) {
		$this = $(this); //element on which it was called
		if (list === undefined || list.length === 0)
			return;
		images = list;
		index = 0;
		//open box
		$('<div/>').box({
			load: {
				loadTemplate: {
					template: 'gallery/show',
					paramsTemplate: images[0]
				}
			},
			success: function () {
				updateImage(images[0], keepUrl);
			},
			overlay: true,
			classCss: 'layer-image'
		});
		setTimeout(function () {
			updateImage(images[0], true)
		}, 500);
	};

})(jQuery);

function updateImage(file, keepUrl) {
	var img = $('.image-box').find('img.image-frame');
	start_loading();
	if (keepUrl)
		$(img).attr('src', getImageUrl(file));
	else
		$(img).attr('src', file);
//	log('image updated to ' + file);
	$(img).one('load', function () {
		stop_loading();
	}).each(function () {
		if (this.complete)
			$(this).load();
	});
}

function getImageUrl(file) {
	return 'img/1024x768/' + file;
}

$(function () {

	$('body').on('click', '.arrow-right', function () {
		log('move right');
		index = (index + 1) % images.length;
		updateImage(images[index], true);
	});

	$('body').on('click', '.arrow-left', function () {
		log('move left');
		index--;
		if (index < 0)
			index = images.length - 1;
		updateImage(images[index], true);
	});

	$('body').on('click', '.open-visualizer', function (event) {
		event.preventDefault();
		var image = $(this).attr('href');
		var list = [image];
		$(this).image_visualizer(list, true);
	});

});


function start_loading(){
	$('.image-box .image-loading').show();
}

function stop_loading(){
	$('.image-box .image-loading').hide();
}

///
function log(str) {
	//console.log(str);
}

function printJson(value) {
	log(JSON.stringify(value));
}

