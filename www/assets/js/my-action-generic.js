(function( $ ){

})( jQuery );

$(function(){
	$('body').on('click', '.action-stampa-tessera', function() {
		var opt = $(this).actionParams();
		if (opt.id == null) {
			opt.id = $(this).tableSelectedId();
		}
		if (opt.id == null) {
			return;
		}
		$(this).download({
			url: opt.url,
			params: {
				id: opt.id
			}
		});
	});	
	
	$('body').on('click', '.action-info-item-all', function() {
		var opt = $(this).actionParams();
		if (opt.id == null) {
			opt.id = $(this).tableSelectedId();
		}
		var box = $('<div/>').box({
				load: {
					loadTemplate: {
						template: 'info/list',
						paramsTemplate: opt
					}, 
					loadData: {
						url: 'info/list',
						params: opt,
						template: 'list',
						dataType: 'list'
					},
					success: function() {
						box.find('.action-load-data').actionParams('data-load-params', opt);
					}
				},
				overlay: true,
				classCss: 'layer-info'
			});
	});
	
	$('body').on('click', '.action-info-item', function() {
		var $this = $(this);
		var isShow = $this.hasClass('show');
		$('body').trigger('hide-on-click');
		if (isShow) {
			return;
		}
		$this.addClass('show');
		var params = {
			menu: true, 
			parent: $this.parent(),
			onClose: function() {
				$this.removeClass('show');
			}
		};
		var cont = $this.find('.item-info-content');
			
		if (cont.length == 0) {
			var id = $this.tableSelectedId();
			if (id == null) {
				return;
			}
			params.content = '<div style="text-align: center;"><img src="/assets/img/loading.gif"></div>';
			var completa = function () {
				var opt = $this.actionParams();
				box.loadTemplateAndData({
						loadTemplate: {
							template: 'info/item'
						},
						loadData: {
							url: 'info/load', 
							params: {id: id, type: opt.type}
						}
					});
			};		
		}
		else {
			params.content = $this.html();
			var completa = function () {};
		}
		var box = $('<div/>').box(params)
			.css({right: '10px', top: '30px'});
		box.dataLoaded();
		completa();
	});
	$('body').on('click', '.action-template', function() {
		var opt = $(this).actionParams();
		opt.css = opt.css == null ? 'layer-small-edit' : opt.css;
		opt.cache = opt.cache == null ? false : opt.cache;
		$('<div/>').box({
				load: {
					loadTemplate: {
						paramsLoad: opt.paramsLoad, 
						paramsTemplate: opt.paramsTemplate, 
						template: opt.template, 
						cache: opt.cache, 
						render: true
					}
				},
				overlay: true,
				classCss: opt.css
			});
	});
	$('body').on('change', '.action-select-onchange-update', function() {
		var opt = $(this).actionParams();
		var update = $(opt.update);
		update.val('');
		update.find('option').css({display: 'none'});
		update.find('option').first().css({display: 'block'});
		update.closest('.keypress').removeClass('keypress');
		var val = $(this).val();
		if (val != '') {
			update.find('option[data-rif-id=' + val + ']').css({display: 'block'});
		}
	});
	$('body').on('click', '.action-template-load', function() {
		var opt = $(this).actionParams();
		opt.css = opt.css == null ? 'layer-small-edit' : opt.css;
		$('<div/>').box({
				load: opt,
				overlay: true,
				classCss: opt.css
			});
	});

	$('body').on('dblclick', '.action-update-dbl-cell .dbl-cell', function() {
		$(this).closest('.action-update-dbl-cell').boxTableSelectedId('action-update');
	});
	$('body').on('dblclick', '.action-update-dbl', function() {
		$(this).closest('.action-update-dbl').boxTableSelectedId('action-update');
	});
	$('body').on('click', '.action-update', function() {
		$(this).boxTableSelectedId('action-update');
	});
	function action_details($this) {
		var opt = $this.actionParams();
		if (opt['action-details']) {
			opt = opt['action-details'];
		}
		var success = null;
		if (opt.update) {
			success = function() {
				if (opt.update.params) {
					var params = opt.params;
				}
				else {
					var params = {id: $this.tableSelectedId()};
				}
				$.xajax('/my/data/' + opt.update.url, {
						data: params,
						noServeErrorMessage: true,
						success: function(data) {
							if (data.data) {
								$('[data-update-content=' + opt.update.type + ']').mergeTemplateData(data.data, {insertType: 'replace_prepend', lightEffect: false});
								$('body').trigger('need-update-info');
							}
						}
					});				
			};
		}
		$this.boxTableSelectedId({key_opt: 'action-details', success: success});		
	}
	$('body').on('click', '.action-details', function() {
		action_details($(this));
	});
	$('body').on('click', '.action-details-cell .details-cell', function() {
		action_details($(this).closest('.action-details-cell'));
	});
	$('body').on('click', '.action-template-load-selected-id', function() {
		$(this).boxTableSelectedId();
	});

	$('body').on('click', '.action-ripristina', function() {
		var opt = $(this).actionParams();
		$(this).actinOnImtemsTable({
			url: '/my/data/' + opt.url,
			title: opt.title,
			text: {
				item: 'Procedere il ripristino della voce selezionata?',
				items: 'Procedere il ripristino delle voci selezionate?'
			},
			callback: 'update',
			type: opt.type
		});
	});	

	$('body').on('click', '.action-abilita', function() {
		var opt = $(this).actionParams();
		opt.callback = opt.callback == null ? 'update' : opt.callback;
		$(this).actinOnImtemsTable({
			params: {abilita:opt.abilita},
			url: '/my/data/' + opt.url,
			title: opt.title,
			text: {
				item: 'Procedere con ' + (opt.abilita ? 'l\'abilitazione' : 'il blocco') + ' della voce selezionata?',
				items: 'Procedere con ' + (opt.abilita ? 'l\'abilitazione' : 'il blocco') + ' delle voci selezionate?'
			},
			callback: opt.callback,
			type: opt.type
		});
	});
	
	$('body').on('click', '.action-delete', function() {
		var opt = $(this).actionParams();
		$(this).actinOnImtemsTable({
			url: '/my/data/' + opt.url,
			title: opt.title,
			text: {
				item: 'Procedere con la cancellazione della voce selezionata?',
				items: 'Procedere con la cancellazione delle voci selezionate?'
			},
			callback: 'delete'
		});
	});
	
	$('body').on('click', '.action-confirm', function() {
		var opt = $(this).actionParams();
		$(this).actinOnImtemsTable({
			paramId: opt.paramId,
			url: '/my/data/' + opt.url,
			title: opt.title,
			text: {
				item: opt.text,
				items: opt.text,
			},
			callback: opt.callback
		});
	});
	
	$('body').on('click', '.action-execute', function() {
		var opt = $(this).actionParams();
		opt.url = '/my/data/' + opt.url;
		$(this).actinOnImtemsTable(opt);
	});
});