$(function(){	
	$('body').on('update-nome-utente', function(e, data) {
		if (data.nome &&
			data.cognome) {
			$('.menu-user-name-data').html(data.nome + ' ' + data.cognome);
			$('.menu-user-benvenuto').html('Benvenuto ' + data.nome + ' ' + data.cognome);
		}
	});
});