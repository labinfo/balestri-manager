var Form = {
	labels: {
		fileTroppoGrande: 'La dimensione del file è troppo grande',
		erroreCaricamentoFile: 'Errore in fase di caricamento file, impossibile procedere',
		formatoFileNonValido: 'Formato file non valido, sono accettate le seguenti estensioni: ',
		defaultServerError: 'Server non raggiungibile o errore interno, riprovare più tardi.',
		defaultError: 'Errore interno, impossibile procedere',
		credenzialiNonValide: 'Accesso non valido, non possiedi le credenziali necessarie per eseguire l\'operazione',
		campiInRossoConErrori: 'I campi evidenziati in rosso contengono degli errori',
		azioneNonEseguita: 'Errore durante l\'esecuzione dell\'azione',
		azioneEseguita: 'Azione eseguita correttamente'
	}
};

H.data.onLoaded('field-data', '.input-datepicker',
	function($this) {
		$this.datepicker({
			format: 'dd/mm/yyyy',
			weekStart: 1
		}).on('changeDate', function() {
			$(this).parent().addClass('keypress');
			if ($(this).attr('data-date-viewmode') == null ||
				$(this).attr('data-date-viewmode') == '') {
				$(this).datepicker('hide');
			}
			else {
				var count = $(this).attr('data-count-click');
				if (count == null || count == '') {
					count = '0'
				}
				
				var day = $(this).val();
				if (day != '') {
					day = day.split('/');
					day = day[0];
				}
				var oldDay = $(this).attr('data-value-day');
				if (oldDay == null || oldDay == '') {
					$(this).attr('data-value-day', day);
				}
				count = parseInt(count) + 1;
				if (count >= 3 && oldDay != day) {
					$(this).datepicker('hide');
					count = 0;
					$(this).attr('data-value-day', '')
				}
				$(this).attr('data-count-click', count);
			}
		}).on('show', function() {
			$(this).closest('.field-line').addClass('focus');
		}).on('hide', function() {
			$(this).closest('.field-line').removeClass('focus');
		});
	});
	
H.data.onLoaded('field-keypress', '.field input[type=text], .field input[type=password], .field textarea, .field select',
	function($this) {
		var valore = H.utils.trim($this.val());
		if (valore.length == 0) {
			$this.parent().removeClass('keypress');
			return;
		}
		$this.parent().addClass('keypress');
	},
	true);
	
H.data.onLoaded('field-tips', '[data-tips]',
	function($this) {
		var closest = $this.closest('.field-line,.field-line-checkbox');
		closest.addClass('field-info');
		closest.append('<div class="field-info-ico"><span>' + $this.attr('data-tips') + '</span></div>');
	});
	
H.data.onLoaded('field-selected-value', 'select[selected-value]',
	function($this) {
		if ($this.attr('selected-value') != '') {
			$this.val($this.attr('selected-value'));
			$this.parent().addClass('keypress');
		}
	},
	true);
H.data.onLoaded('field-update-with', '.update-with-value', function($this) {
		var opt = $this.actionParams();
		if (opt.updateWithValue == null || opt.updateWithValue.content == null) {
			return;
		}
		var cont = $this.closest('.window').find('[data-content-update-with-value=' + opt.updateWithValue.content + ']');
		if (cont.length == 0) {
			return;
		}
		function update() {
			var value = $this.val();
				if (value != null && value != '') {
				cont.find('.content-update-with-value').each(function() {
					$(this).find('[data-update-with-value]').css({display: 'none'});
					var uwv = $(this).find('[data-update-with-value=' + value + ']');
					if (uwv.length == 0) {
						uwv = $(this).find('[data-update-with-value=all]');
						if (uwv.length == 0) {
							uwv = $(this).find('[data-update-with-value=void]');
						}
					}
					uwv.css({display: 'block'});
				});
			}
		}
		update();
		$this.on('change', update);
	}, 
	true);
H.data.onLoaded('field-checkbox-value', '[checkbox-value]', function($this) {
		var list = $this.attr('checkbox-value');
		if (list == null || list == '') {
			return;
		}
		list = list.split(',');
		for (var i = 0; i < list.length; i++) {
			$this.find('.field-line-checkbox [data-value=' + list[i] + ']').click_checkbox(null, false);
		}
	}, true);
(function( $ ){
	$.fn.submitFormData = function(opt) {
		var form = $(this);
		if (form.hasClass('form-loading')) {
			return;
		}
		if (opt == null) {
			opt = $this.actionParams();
			if (opt == null) {
				return;
			}
		}
		form.removeClass('form-loading');
		form.removeClass('form-error');
		form.find('.field-error').removeClass('field-error');
				
		function submitForm(url) {
//                    console.log('request to : ' + url);
			form.addClass('form-loading');
			var multipleFields = [];
			form.find('[data-multiple-fields]').each(function() {
				multipleFields.push($(this).attr('data-multiple-fields'));
			});
			if (multipleFields.length > 0) {
				multipleFields = multipleFields.join(',');
				form.find('[name=multiple_fields_references]').remove();
				form.append('<input name="multiple_fields_references" type="hidden" value="' + multipleFields + '">');
			}
			
			opt.method = opt.method == null ? 'json' : opt.method;
			switch(opt.method) {
			case 'get':
			case 'post':
				form.attr('onsubmit', '');
				form.attr('method', opt.method);
				form.attr('action', url);
				form.submit();
				break;
			default:
				$.xajax(url, {
					data: form.serializeObject(),
					hideLoading: function() {
						form.removeClass('form-loading');
					},
					success: function (data) {
						form.removeClass('form-loading');
						if (data.success) {	
							if (opt.successCallback) {
								opt.successCallback(data);
							}
							else {
								form.successSubmit(opt, data);
							}
						}
						else {
							if (opt.errorCallback) {
								opt.errorCallback(data);
							}
							else {
								form.errorSubmit(opt, data);
							}
						}
					},
					error: function() {
						if (opt.errorCallback) {
							opt.errorCallback();
						}
						else {
							form.errorSubmit(opt);
						}
					}
				});	
			}
        }

        opt.validaForm = opt.validaForm == null ? true : opt.validaForm;
        if (!opt.validaForm || H.validator.checkIsValidForm(form)) {
            if (opt.confirm) {
                H.confirm(opt.confirm, function() {
                    if(opt.urls){
                        for(i=0; i<opt.urls.length; i++)
                            submitForm(opt.urls[i]);
                    }
                    else{
                        submitForm(opt.url);
                    }
                });
            }
            else {
                if(opt.urls){
                    for(i=0; i<opt.urls.length; i++)
                        submitForm(opt.urls[i]);
                }
                else{
                    submitForm(opt.url);
                }
            }
        }
        else {
            form.formErrorMessage(Form.labels.campiInRossoConErrori);
		}	
	};
	
	$.fn.reset = function() {	
		return $(this).each(function() {
			var form = $(this);
			form.find('.checkbox-checked').removeClass('checkbox-checked');
			form.find('input,select,textarea').each(function() {
				var $this = $(this);
				var val = $this.attr('data-default-value');
				if (val == null) {
					val = '';
				}
				$this.val(val);
				$this.parent().removeClass('keypress');
				if ($this.closest('.checkbox').length > 0 && val != '') {
					$this.closest('.checkbox').addClass('checkbox-checked');
				}
			});
		});
	};
	
	$.fn.upload = function(opt) {
		var $this = $(this);	
		
		var id = H.id();
		var html = [
			'<form id="',id ,'" enctype="multipart/form-data" style="display: none;">',
			'<input name="file" type="file" />',
			'</form>'
		];	
		var cont = opt.cont == null ? $this.closest('.layer') : $(opt.cont);
		if (cont.length == 0 || cont.hasClass('layer-menu')) {
			cont = $('body');
		}
		cont.append(html.join(''));	
		var form = $('#' + id);
		var inputField = form.find('input[name=file]');
		
		if (opt.select) {
			var sel = $(this).tableSelectedId();
			if (sel == null) {
				return;
			}
			opt.params = opt.params == null ? {} : opt.params;
			opt.params.id = sel;
		}
		if (opt.params) {
			for (var key in opt.params) {
				if (opt.params.hasOwnProperty(key)) {
					if (opt.params[key]) {
						var input = $('<input type="hidden" name="' + key + '"/>');
						input.val(opt.params[key]);
						form.append(input);
					}
				}
			}
		}
		
		inputField.fileupload({
			url: opt.url,
			dataType: 'json',
			add: function (e, data) {
				var valido = true;
				var fileName = inputField.val();
				if (opt.fileType) {
					var i = fileName.lastIndexOf('.');
					if (i > 0) {
						var estensione = fileName.substr(i+1).toLowerCase();
						var trovato = false;
						for (var i = 0; i < opt.fileType.length && !trovato; i++) {
							if (opt.fileType[i] == estensione) {
								trovato = true;
							}
						}
						if (!trovato) {
							H.message.error(Form.labels.formatoFileNonValido + opt.fileType.join(', '));
							valido = false;
						}
					}
					else {
						valido = false;
					}
				}
				if (valido) {
					var html = [
						'<div>',
						'<div class="progress">',
							'<div class="progress-line-wrapper">',
								'<div class="progress-line"></div>',
							'</div>',
							'<div class="progress-text">0%</div>',
							'<div class="progress-details"></div>',
						'</div>',
						'</div>'
					];
					
					opt.progress = $(html.join('')).box({
						overlay: true,
						classCss: 'layer-upload',
						onClose: function() {
							data.abort();
						}
					});
					data.submit();
				}
				else {
					data.abort();
				}
			},      
			start: function (e) {
			}, 			
			fail: function (e) {
				form.errorSubmit(opt);
				H.message.error(Form.labels.erroreCaricamentoFile);
				if (opt.progress != null) {
					opt.progress.box(false);
				}
				form.remove();
			}, 
			done: function (e, data) {
				data = data.result;
				if (data.session_fault || (data.data && data.data.session_fault)) {
					H.message.error('Sessione scaduta.');
					H.loading.show();
					H.cookie.set('callback_message_error', 'Sessione scaduta o utente entrato senza autorizzazioni');
					location.href = '/index';
					return;
				}
				if (data.success) {
					if (data.data) {
						var cont = $this.closest('.field-line-image');
						if (cont.length > 0 && data.data.tempThumbImage) {
							cont.addClass('field-anteprima-show');
							cont.find('.field-anteprima').attr('src', data.data.tempThumbImage);
							cont.find('.field-image').val(data.data.tempImage);
							cont.find('.field-image').trigger('blur');
						}
						else{
							cont = $this.closest('.field-line-file');
							if (cont.length > 0 && data.data.fileName) {						
								cont.addClass('field-is-file');
								cont.find('.field-anteprima').html(data.data.fileName);
								cont.find('.field-file').val(data.data.tempFile);
								cont.find('.field-file').trigger('blur');
								cont.find('.field-file-name').val(data.data.fileName);
							}
						}
					}
					if (opt.success) {
						opt.success(data);
					}
					form.successSubmit(opt, data);					
				}
				else {
					form.errorSubmit(opt, data);
					if (data.message == null) {
						H.message.error(Form.labels.fileTroppoGrande);
					}
					else {
						H.message.error(data.message);
					}
				}
				if (opt.progress != null) {
					opt.progress.box(false);
				}
				form.remove();
			},
			progressall: function (e, data) {
				var perc = Math.round(data.loaded * 100 / data.total);
				if (perc > 100) {
					perc = 100;
				}
				$('.progress-line').css({width: perc + '%'});
				$('.progress-text').html(perc + '%');
				$('.progress-details').html(data.loaded + '/' + data.total + ' byte');
			}
		});
		inputField.click();		
	};

	$.fn.click_checkbox = function(e, fireEvent) {
		fireEvent = fireEvent == null ? true : fireEvent;
		var val = '';
		
		function clearSelected(g) {
			var checked = g.find('.checkbox-checked');
			checked.find('input').val('');
			checked.removeClass('checkbox-checked');
			checked.trigger('checkbox-change', [checked]);		
		}
		
		var group = $(this).closest('.checkbox-group');
		if (group.length > 0) {
			if ($(this).hasClass('checkbox-checked')) {
				return;
			}
			else {
				clearSelected(group);
			}
		}
		
		var checkboxChecked = $(this).hasClass('checkbox-checked');
		group = $(this).closest('.table-single-selection');
		if (group.length > 0) {
			if (!$(this).hasClass('checkbox-checked')) {
				clearSelected(group);
			}
		}
		else {
			group = $(this).closest('.table-single-selection-shift');
			if (group.length > 0) {
				if (e != null && e.ctrlKey) {
					//seleziona altre check!
				}
				else {
					clearSelected(group);
				}
			}
		}
		
		if (checkboxChecked) {
			$(this).removeClass('checkbox-checked');
		}
		else {
			$(this).addClass('checkbox-checked');
			val = $(this).attr('data-value');
		}
		$(this).find('input').val(val);
		
		if (fireEvent) {
			$(this).trigger('checkbox-change', [$(this)]);
		}

		//## Added continuation to update other forms in the process
		var parent = $(this).closest('.update-with-data');
		if(parent.length){
			var opt = $.parseJSON(parent.attr('data-action'));
			var content = opt.updateWithData.content;
			var data = parent.closest('.field-group-body')
				.find('[data-value]')
				.filter('.checkbox-checked')
				.map(function(){return $(this).attr('data-value');}).get();
			var who = parent.closest('.window-filter')
				.find('[data-content-update-with-data='+content+']');
			if(who.length)
				who.updateWithData(data);
		}
	};

	//## update only to show selected content
	$.fn.updateWithData = function(data){
		var elem = $(this);
		if(!$.isArray(data)){
			data = [data];
		}
		if(data.length == 0){
			elem.find('[data-id-data]').show();
		} else {
			//console.log(JSON.stringify(data));
			elem.find('[data-id-data]').hide();
			//clear checkboxes if any
			var checked = elem.find('.checkbox-checked');
			checked.find('input').val('');
			checked.removeClass('checkbox-checked');
			checked.trigger('checkbox-change', [checked]);
			$.each(data, function (index, value){
				elem.find('[data-id-data='+value+']')
					.show();
			});
		}
	}

	$.fn.formErrorMessage = function(text) {
		var form = $(this);
		
		if (text == null || text.length == 0) {
			text = Form.labels.defaultServerError;
		}
		form.removeClass('form-loading');
		form.find('.form-error-message').html(text);
		form.addClass('form-error');
		H.message.error(text);
	};

	$.fn.errorSubmit = function(opt, data) {
		var form = $(this);
		form.removeClass('form-loading');
		if (opt.onError != null) {
			if (opt.onError.event) {
				var res = null;
				if (data != null) {
					res = data.data;
				}
				$('body').trigger(opt.onError.event, [res]);
			}		
		}
		if (data == null) {
			form.formErrorMessage();
			return;
		}
		if (data.fields_errors) {
			form.addClass('form-error');
			var fields = data.fields_errors;
			if (data.message == null || data.message == '') {
				data.message = Form.labels.campiInRossoConErrori;
			}
			form.formErrorMessage(data.message);
			for (var key in fields) {
				if (fields.hasOwnProperty(key)) {
					var field = form.find('[name=' + key + ']');
					var wrapper = field.closest('.field-line');
					var text = wrapper.find('.error-text');
					if (text) {
						text.remove();
					}
					wrapper.addClass('field-error');
					wrapper.append('<span class="error-text">' + fields[key] + '</span>');
				}
			}
		}
		else {
			form.formErrorMessage(data.message);
		}	
	};
	
	$.fn.successSubmit = function(opt, data) {
		var form = $(this);
		H.message.confirm(data.message);
		if (opt.onComplete == null) {
			form.removeClass('form-loading');
			$('html, body').animate({scrollTop:0}, 200);
			return;
		}
		if (opt.onComplete.event) {
			$('body').trigger(opt.onComplete.event, [data.data]);
		}
		if (opt.onComplete.update) {
			$('[data-update-content=' + opt.onComplete.update + ']').mergeTemplateData(data.data, {insertType: 'replace_prepend', lightEffect: true});
		}
		if (opt.onComplete.light) {
			if (opt.onComplete.from) {
				var list_id = $(opt.onComplete.from).selectionInList({no_message: true, join: false});
			}
			else {
				var list_id = [opt.onComplete.id];
			}
			if (list_id != null) {
				for (var i = 0; i < list_id.length; i++) {
					$('[data-update-content=' + opt.onComplete.light + '] tr[data-id=' + list_id[i] + ']').light();
				}
			}
		}
		if (opt.onComplete.reload) {
			$('[data-update-content=' + opt.onComplete.reload + ']').loadData({empty: true});;
		}
		switch(opt.onComplete.type) {
		case 'reload':
			opt.onComplete.url = '' + location.href;
		case 'url':	
			H.loading.show();
			location.href = opt.onComplete.url;
			return;
		case 'reset':
			form.reset();
			return;
		case 'show':
			$(opt.onComplete.hide).css({display: 'none'});
			$(opt.onComplete.show).fadeIn(500);
			form.removeClass('form-loading');
			if (!$(opt.onComplete.hide).hasClass('hide-no-scroll')) {
				$('html, body').animate({scrollTop:0}, 200);
			}
			return;
		case 'close-win':
			form.closest('.layer').box(false);
			return;
		}
	};
})( jQuery );

$(function(){
	$('body').on('click','.action-upload-file-reset',function() {
		var $this = $(this);
		var cont = $(this).closest('.field-line-file');
		cont.find('.field-file').val('');
		cont.find('.field-file-name').val('');
		cont.find('.field-anteprima').html('');
		cont.removeClass('field-is-file');
	});
	$('body').on('click','.action-upload-file',function() {
		$(this).upload($(this).actionParams());
	});
	
	$('body').on('click', '.action-reset-form', function() {
		$(this).closest('form').reset();
	});
	
	$('body').on('change', 'select', function() {
		if ($(this).val() == '') {
			$(this).removeClass('sel');
		}
		else {
			$(this).addClass('sel');
		}
	});
	
	$('body').on('change', '.field select', function() {
		if ($(this).val() == '') {
			$(this).parent().removeClass('keypress');
		}
		else {
			$(this).parent().addClass('keypress');
		}
	});
	$('body').on('focus', '.field select', function() {
		$(this).parent().addClass('keypress');
	});
	$('body').on('blur', '.field select', function() {
		if ($(this).val() == '') {
			$(this).parent().removeClass('keypress');
		}
	});
	$('body').on('keyup', '.field input[type=text], .field input[type=password], .field textarea', function() {
		var valore = H.utils.trim($(this).val());
		if (valore.length == 0) {
			$(this).parent().removeClass('keypress');
			$(this).val(valore);
		}
		else {
			$(this).parent().addClass('keypress');
		}
	});
	$('body').on('keyup', '.field [data-maxlength]', function() {
		var max = $(this).attr('data-maxlength');
		if (max != null && max != '') {
			max = parseInt(max);
			if (max > 0 && valore.length > max) {
				$(this).val(valore.substr(0, max));
			}
		}
	});
	$('body').on('focus', '.field input[type=text], .field input[type=password], .field textarea', function() {
		$(this).parent().addClass('focus');
	});
	$('body').on('blur', '.field input[type=text], .field input[type=password], .field textarea', function() {
		$(this).parent().removeClass('focus');
	});
	
	$('body').on('click', '.field-line-checkbox label', function() {
		$(this).parent().find('.checkbox').click();
	});
		
	$('body').on('click', '.checkbox', function(e) {
		e.preventDefault();
		$(this).click_checkbox(e);
		return false;
	});	
	
	$('body').on('change', '.select-group', function(e) {
		var closest = $(this).closest('.select-group-wrapper');
		closest.find('[data-select-group]').css({display: 'none'});
		var val = $(this).val();
		if (val == '') {
			val = -1;
		}
		closest.find('[data-select-group=' + val + ']').css({display: 'block'});
	});	
			
	$('.action-return-submit').each(function() {
		var $this = $(this);
		var form = $this.closest('form');
		form.attr('onsubmit', 'return false;');
		$this.parent().append('<input type="image" src="/assets/img/blank.png">');
		form.submit(function() {
			if (form.attr('onsubmit') != '') {
				$this.click();
			}
		});
	});		
	
	$('body').on('click','.action-submit-form',function() {
		if ($(this).closest('form').length == 0) {
			return;
		}
		$(this).closest('form')
			.submitFormData($(this).actionParams());
	});
});