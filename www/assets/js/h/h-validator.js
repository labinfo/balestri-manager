H.validator = {
	validatorType: [
			'orasimple',
			'datasimple',
			'email',
			'emailsuper',
			'nickname',
			'codice',
			'password',
			'confermapassword',
			'telefono',
			'piva',
			'codicefiscale',
			'euro',
			'float',
			'url',
			'rgb'
		],
	labels: {
		urlNonValido: 'Indirizzo non valido, inserire ad esempio http://www.nomedominio.it',
		floatNonValido: 'Inserire un numero decimale valido #.##',
		euroNonValido: 'Inserire un valore in euro corretto #,##',
		orasimpleNonValida: 'Orario non vlido, inserire hh:mm',
		telefonoNonValido: 'Il numero di telefono deve essere nel seguente formato +##-####-######',
		pivaNonValido: 'Partita IVA non valida',
		codiceFiscaleNonValido: 'Codice fiscale non valido',
		erroreServer: 'Errore interno impossibile procedere',
		campiEvidenziatiInRossoContengonoErrori: 'I campi evidenziati in rosso contengono degli errori, impossibile procedere con l\'invio dei dati',
		campoObbligatorio: 'Campo obbligatorio',
		indirizzoEmailNonValido: 'Indirizzo di posta elettronica non valido',
		codiceNonValido: 'Codice non valido, &egrave; possibile inserire solo lettere, numeri e i caratteri _ (underscore), - (trattino)',
		nicknameNonValido: 'Nickname non valido, &egrave; possibile inserire solo lettere, numeri e i caratteri _ (underscore), - (trattino)',
		nicknameCorto: 'Il nickname deve essere di almeno 2 caratteri',
		passwordNonValida: 'Non sono ammessi spazi all\'interno della password',
		passwordCorta: 'La password deve essere lunga almeno 8 caratteri',
		dataNonValida: 'La data inserita non &egrave; valida',
		rgbNonValido: 'Valore colore RGB non valido',
		passwordCaratteri: 'La password deve contenere almeno una lettera maiuscola, una lettera minuscola e un numero',
		confermaPasswordNonValida: 'Conferma password non corrispondente'
	}
};

H.validator.action = {
	valida: function(field, validator) {
		field = $(field);
		if (field.attr('type') == 'file') {
			var val = field.val();
		}
		else {
			var val = H.utils.trim(field.val());
			field.val(val);
		}
		var wrapper = field.closest('.field-line');
		var resp = H.validator.action['check_' + validator](field, field.val(), wrapper);
		return H.validator.action.fieldMessage(wrapper, resp);
	},
	fieldMessage: function(wrapper, resp) {
		if (resp.valido) {
			wrapper.removeClass('field-error');
			return true;
		}
		else {
			if (resp.msg != null) {
				var text = wrapper.find('.error-text');
				if (text) {
					text.remove();
				}
				wrapper.addClass('field-error');
				wrapper.append('<span class="error-text">' + resp.msg + '</span>');
			}
			return false;
		}
	},
	check_required: function(field, value, wrapper) {
		if (wrapper.hasClass('field-error')) {
			return {valido:false};
		}
		if (value == '') {
			return {valido:false, msg: H.validator.labels.campoObbligatorio};
		}
		if (field.attr('type') == 'checkbox') {
			if (field.hasClass('valida-singlecheck')) {
				if (field.closest('.field-line').find('input.valida-singlecheck:checked').length == 0) {
					return {valido:false, msg: H.validator.labels.campoObbligatorio};
				}
			}
			else {
				if (!field.prop('checked')) {
					return {valido:false, msg: H.validator.labels.campoObbligatorio};
				}
			}
		}
		return {valido:true};	
	},
	formatUrl: function(field, value) {
		var url = value.toLowerCase();
		if (url.indexOf('http://') == 0 || url.indexOf('https://') == 0 || url.indexOf('ftp://') == 0) {
			return value;
		}
		value = 'http://' + value;
		if (field != null) {
			field.val(value);
		}
		return value;
	},
	check_url: function(field, value, wrapper) {
		if (value != '') {
			value = H.validator.action.formatUrl(field, value);
			var check = /(((https?)|(ftp)):\/\/([\-\w]+\.)+\w{2,3}(\/[%\-\w]+(\.\w{2,})?)*(([\w\-\.\?\\\/+@&#;`~=%!]*)(\.\w{2,})?)*\/?)/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.urlNonValido};
			}
		}
		return {valido:true};
	},	
	check_float: function(field, value, wrapper) {
		if (value != '') {
			var check = /^(-)?[0-9]+(\.)?[0-9]*$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.floatNonValido};
			}
		}
		return {valido:true};
	},
	check_euro: function(field, value, wrapper) {
		if (value != '') {
			var check = /^[0-9]+$/i;
			if (check.test(value)) {
				return {valido:true};
			}
			value = value.replace('\.', '');
			check = /^(-)?[0-9]+,?[0-9]{2}$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.euroNonValido};
			}
		}
		return {valido:true};
	},
	check_orasimple: function(field, value, wrapper) {
		if (value != '') {
			var check = /^[0-9]{1,2}(:)[0-9]{1,2}/i;
			if (value.length != 5 || !check.test(value)) {
				return {valido:false, msg: H.validator.labels.orasimpleNonValida};
			}
			var part = value.split(':');
			part[0] = parseInt(part[0]);
			part[1] = parseInt(part[1]);
			if (part[0] < 0 || part[0] > 23 || part[1] < 0 || part[1] > 59) {
				return {valido:false, msg: H.validator.labels.orasimpleNonValida};
			}
		}
		return {valido:true};	
	},
	check_telefono: function(field, value, wrapper) {
		if (value != '') {
			var check = /^\+[0-9]{2,3}-[0-9]{2,5}-[0-9]{2,12}$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.telefonoNonValido};
			}
		}
		return {valido:true};
	},
	check_piva: function(field, value, wrapper) {
		if (value != '') {
			var check = /^[a-zA-Z]*[0-9]+$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.pivaNonValido};
			}
		}
		return {valido:true};
	},
	check_codicefiscale: function(field, value, wrapper) {
		if (value != '') {
			var check = /^([A-Za-z]{6}[0-9lmnpqrstuvLMNPQRSTUV]{2}[abcdehlmprstABCDEHLMPRST]{1}[0-9lmnpqrstuvLMNPQRSTUV]{2}[A-Za-z]{1}[0-9lmnpqrstuvLMNPQRSTUV]{3}[A-Za-z]{1})|([0-9]{11})$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.codiceFiscaleNonValido};
			}
		}
		return {valido:true};
	},
	check_email: function(field, value, wrapper) {
		if (value != '') {
			var check = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.indirizzoEmailNonValido};
			}
		}
		return {valido:true};
	},
	check_emailsuper: function(field, value, wrapper) {
		if (value != '' && value != 'superadmin') {
			var check = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.indirizzoEmailNonValido};
			}
		}
		return {valido:true};
	},
	check_nickname: function(field, value, wrapper) {
		if (value != '') {
			if (value.length < 2) {
				return {valido:false, msg: H.validator.labels.nicknameCorto};
			}
			var check = /^[a-zA-Z0-9\_\-]+$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.nicknameNonValido};
			}
		}
		return {valido:true};
	},	
	check_rgb: function(field, value, wrapper) {
		if (value != '') {
			var check = /^[0-9abcdefABCDEF]+$/i;
			if (!check.test(value) || value.length != 6) {
				return {valido:false, msg: H.validator.labels.rgbNonValido};
			}
		}
		return {valido:true};
	},
	check_codice: function(field, value, wrapper) {
		if (value != '') {
			var check = /^[a-zA-Z0-9\_\-]+$/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.codiceNonValido};
			}
		}
		return {valido:true};
	},
	check_password: function(field, value, wrapper) {
		if (value != '') {
			if (value.length < 8) {
				return {valido:false, msg: H.validator.labels.passwordCorta};
			}
			if (value.indexOf(' ') >= 0) {
				return {valido:false, msg: H.validator.labels.passwordNonValida};
			}
			
			function isCarattere(str, check) {
				for (var i = 0; i < str.length; i++) {
					if (check.indexOf(str.charAt(i)) >= 0) {
						return true;
					}
				}
				return false;
			}
			function noCarattere(str, check) {
				for (var i = 0; i < str.length; i++) {
					if (check.indexOf(str.charAt(i)) < 0) {
						return true;
					}
				}
				return false;
			}
			
			if (!isCarattere(value, 'ABCDEFGHILMNOPQRSTUVZXWYJK') ||
				!isCarattere(value, 'abcdefghilmnopqrstuvzxwyjk') ||
				!isCarattere(value, '0123456789')) {
				return {valido:false, msg: H.validator.labels.passwordCaratteri};
			}
		}
		return {valido:true};
	},
	check_datasimple: function(field, value, wrapper) {
		if (value != '') {
			var check = /^[0-9]{1,2}(-|\/)[0-9]{1,2}(-|\/)[0-9]{4}/i;
			if (!check.test(value)) {
				return {valido:false, msg: H.validator.labels.datasimpleNonValida};
			}
			value = value.replace(/-/gi, '/');
			var part = value.split('/');
			part[0] = parseInt(part[0]);
			part[1] = parseInt(part[1]);
			if (part[0] < 1 || part[0] > 31 || part[1] < 1 || part[1] > 12) {
				return {valido:false, msg: H.validator.labels.datasimpleNonValida};
			}
			switch (part[1]) {
				case 2:
					if (part[0] > 29) {
						return {valido:false, msg: H.validator.labels.datasimpleNonValida};
					}
					break;
				case 4:
				case 6:
				case 9:
				case 11:	
					if (part[0] > 30) {
						return {valido:false, msg: H.validator.labels.datasimpleNonValida};
					}
					break;
			}
			field.val(value);
		}
		return {valido:true};	
	},
	check_confermapassword: function(field, value, wrapper) {
		var password = field.closest('form').find('.valida-check-password').val();
		if (password != value) {
			return {valido:false, msg: H.validator.labels.confermaPasswordNonValida};	
		}
		if (value != '') {
			if (password == value) {
				return {valido:true};
			}
			var resp = H.validator.action.check_password(field, value);
			if (!resp.valido) {
				return {valido:false, msg: H.validator.labels.confermaPasswordNonValida};
			}
		}
		return {valido:true};	
	}
};


H.validator.checkIsValidForm = function(form) {
	function check(type) {
		var ok = true;
		form.find('.valida-' + type).each(function(i) {
			ok = H.validator.action.valida($(this), type) && ok;
		});
		return ok;
	}
	
	var ok = true;
	for (var i = 0; i < H.validator.validatorType.length; i++) {
		ok = check(H.validator.validatorType[i]) && ok;
	}
	
	ok = check('data') && ok;
	ok = check('required') && ok;
	
	if (ok) {
		var errorCheckboxGroup = [];
		form.find('.checkbox-group[data-minsel]').each(function() {
			var min = $(this).attr('data-minsel');
			if (min == null || min == '' || min <= 0) {
				return;
			}
			if ($(this).find('.image-checkbox-checked').length < min) {					
				errorCheckboxGroup.push('<br>almeno ' + min + ' voce per <strong>' + $(this).parent().find('.checkbox-group-title').html() + '</strong>');
			}
		});
	}
	
	return ok;
}


$(function(){
	$('body').on('blur', 'textarea.valida-required,.valida-required[type=text],.valida-required[type=hidden],.valida-required[type=password]', function(event) {
		var wrapper = $(this).closest('.field-line');
		if (wrapper.hasClass('field-error')) {
			$(this).val(H.utils.trim($(this).val()));
			if ($(this).val().length > 0) {
				wrapper.removeClass('field-error');
			}
		}
	});
	$('body').on('keyup', 'textarea.valida-required,.valida-required[type=text],.valida-required[type=hidden],.valida-required[type=password]', function(event) {
		if (event.keyCode == 13) {
			return;
		}
		var wrapper = $(this).closest('.field-line');
		if (wrapper.hasClass('field-error')) {
			$(this).val(H.utils.trim($(this).val()));
			if ($(this).val().length > 0) {
				wrapper.removeClass('field-error');
			}
		}
	});
	$('body').on('change', 'select.valida-required', function(event) {
		var wrapper = $(this).closest('.field-line');
		if (wrapper.hasClass('field-error')) {
			if ($(this).val().length > 0) {
				wrapper.removeClass('field-error');
			}
		}
	});
		
	$('body').on('keyup','.valida-maxlength', function() {
		var max = parseInt($(this).attr('maxlength'));
		var text = $(this).val();
		if(text.length > max){
            $(this).val(text.substr(0, max));  		
		}
	});		
	$('body').on('keyup','.valida-digitnumber', function() {
		$(this).val(H.utils.trim($(this).val()).replace(/[^\d.]/g, ''));
	});
	$('body').on('keyup','.valida-digitalphanum', function() {
		$(this).val(H.utils.trim($(this).val()).replace(/[^a-zA-Z0-9]/g, ''));
	});
	$('body').on('keyup','.valida-digitalpha', function() {
		$(this).val(H.utils.trim($(this).val()).replace(/[^a-zA-Z]/g, ''));
	});	
	$('body').on('keyup','.valida-digitnickname', function() {
		$(this).val(H.utils.trim($(this).val()).replace(/[^a-zA-Z0-9\-\_]/g, ''));
	});
	$('body').on('keyup','.valida-digittelefono', function() {
		$(this).val(H.utils.trim($(this).val()).replace(/[^0-9\+\-]/g, ''));
	});
	$('body').on('keyup','.valida-digiteuro', function() {
		$(this).val(H.utils.trim($(this).val()).replace(/[^0-9\+\-\,\.]/g, ''));
	});
	$('body').on('keyup','.valida-digitrgb', function() {
		$(this).val(H.utils.trim($(this).val()).replace(/[^0-9abcdefABCDEF]/g, ''));
	});
	
	function valida(i) {
		return function() {
			H.validator.action.valida($(this), H.validator.validatorType[i]);
		}
	}
	
	for (var i = 0; i < H.validator.validatorType.length; i++) {
		$('body').on('blur', '.valida-' + H.validator.validatorType[i], valida(i));
	}
});
