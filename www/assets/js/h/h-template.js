H.data.onLoaded('block-template', '.block-template',
	function($this) {
		$this.addBlockTemplate();
	});
	
(function( $ ){
	$.fn.addBlockTemplate = function(params) {
		return $(this).each(function() {
			$this = $(this);
			var opt = $this.actionParams();
			opt = $.extend(opt, params);
			opt.cont = opt.cont == null ? '.window' : opt.cont;
			opt.addVoid = opt.addVoid == null ? true : opt.addVoid;
			var cont = $this.closest(opt.cont);
			var template = cont.find('[data-block-template=' + opt.template + ']').html();
			template = template.replace(/\[%/g, '[[').replace(/\%]/g, ']]');
			var tempFn = doT.template(template);
			var $thisInner = $this.find('.block-template-inner');
			if ($thisInner.length == 0) {
				$thisInner = $('<div class="block-template-inner"/>');
				$this.append($thisInner);
			}
			function insert(c, o) {
				o = o == null ? {} : o;
				o._count = opt.key + '_' + c;
				var block = $('<div class="block-template-wrapper" data-multiple-fields="' + opt.key + '_' + c +'"/>');
				block.append('<a class="action-block-template-delete button-block-template-delete" href="javascript:void(0)"></a>');
				block.append(tempFn(o));
				block.dataLoaded();
				$thisInner.append(block);	
			}
			var count = $this.attr('data-block-template-count');
			if (count == null || count == '') {
				count = 0;
			}
			else {
				count = parseInt(count);
			}
			
			$this.append('<a class="action-block-template-add button-block-template-add" href="javascript:void(0)"></a>');
			
			var isResources = false;
			var res = cont.find('[data-block-template-resources=' + opt.template + ']');
			if (res.length > 0) {
				var data = H.utils.trim(res.html());
				if (data != '') {
					var obj = null;
					try {
						obj = $.parseJSON(data);
					}
					catch(e) {
						alert(e + '\n\n' + data);
						return;
					}
					for (var i = 0; i < obj.length; i++) {
						insert(count++, obj[i]);			
					}
					isResources = true;
				}
				res.remove();
			}
			if (opt.addVoid && !isResources) {
				insert(count++);
			}
			$this.attr('data-block-template-count', count);		
		});
	};
	$.fn.mergeTemplateData = function(data, params) {
		params = params == null ? {} : params;
		return $(this).each(function() {
			var $this = $(this);
			var opt = $this.actionParams();
			if (opt.mergeTemplate) {
				opt = opt.mergeTemplate;
			}
			opt = $.extend(opt, params);

			opt.empty = opt.empty == null ? false : opt.empty;
			opt.insertType = opt.insertType == null ? 'html' : opt.insertType;
			var obj = null;
			var template = $this.find('[data-merge-template=' + opt.template + ']');
			if (template.length == 0) {
				template = $this;
			}

			opt.content = opt.content == null ? '[data-merge-content=' + opt.template + ']' : opt.content;
			var content = $this.find(opt.content);
			if (content.length == 0) {
				content = $this;
			}

			var tempFn = doT.template(template.html());
			switch (opt.dataType) {
			case 'list-simple':
			case 'list':
				var param_name = opt.param_name == null ? 'list' : opt.param_name;
				if (data[param_name] == null) {
					if (param_name != 'item' && data['item'] != null) {
						data[param_name] = [
							data['item']
						];
					}
					else {
						data[param_name] = [];
						data[param_name + '_total_items'] = 0;
					}
				}
				obj = {list: data[param_name], total: data[param_name + '_total_items']};
				break;
			default:
				var param_name = opt.param_name == null ? 'item' : opt.param_name;
				if (typeof param_name == 'string') {
					obj = data[param_name];
				}
				else {
					obj = {};
					for (var i = 0; i < param_name.length; i++) {
						if (data[param_name[i]]) {
							obj[param_name[i]] = data[param_name[i]];
						}					
					}
				}
				break;
			}
			if (obj == null) {
				obj = {};
			}
			var result = tempFn(obj);

			if (obj.total != null) {
				content.attr('data-total-items', obj.total);
				content.closest('.window').find('.window-toolbar-total').html('TOT: <strong>' + obj.total + '</strong>');
			}
				
			result = $(result);
			if (opt.dataType == 'list') {
				result = result.find('tr');
			}
			
			if (opt.empty || opt.insertType == 'html') {
				content.empty();
			}
			switch(opt.insertType) {
			case 'replace_append':
			case 'replace_prepend':
			case 'replace':
				if (result.length > 1) {
					var list = [];
					result.each(function() {
						list.push({
							uid: $(this).attr('data-uid'),
							obj: $(this)
						});
					});
				}
				else {
					var list = [{
						uid: result.attr('data-uid'),
						obj: result
					}];
				}
				for (var i = 0; i < list.length; i++) {
					var old = content.find('[data-uid=' + list[i].uid + ']');
					if (old.length > 0) {
						old.replaceWith(list[i].obj);
					}
					else {
						switch(opt.insertType) {
						case 'replace_append':
							content.append(list[i].obj);
							break;
						case 'replace_prepend':
							content.prepend(list[i].obj);
							break;
						}
					}
				}
				break;
			case 'prepend':
				content.prepend(result);
				break;
			case 'html':
			case 'append':
				content.append(result);
				break;
			}
			if (opt.lightEffect) {
				result.light();
			}
			result.dataLoaded();
		});
	};
	
	$.fn.light = function() {
		var $this = $(this);
		var count = 0;
		var timeout = 1000;
		var baseBackground = $this.attr('data-background');
		if (baseBackground == null || baseBackground == '') {
			baseBackground = '#ffffff'
		}
		function changeColor() {
			count++;
			if (count % 2 == 0) {
				timeout = 50;
				$this.css({background: baseBackground});
				if (count >= 14) {
					return;
				}
			}
			else {
				//## flash color
				$this.css({background: '#F2F2F2'});
			}
			setTimeout(changeColor, timeout);
		}
		changeColor();	
	};
	
	H._templateCache = [];
	$.fn.loadTemplate = function(params) {
		function loadTmplate($this, html_template, opt) {
			if (opt.render) {
				var tempFn = doT.template(html_template);
				html_template = tempFn({});
			}
			if (opt.paramsTemplate) {
				for (key in opt.paramsTemplate) {
					if (opt.paramsTemplate.hasOwnProperty(key)) {
						var re = new RegExp('%' + key + '%', 'g');
						html_template = html_template.replace(re, opt.paramsTemplate[key]);
					}
				}
			}
			$this.html(html_template);
			if (opt.success) {
				opt.success($this);
			}
			$this.loading(false);
			$this.find('.window').css({display: 'block'});
			$this.find('.window-content').scrollTop(0);
			if (opt.parseOnLoaded) {
				$this.dataLoaded();
			}
			//# try to load maps
			if ($('.autoload-google-map-main').length > 0) {
				setTimeout(
					function(){
						$('.autoload-google-map-main')
							.removeClass('autoload-google-map-main')
							.gmap_main();}
					,1000);
			}
		}
		
		return $(this).each(function() {
			var $this = $(this);
			var opt = $this.actionParams();
			if (opt.loadTemplate) {
				opt = opt.loadTemplate;

			}
			opt = $.extend(opt, params);
			opt.cache = opt.cache == null ? false : opt.cache;
			opt.render = opt.render == null ? false : opt.render;
			opt.parseOnLoaded = opt.parseOnLoaded == null ? true : opt.parseOnLoaded;
			$this.html('');
			$this.loading();
			if (H._templateCache[opt.template] == null || opt.paramsLoad != null) {
				opt.paramsLoad = opt.paramsLoad == null ? {} : opt.paramsLoad;
				var paramsTemplate = $.extend({template: opt.template}, opt.paramsLoad);
				//# added to forward selected parameters to template/load.action
				if(opt.paramsTemplate != null){
					paramsTemplate = $.extend(paramsTemplate, opt.paramsTemplate);
				}
				$this.load('/my/template/load', paramsTemplate, function() {					
					if ($this.find('#invalid-authentication').length) {
						location.href = '/index';
					}
					else {
						var tmpl = $this.html();
						if (opt.cache) {
							H._templateCache[opt.template] = tmpl;
						}
						loadTmplate($this, tmpl, opt);
					}
				});
			}
			else {
				loadTmplate($this, H._templateCache[opt.template], opt);
			}
		});
	};
	
	$.fn.loadTemplateMergeWithData = function(data, opt) {
		return $(this).each(function() {
			var $this = $(this);
			opt = $.extend($this.actionParams(), opt);
			opt.success = function() {
				$this.mergeTemplateData(data, opt);
			}
		});
	};

	$.fn.showMessageWithAction = function(opt){
		$(this).each(function() {
			var $this = $(this);
			opt = $.extend($this.actionParams(), opt);
			opt.success = function() {
				opt.message.success = opt.message.success == null ?
					H.labels.azioneEseguita : opt.message.success;
				H.message.confirm(opt.message.success);
			}
			opt.error = function(){
				opt.message.error = opt.message.error == null ?
					H.labels.azioneNonEseguita : opt.message.error;
				H.message.error(opt.message.error);
			}
			$this.loadData(opt);
		});
	}

	$.fn.loadTemplateAndData = function(opt) {
		return $(this).each(function() {
			var $this = $(this);
			$this.loading();
			opt = $.extend($this.actionParams(), opt);
			var success = opt.success == null ? function() {} : opt.success;
			if (opt.loadTemplate) {
				var topt = opt.loadTemplate;
			}
			else {
				var topt = opt;
			}
			topt.error = opt.error == null ? function() {} : opt.error;
			if (opt.loadData) {
				topt.parseOnLoaded = false;
				topt.success = function(cont) {
					opt.loadData.success = function(data) {
						$this.dataLoaded();
						success(data);
					};
					var obj = $this.find('.action-load-data');
					if (obj.length == 0) {
						obj = $this;
					}
					obj.loadData(opt.loadData);
				};
			}
			else {
				topt.success = function(cont) {
					$this.loading(false);
					success();
				};			
			}
			$this.loadTemplate(topt);
		});
	};
	
	$.fn.loadData = function(opt) {
		var $this = $(this);		
		var ap = $this.actionParams();
		if (ap.loadData) {
			ap = ap.loadData;
		}

		opt = $.extend(ap, opt);
		var success = opt.success == null ? function() {} : opt.success;
		var error = opt.error == null ? function() {} : opt.error;
		opt.params = $.extend($this.actionParams('data-load-params'), opt.params);
		$this.find('.window').css({visibility: 'hidden'});
		$this.loading();
		$.xajax('/my/data/' + opt.url, {
			data: opt.params,
			hideLoading: function() {
				$this.loading(false);
			},
			success: function (response) {
				if (response.success) {
					$this.mergeTemplateData(response.data, opt);
					$this.dataLoaded();
					success(response.data);
				}
				else {
					H.message.error('Parametri di caricamento non validi, impossibile procedere.');
				}
				$this.find('.window').css({visibility: 'visible'});
				$this.loading(false);
			},
			error: function() {
				H.message.error('Server non raggiungibile, impossibile procedere.');
				$this.loading(false);
				error();
			}
		});

		return $(this);
	};
})( jQuery );

$(function(){
	$('body').on('reset-template-cache', function() {
		H._templateCache = [];
	});
	$('body').on('click', '.action-block-template-delete', function() {
		$(this).closest('.block-template-wrapper').fadeOut(200, function() {
			$(this).remove();
		});
	});
	$('body').on('click', '.action-block-template-add', function() {
		$(this).closest('.block-template').addBlockTemplate({addVoid: true, endOnAdd: true});
	});
	$('body').on('click', '.action-message', function() {
		var opt = $.parseJSON($(this).attr('data-action'));
		//console.log(JSON.stringify(opt));
		$(this).showMessageWithAction(opt);
	});
});