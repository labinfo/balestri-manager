if(!Array.prototype.contains) {
    Array.prototype.contains = function(needle) {
        for(var i = 0; i < this.length; i++) {
            if(this[i] === needle) {
                return true;
            }
        }
        return false;
    };
}

var H = {
	_countId: 0,
	_scriptLoaded: [],
	id: function() {
		return '_uid_' + (this._countId++);
	},
	content: function(ref) {
		return $('[data-content=' + ref + ']');
	},
	loadScript: function(opt) {
		opt.url = '/assets/js/' + opt.url + '.js?b=' + H.build;
		opt.onComplete = opt.onComplete == null ? function() {} : opt.onComplete;
		opt.onError = opt.onError == null ? function() {} : opt.onError;
		if (this._scriptLoaded[opt.url] == null) {
			$.getScript(opt.url)
				.done(function() {
					H._scriptLoaded[opt.url] = opt.url;
					opt.onComplete();
				})
				.fail(function() {
					opt.onError();
					H.message.error('Impossibile caricare le risorse dinamiche, verificare la connessione.');
				});		
		}
		else {
			opt.onComplete();
		}
	},
	data: {
		actions: [],
		onLoaded: function(key, selector, action, force) {
			force = force == null ? false : force;
			H.data.actions.push({key: key, selector: selector, action: action, force: force});
		},
		loaded: function(cont) {
			cont = cont == null ? $('body') : cont;
			for (var i = 0; i < H.data.actions.length; i++) {
				var a = H.data.actions[i];
				var list = cont.find(a.selector);
				if (!a.force) {
					list = list.not('[data-merge-template] *,[data-content] *');
				}
				list = list.not('[data-parse-' + a.key + ']');
				list.each(function() {
					$(this).attr('data-parse-' + a.key, 1);
					a.action($(this));
				});
			}	
		}
	},
	utils: {
		trim: function(value) {
			if (value == null || value.length == 0) {
				return '';
			}
			return value.replace(/\s+$|^\s+/g, '');
		},
		getURLParameter: function(sParam) {
			var sPageURL = window.location.search.substring(1);
			var sURLVariables = sPageURL.split('&');
			for (var i = 0; i < sURLVariables.length; i++) {
				var sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] == sParam) {
					return decodeURIComponent(sParameterName[1]);
				}
			}
			return null;	
		},
		location: {
			getHash: function() {
				return location.hash.replace('#', '');
			},
			resetHash: function() {
				Utility.location.setHash(null);
			},
			setHash: function(value) {
				location.hash = value;	
			}
		}
	},
	cookie: {
		set: function(name, value, exdays) {
			exdays = exdays == null ? 365 : exdays;
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+d.toUTCString();
			document.cookie = name + "=" + value + "; " + expires + "; path=/";
		},
		get: function(name) {
			name += '=';
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
			}
			return '';
		},
		rem: function(name) {
			document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
		}
	},
	loading: {
		show: function() {
			$('body').loading();
		},
		hide: function() {
			$('body').loading(false);
		}
	},
	message: {
		error: function(text) {
			$('body').message({message: text, type: 'error'});
		},
		confirm: function(text) {
			if (text == null || text == '') {
				return;
			}
			$('body').message({message: text});
		},
		hide: function() {
			$('body').message(true);
		}
	}
};
	
(function( $ ){
	$.xajax = function(url, opt) {
		opt = opt == null ? {} : opt;
		opt.url = url;
		opt.data = opt.data == null ? {} : opt.data;
		opt.data.hson = H.id();
		opt.type = 'post';
		opt.contenttype = 'application/json; charset=utf-8';
		opt.dataType = opt.dataType == null ? 'json' : opt.dataType;
		var hideLoading = opt.hideLoading == null ? function() {} : opt.hideLoading;
		if (opt.success == null) {
			var success = function(data) {
				hideLoading();
				if (data.message != null && data.message != '') {
					if (data.success) {
						H.message.confirm(data.message);
						if (opt.successAndCallbackOk) {
							opt.successAndCallbackOk(data);
						}
					}
					else {
						H.message.error(data.message);
					}
				}
			}
		}
		else {
			var success = opt.success;
		}
		var error = opt.error == null ? function() {} : opt.error;
		opt.noServeErrorMessage = opt.noServeErrorMessage == null ? false : opt.noServeErrorMessage;
		opt.error = function() {
			H.loading.hide();
			hideLoading();
			if (!opt.noServeErrorMessage) {
				H.message.error('Server non raggiungibile, impossibile procedere con il caricamento.');
			}
			error();
		};
		opt.success = function(response) {
			if (response.session_fault != null || (response.data && response.data.session_fault)) {
				H.message.error('Sessione scaduta.');
				H.loading.show();
				H.cookie.set('callback_message_error', 'Sessione scaduta o utente entrato senza autorizzazioni');
				location.href = H.url_logout;
				return;
			}
			if (response.data && response.data.redirect_to_url) {
				H.loading.show();
				location.href = response.data.redirect_to_url;
				return;
			}
			H.loading.hide();
			if (response.exception != null && response.exception) {
				hideLoading();
				H.message.error('Errore interno al server, impossibile procedere con il caricamento.');
				return;
			}
			success(response);			
		};
		$.ajax(opt);
	};
	
	$.fn.dataLoaded = function(show) {
		return $(this).each(function() {
			H.data.loaded($(this));
		});
	};
	
	$.fn.loading = function(show) {
		show = show == null ? true : show;
		return $(this).each(function() {
			var cont = $(this).closest('.window-base');
			var base = true;
			if (cont.length == 0) {
				cont = $(this);
				base = false;
			}
			var loading = cont.find('.loading');
			if (show) {
				if (base) {
					if (loading.length > 0) {
						return;
					}
					loading = $('<div class="loading"/>');
					cont.append(loading);
					if ($(this).closest('.action-autoupdate')) {
						loading.addClass('loading-autoupdate');
					}
					else {
						loading.addClass('loading-simple');
					}
				}
				else {
					cont.addClass('loading-layer');
				}
			}
			else {
				cont.removeClass('loading-layer');
				loading.remove();
			}
		});
	};
		
	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			var name = this.name;
			if (name.endsWith('[]')) {
				name = name.substr(0, name.length - 2);
			}
			if (o[name] !== undefined) {
				if (!o[name].push) {
					o[name] = [o[name]];
				}
				o[name].push(this.value || '');
			} else {
				o[name] = this.value || '';
			}
		});
		return o;
	};
	
	$.fn.actionParams = function(name, params, update) {
		name = name == null ? 'data-action' : name;
		if (params == null) {
			var attr = $(this).attr(name);
			if (attr == null || attr == '') {
				return {};
			}
			var obj = null;
			try {
				obj = $.parseJSON(attr);
			}
			catch(e) {
				alert(e + '\n\n' + attr);
			}
			if (obj == null) {
				return {};
			}
			return obj;
		}
		else {
			update = update == null ? false : update;
			if (update) {
				var old = $(this).actionParams(name);
				params = $.extend(old, params);
			}
			$(this).attr(name, JSON.stringify(params));
			return params;
		}
	};
	
	$.fn.message = function(params) {
		if (typeof params === 'boolean') {
			$('.message-wrapper').click();
			return;
		}		
		if (typeof params === 'string') {
			params = {message: params};
		}
		if (params.message == null || params.message == '') {
			return;
		}
		var wrapper = $('<div class="message-wrapper">' + params.message + '</div>');
		$(this).append(wrapper);

		wrapper.addClass('message-wrapper-' + (params.type == null ? 'confirm' : params.type));
		wrapper.css({top: -wrapper.outerHeight()});
		wrapper.animate({
				top: 0
			}, 300);
		wrapper.click(function () {
			wrapper.fadeOut(300, function() {
				$(this).remove();
			});
		});
		setTimeout(function() {
			wrapper.click();
		}, 8 * 1000);
	};
	
	$.fn.loadPage = function(opt) {
		var $this = $(this);
		opt = $.extend($this.actionParams(), opt);
		$this.html('');
		$this.loading();
		$this.load(opt.url, opt.params, function() {					
			if ($this.find('#invalid-authentication').length) {
				location.href = H.url_logout;
			}
			else {
				$this.loading(false);
				$this.find('.window').css({display: 'block'});
				$this.dataLoaded();
				if (opt.complete) {
					opt.complete();
				}
			}
		});
	};
	$.fn.updatePage = function(opt) {
		var $this = $(this);
		opt = $.extend($this.actionParams(), opt);
		opt.insert = opt.insert == null ? 'append' : opt.insert;
		opt.loading = opt.loading == null ? false : opt.loading;
		if (opt.loading) {
			$this.loading();
		}
		$('<div/>').load(opt.url, opt.params, function() {					
			if ($(this).find('#invalid-authentication').length) {
				location.href = H.url_logout;
			}
			else {
				var html = $(this).html();
				var is = false;
				if (html != '') {
					switch (opt.insert) {
					case 'append':
						$this.append(html);
						break;
					case 'prepend':
						$this.prepend(html);
						break;
					}
					is = true;
					$this.dataLoaded();
				}
				$this.loading(false);
				if (opt.complete) {
					opt.complete(is);
				}
			}
		});
	};
})( jQuery );

$(function(){
	$('body').dataLoaded();
	
	var callback_message_error = H.cookie.get('callback_message_error');
	if (callback_message_error != null && callback_message_error != '') {
		H.cookie.rem('callback_message_error');
		H.message.error(callback_message_error);
	}
	else {
		var callback_message_confirm = H.cookie.get('callback_message_confirm');
		if (callback_message_confirm != null && callback_message_confirm != '') {
			H.cookie.rem('callback_message_confirm');
			H.message.confirm(callback_message_confirm);
		}	
	}
});