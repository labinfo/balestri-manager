H.data.onLoaded('tab', '.tab',
        function ($this) {
            $this.tab();
        });
H.data.onLoaded('visual-editor', '.visual-editor',
        function ($this) {
            $this.redactor({
                lang: 'it'
            });
        });

(function ($) {
    $.fn.addOrRemoveClass = function (cls) {
        return $(this).each(function () {
            if ($(this).hasClass(cls)) {
                $(this).removeClass(cls)
            } else {
                $(this).addClass(cls)
            }
        });
    };

    $.fn.tab = function () {
        return $(this).each(function () {
            $(this).find('[data-tab-rif]').eq(0).click();
        });
    };

    $.fn.alttext = function () {
        var value = H.utils.trim($(this).attr('data-alttext'));
        if (value == '') {
            return;
        }
        var alt = $('<div class="alttext-wrapper"><div class="alttext-arrow-up-alt"></div><div class="alttext">' + value + '</div></div>');
        $('body').append(alt);
        var offset = $(this).offset();
        var left = offset.left + $(this).outerWidth() / 2 - alt.outerWidth() / 2;
        var leftArrow = (alt.outerWidth() - 16) / 2;
        if (left + alt.outerWidth() > $(document).width() - 5) {
            left = $(document).width() - 5 - alt.outerWidth();
            leftArrow = offset.left - left - 8 + $(this).outerWidth() / 2;
        } else if (left < 0) {
            left = 5;
            leftArrow = offset.left - 12 + $(this).outerWidth() / 2
        }
        alt.css({top: (offset.top + $(this).outerHeight()) + 'px', left: left + 'px'});
        alt.find('.alttext-arrow-up-alt').css({left: leftArrow + 'px'});
    };

    $.fn.foreground = function () {
        var zIndex = 1000;
        $('[data-layer-index]').each(function () {
            var curZIndex = $(this).attr('data-layer-index');
            if (curZIndex == null || curZIndex == '') {
                return;
            }
            curZIndex = parseInt($(this).attr('data-layer-index'));
            if (curZIndex > zIndex) {
                zIndex = curZIndex;
            }
        });
        zIndex++;
        $(this).attr('data-layer-index', zIndex);
        $(this).css({zIndex: zIndex});
        $(this).fadeIn(300);
        return $(this);
    };
    $.fn.box = function (opt) {
        $('body').trigger('hide-on-click');
        var $this = $(this);
        if (typeof opt === 'boolean') {
            if (!$this.hasClass('layer')) {
                $this = $this.closest('.layer');
            }
            $this.trigger('close-layer');
            return;
        }
        H.message.hide();
        $(document).off('click');
        opt = $.extend($this.actionParams(), opt);

        if (opt.classCss) {
            if (typeof opt.classCss == 'string') {
                opt.classCss = [opt.classCss];
            }
        } else {
            opt.classCss = [];
        }

        opt.menu = opt.menu == null ? false : opt.menu;
        opt.hideBtnClose = opt.hideBtnClose == null ? false : opt.hideBtnClose;
        opt.classCss.push('layer');
        opt.classCss.push('layer-window');
        opt.parent = opt.parent == null ? 'body' : opt.parent;
        var parent = $(opt.parent);
        parent.scrollTop(0);

        var overlay = null;
        if (opt.overlay) {
            overlay = $('<div class="layer-overlay"/>').foreground();
            parent.append(overlay);
            parent.addClass('layer-overlay-show');
            if (opt.parent != 'body') {
                overlay.css({position: 'absolute'});
            }
        }
        $this.css({display: 'none'});
        parent.append($this);

        function close() {
            if (opt.onClose) {
                opt.onClose();
            }
            $this.fadeOut(100, function () {
                $(this).remove();
            });
            if (overlay != null) {
                overlay.fadeOut(100, function () {
                    parent.removeClass('layer-overlay-show');
                    $(this).remove();
                });
            }
        }

        function addButtonClose() {
            if (overlay == null || opt.hideBtnClose) {
                return;
            }
            $this.append('<div class="action-layer-close-button layer-close-button"></div>');
        }

        if (opt.menu) {
            opt.classCss.push('layer-light');
            opt.classCss.push('layer-menu');
            $this.on('hide-layer-menu', function () {
                close();
            });
            setTimeout(function () {
                $(document).on('click', function () {
                    close();
                });
            }, 1);
        } else {
            opt.classCss.push('layer-base');
        }

        for (var i = 0; i < opt.classCss.length; i++) {
            $this.addClass(opt.classCss[i]);
        }

        if (opt.css) {
            $this.css(opt.css);
        }

        if (opt.content) {
            if (typeof opt.content == 'string') {
                $this.html(opt.content);
            } else {
                $this.html(opt.content.html());
            }
            $this.showIf(opt.showIf);
            addButtonClose();
            $this.foreground();
        } else {
            if (opt.load) {
                var success = opt.load.success == null ? function () {} : opt.load.success;
                opt.load.success = function () {
                    $this.showIf(opt.showIf);
                    addButtonClose();
                    $this.dataLoaded();
                    $this.foreground();
                    success();
                };
                $this.loadTemplateAndData(opt.load);
            } else if (opt.url) {
                var win = $('<div class="window"/>');
                addButtonClose();
                $this.append(win);
                $this.foreground();
                win.load(opt.url, opt.params, function () {
                    $this.dataLoaded();
                    if (opt.onLoad) {
                        opt.onLoad();
                    }
                });
            } else {
                $this.showIf(opt.showIf);
                addButtonClose();
                $this.dataLoaded();
                $this.foreground();
            }
        }
        $this.on('close-layer', close);
        return $this;
    };

    $.fn.showIf = function (params) {
        $(this).find('.item-show-if').css({display: 'none'});
        if (params == null) {
            return $(this);
        }
        if (typeof params === 'string') {
            var list = [params];
        } else {
            var list = params;
        }
        return $(this).each(function () {
            for (var i = 0; i < list.length; i++) {
                $(this).find('.show-if-' + list[i]).css({display: 'block'});
            }
        });
    };

    H.alert = function (opt) {
        opt.buttonOk = opt.buttonOk == null ? 'Ok' : opt.buttonOk;
        opt.title = opt.title == null ? '' : opt.title;
        opt.onConfirm = opt.onConfirm == null ? function () {} : opt.onConfirm;
        var html = [
            '<div class="window">',
            '<div class="window-base window-base-bottombar">',
            '	<div class="window-title">',
            opt.title,
            '	</div>',
            '	<div class="window-content">',
            '		<div class="window-content-inner">',
            opt.text,
            '		</div>',
            '	</div>',
            '	<div class="window-bottombar">',
            '		<a class="button button-submit action-confirm-ok">', opt.buttonOk, '</a>',
            '	</div>',
            '</div>',
            '</div>'
        ];
        var win = $(html.join(''));
        win = win.box({overlay: true, classCss: 'layer-confirm'});
        win.on('click', '.action-confirm-ok', function () {
            win.box(false);
            opt.onConfirm();
        });
    };
    H.confirm = function (opt) {
        opt.buttonOk = opt.buttonOk == null ? 'Si' : opt.buttonOk;
        opt.title = opt.title == null ? 'Conferma' : opt.title;
        opt.onConfirm = opt.onConfirm == null ? function () {} : opt.onConfirm;
        opt.onCancel = opt.onCancel == null ? function () {} : opt.onCancel;
        var html = [
            '<div class="window">',
            '<div class="window-base window-base-bottombar">',
            '	<div class="window-title">',
            opt.title,
            '	</div>',
            '	<div class="window-content">',
            '		<div class="window-content-inner">',
            opt.text,
            '		</div>',
            '	</div>',
            '	<div class="window-bottombar">',
            '		<a class="button button-style button-close action-layer-close-button">Annulla</a>',
            '		<a class="button button-submit action-confirm-ok">', opt.buttonOk, '</a>',
            '	</div>',
            '</div>',
            '</div>'
        ];
        var win = $(html.join(''));
        win = win.box({overlay: true, onClose: opt.onCancel, classCss: 'layer-confirm'});
        win.on('click', '.action-confirm-ok', function () {
            win.box(false);
            opt.onConfirm();
        });
    };
})(jQuery);

$(function () {
    $('body').on('click', '[data-tab-rif]', function () {
        if ($(this).hasClass('open')) {
            return;
        }
        var cont = $(this).closest('.tab');
        cont.find('[data-tab-rif].open').removeClass('open');
        $(this).addClass('open');
        cont.find('[data-tab]').css({display: 'none'});

        $tab = cont.find('[data-tab=' + $(this).attr('data-tab-rif') + ']');
        if (!!$tab.attr('scale-window')) {
            var size = $tab.attr('scale-window');
            $window = $(this).closest('.layer');
            doResize($window, jQuery.parseJSON(size));
        }

        cont.find('[data-tab=' + $(this).attr('data-tab-rif') + ']').css({display: 'block'});
    });

    function doResize($window, size) {
        if(size.height === undefined){
            size.height = $(document).height() - 50;
        }
        var margintop = '-' + parseInt(size.height) / 2;
        var marginleft = '-' + parseInt(size.width) / 2;

        $window.animate({
            marginTop: margintop,
            marginLeft: marginleft,
            width: size.width,
            height: size.height
        }, 500);

    }


    $('body').on('hide-on-click', function () {
        $('.layer-menu').trigger('hide-layer-menu');
        $('.action-item.show').removeClass('show');
    });

    $('body').on('click', '.action-layer-close-button', function () {
        $(this).closest('.layer-window').box(false);
    });

    $('body').on('mouseover', '[data-alttext]', function () {
        $(this).alttext();
    });
    $('body').on('mouseout', '[data-alttext]', function () {
        $('.alttext-wrapper').remove();
    });
    $('body').on('click', '[data-alttext]', function () {
        $('.alttext-wrapper').remove();
    });
    $('body').on('click', '.field-group-expand', function () {
        var $this = $(this);
        $this.find('.field-group-body').toggle(function () {
            $this.addOrRemoveClass('field-group-expand-open');
        });
    });
});