H.data.onLoaded('table-fix-header', '.table-fix-header',
	function($this) {
		$this.fixedHeader();
	});
H.data.onLoaded('autoupdate', '.action-autoupdate',
	function($this) {
		$this.autoupdate();
	});

(function( $ ){
	$.fn.fixedHeader = function(params) {
		var $this = $(this);
		var cont = $(this).closest('.window-content');
				
		var fixed_table = $('<table class="table table-fixed-header" cellpadding="0" cellspacing="0" border="0"></table>');
		var fixed_cont = $('<div class="table-fixed-header-wrapper"/>').css({position:'absolute', left: '0px', top:'0px', right: '0px'});
		
		var thead = $('<thead/>');
		$this.find('thead th').each(function() {
			var clone = $(this).clone();
			var refId = H.id();
			$(this).attr('data-ref-id', refId);
			clone.attr('data-ref-id', refId);
			thead.append(clone);
		}); 
		
		fixed_table.append($this.find('colgroup').clone())
			.append(thead);
		$this.closest('.table-wrapper').append(fixed_cont.append(fixed_table));

		cont.scroll(function() {
			fixed_cont.css({top: $(this).scrollTop() + 'px'});
		});
	};
		
	$.fn.alttext = function() {
		var value = $(this).attr('data-alttext');
		if (value == '') {
			return;
		}
		var alt = $('<div class="alttext-wrapper"><div class="alttext-arrow-up-alt"></div><div class="alttext">' + value + '</div></div>');
		$('body').append(alt);
		var offset = $(this).offset();
		var left = offset.left + $(this).outerWidth() / 2 - alt.outerWidth() / 2;
		var leftArrow = (alt.outerWidth() - 16) / 2;
		if (left + alt.outerWidth() > $(document).width() - 5) {
			left = $(document).width() - 5 - alt.outerWidth();
			leftArrow = offset.left - left - 8 + $(this).outerWidth() / 2;
		}
		else if (left < 0) {
			left = 5;
			leftArrow = offset.left - 12 + $(this).outerWidth() / 2
		}
		alt.css({top: (offset.top + $(this).outerHeight()) + 'px', left: left + 'px'});
		alt.find('.alttext-arrow-up-alt').css({left: leftArrow + 'px'});
	};
		
	$.fn.selectionInList = function(opt) {
		opt = opt == null ? $(this).actionParams() : opt;
		opt.no_message = opt.no_message == null ? false : opt.no_message;
		opt.join = opt.join == null ? true : opt.join;
		var message = null;
		var selected = $(this).find('.checkbox-checked');
		if (!opt.no_message) {
			if (opt.check == null || opt.check <= 0) {
				if (selected.length <= 0) {
					message = opt.errorMessage == null ? 'Selezionare almeno un valore dall\'elenco, tenere premuto il pulsante shift per la selezione multipla' : opt.errorMessage;
				}		
			}
			else {
				if (opt.check != selected.length) {
					message = opt.errorMessage == null ? 'Selezionare ' + opt.check + ' valore dall\'elenco' : opt.errorMessage;
				}
			}
		}
		if (message == null) {
			var list = [];
			selected.each(function() {
				list.push($(this).attr('data-value'));
			})
			if (opt.join) {
				return list.join(',');
			}
			return list;
		}
		H.message.error(message);
		return null;
	};
	
	$.fn.actinOnImtemsTable = function(params) {
		var $this = $(this);
		params.paramId = params.paramId == null ? 'data-id' : params.paramId;
		var closest = $this.closest('tr[' + params.paramId + ']');
		var id = $this.tableSelectedId(params.paramId);
		if (id == null) {
			return;
		}
		var opt = $this.actionParams();
				
		var callback = function() {}
		switch (params.callback) {
		case 'reload':
			callback = function(data) {
				$('[data-update-content=' + params.type +']').loadData({empty: true});
				if (params.close) {				
					$this.closest('.layer').box(false);
				}
			};
			break;
		case 'update':
			callback = function(data) {
				$('[data-update-content=' + params.type +']').mergeTemplateData(data.data, {insertType: 'replace_prepend', lightEffect: true});
				if (params.close) {
					$this.closest('.layer').box(false);
				}
			};
			break;
		case 'delete':
			callback = function(data) {
				if (closest.length > 0) {
					closest.fadeOut(200, function() {
								$(this).remove();
							});
				}
				else {
					id = id.split(',');
					for (var i = 0; i < id.length; i++) {
						$(opt.select.from).find('[data-id=' + id[i] + ']').fadeOut(200, function() {
								$(this).remove();
							});
					}
				}
				if (params.close) {
					$this.closest('.layer').box(false);
				}
			};
			break;
		}
		params.params = $.extend({id: id}, params.params);

		if (params.title) {
			if (typeof params.text == 'string') {
				var text_item = params.text;
				var text_items = params.text;
			}
			else {
				var text_item = params.text.item;
				var text_items = params.text.items;
			}
			var text = id.split(',').length == 1 ? text_item : text_items;
			H.confirm({
				title: params.title,
				text: text,
				onConfirm: function() {
					H.loading.show();
					$.xajax(params.url, {
							data: params.params,
							successAndCallbackOk: callback
						});			
				}
			});	
		}
		else {
			H.loading.show();
			$.xajax(params.url, {
					data: params.params,
					successAndCallbackOk: callback
				});
		}
	};
	
	$.fn.tableSelectedId = function(param) {
		param = param == null ? 'data-id' : param;
		var $this = $(this);
		var cont = $this.closest('tr[' + param + ']');
		if (cont.length > 0) {
			var id = cont.attr(param);
		}
		else {
			var opt = $this.actionParams();
			var id = $(opt.select.from).selectionInList(opt.select);
		}	
		return id;
	};
	
	$.fn.boxTableSelectedId = function(optfun) {
		var $this = $(this);
		var opt = $this.actionParams();
		var key_opt = null;
		var success = null;
		if (optfun != null) {
			if (typeof optfun == 'string') {
				key_opt = optfun;
			}
			else {
				key_opt = optfun.key_opt;
				if (optfun.success) {
					success = optfun.success;
				}
			}
		}
		if (key_opt && opt[key_opt]) {
			opt = opt[key_opt];
		}
		if (opt.id == null) {
			var id = $this.tableSelectedId();
		}
		else {
			var id = opt.id;
		}
			
		if (id == null) {
			return;
		}
		opt.paramsIdName = opt.paramsIdName == null ? 'id' : opt.paramsIdName;
		var params = {};
		params[opt.paramsIdName] = id;
		params = $.extend(params, opt.params);
		if (opt.paramsTemplate) {
			var paramsTemplate = params;
			if (typeof opt.paramsTemplate === 'object') {
				paramsTemplate = $.extend(paramsTemplate, opt.paramsTemplate);
			}
		}
		else {
			var paramsTemplate = null;
		}
		opt.overlay = opt.overlay == null ? true : opt.overlay;
		opt.css = opt.css == null ? 'layer-small-edit' : opt.css;
		$('<div/>').box({
				load: {
					loadTemplate: {
						template: opt.template,
						paramsLoad: opt.paramsLoad,
						cache: opt.cache,
						paramsTemplate: paramsTemplate
					}, 
					loadData: {
						param_name: opt.param_name, 
						url: opt.url, 
						params: params
					},
					success: function() {
						if (success) {
							success();
						}
					}
				},
				overlay: opt.overlay,
				classCss: opt.css
			});
	};
	
	$.fn.autoupdate = function(param) {
		if (param != null) {
			switch(param) {
				case 'reset':
					$(this).trigger('autoupdate-reset');
					break;
			}
			return;
		}

		var $this = $(this);
		var content_scroll = $this.closest('.window-content');
		var opt = $this.actionParams();
		
		function updateElementsHeight() {
			var sum = 0;
			$this.find('tr').each(function() {
				sum += $(this).find('td').eq(0).outerHeight();
			});
			opt.elementsHeight = sum;
		}
		

		var inLoading = false;
		function loadOtherElements() {
			if (inLoading) {
				return;
			}
			inLoading = true;
			
			$this.actionParams('data-load-params', {start: getNumElems()}, true);
			$this.loadData({
				insertType: 'append', 
				success: function(data) {
					if (data && data.total_items) {
						$this.attr('data-autoload-total', data.total_items);
					}
					updateHeightWrapper();
					updateElementsHeight();
					inLoading = false;
					content_scroll.loading();
				},
				error: function() {
					inLoading = false;
				}
			});
		}
		function updateHeightWrapper() {
			opt.height = content_scroll.height();
		}
		function getNumElems() {
			return $this.find('.table tbody tr').length;
		}
		function updateTotElems() {
			var value = $this.attr('data-total-items');
			if (value == null || value == '') {
				var cont = $this.find('[data-total-items]');
				if (cont.length > 0) {
					value = cont.attr('data-total-items');
				}
			}
			if (value != null && value != '') {
				opt.totalElems = parseInt(value);
			}
			else {
				opt.totalElems = 0;
			}
		}
		
		function fillSpace() {
			if (getNumElems() < opt.totalElems && opt.height > opt.elementsHeight) {
				loadOtherElements();
			}
		}
				
		function reset() {
			inLoading = false;		
			updateHeightWrapper();
			updateElementsHeight();
			opt.totalElems = -1;
			$this.scrollTop(0);
			fillSpace();
		}
		reset();

		content_scroll.on('scroll',function() {
			if (inLoading) {
				return;
			}
			if (opt.totalElems == -1) {
				updateTotElems();
			}
			if (getNumElems() >= opt.totalElems) {
				return;
			}
			var sth = 1.5 * ($(this).scrollTop() + opt.height);
			if (sth > opt.elementsHeight) {
				loadOtherElements();
			}
		});
		
		$(window).resize(function() {
			updateHeightWrapper();
			fillSpace();
		});
		$this.on('autoupdate-reset', function() {
			reset();
		});
	};
})( jQuery );

$(function(){
	$('body').on('click', '[data-order]', function() {
		var $this = $(this);
		var cont = $this.closest('.action-load-data');
		var curOrderType = $this.attr('data-order-type');
		cont.find('[data-order-type]').attr('data-order-type', '');
		curOrderType = curOrderType == 'asc' ? 'desc' : 'asc';		
		var refId = $this.attr('data-ref-id');
		if (refId != null && refId != '') {
			$('[data-ref-id=' + refId + ']').attr('data-order-type', curOrderType);
		}
		else {
			$this.attr('data-order-type', curOrderType);
		}
		
		var order = $this.attr('data-order') + ':' + curOrderType;
		cont.actionParams('data-load-params', {order: order, start: 0}, true);
		cont.loadData({empty: true, success: function() {
			cont.find('.table-fixed-header-wrapper').css({top: '0px'});
			cont.closest('.window-content').scrollTop(0);
			cont.autoupdate();
		}});
	});
});