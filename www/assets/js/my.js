$(function(){
	$('body').on('click', '.action-download-file', function() {
		var opt = $(this).actionParams();
		$.xajax('/my/check-download', {
				data: opt,
				success: function(data) {
					var id = H.id();
					var cont = $('<div style="display: none;" class="download-wrapper"/>');
					var form = $('<form action="/my/download" target="' + id + '" method="post"/>');
					cont.append('<iframe name="' + id + '"/>')
						.append(form);
					for (var key in opt) {
						if (opt.hasOwnProperty(key)) {
							form.append('<input type="hidden" name="' + key + '" value="' + opt[key] + '"/>');
						}
					}
					$('body').append(cont);
					form.submit();
				}
			});		
	});
	
	var _timeout = null;
	var inLoading = false;
	function check(force) {
		force = force == null ? false : force;
		clearTimeout(_timeout);
		if (inLoading && !force) {
			return;
		}
		inLoading = true;
		
		function update_num(key, num) {
			var cont = $('.update_num_' + key);
			if (cont.length == 0) {
				if (cont.hasClass('hide-when-zero')) {
					cont.css({display: 'none'});
				}
				return;
			}
			cont.html(num);
			cont.attr('data-num', num);
			if (cont.hasClass('hide-when-zero')) {
				if (parseInt(num) == 0) {
					cont.css({display: 'none'});
				}
				else {
					cont.css({display: 'block'});
				}
			}
		}
		
		var params = {};
		$.xajax('/my/check', {
				data: params,
				noServeErrorMessage: true,
				success: function(data) {
					inLoading = false;
					if (data.data) {
						if (data.data.user_info) {
							var ui = data.data.user_info;
							update_num('richieste_contatto', ui.num_richieste_contatto_nuovo);
						}
					}
					_timeout = setTimeout(check, 15 * 1000);
				},
				error: function() {
					setTimeout(function() {
						check(true);
					}, 5 * 1000);
				}
			});
	}
	_timeout = setTimeout(check, 15 * 1000);
	
	$('body').on('need-update-info', function() {
		check();
	});
});