var map;
var geocoder;

var Url = {
    cartelli: 'map/cartelli',
    negozi: 'map/negozi',
    rivali: 'map/rivali'
};
var useLabel = false;
var Markers = {
    cartelli: [],
    negozi: [],
    rivali: []
};

var Data = {
    cartelli: [],
    negozi: [],
    rivali: []
};

(function ($) {
    //called inside h-template
    $.fn.gmap_main = function () {
        var $this = $(this);
        var mapConfig = {
            center: {lat: 44.1396438, lng: 12.2464292},
            scrollwheel: true,
            zoom: 15
        };
        log("init maps");
        map = new google.maps.Map($this.get(0), mapConfig);
        geocoder = new google.maps.Geocoder();
        init(true);
    }

})(jQuery);


function init(firstTime) {

    if (map === undefined) {
        log('map not found');
        return;
    }

    if ($('input[name="show_cartelli"]').val() === '1') {
        //cartelli
        askRemote(Url.cartelli, function (data) {
            var list = data.list;
            updateCartelli(list, map, firstTime);
        });
    }
    //negozi
    if ($('input[name="show_negozi"]').val() === '1') {
        askRemote(Url.negozi, function (data) {
            var list = data.list;
            updateNegozi(list, map);
        });
    }

    //rivali
    if ($('input[name="show_concorrenti"]').val() === '1') {
        askRemote(Url.rivali, function (data) {
            var list = data.list;
            updateRivali(list, map);
        });
    }
}

function updateRivali(list, map) {
    for (i = 0; i < list.length; i++) {
        var it = list[i];
        var location = {
            lat: it.recapito_latitudine,
            lng: it.recapito_longitudine
        };

        var marker;

        if (MarkerWithLabel) {

            var url = 'img/80x80/' + it.logo;
            $iconTemplate = $('<img/>').attr('src', url);

            marker = new MarkerWithLabel({
                position: location,
                map: map,
                //icon: {
                //    size: new google.maps.Size(60, 60)
                //},
                icon: " ",
                labelContent: $('<div/>').append($iconTemplate).html(),
                labelAnchor: new google.maps.Point(30, 30),
                labelClass: 'marker-logo'
            });
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map,
                title: it.nome,
                label: useLabel ? it.nome : null,
                icon: getLogo(it.logo, 60, 60) //to change
            });
        }

        var infowindow = new google.maps.InfoWindow({
            content: it.nome
        });
        //add to the list
        Markers.rivali.push({
            marker: marker,
            window: infowindow
        });
        marker.info = it;
        marker.addListener('click', function () {
            openInfoBox(this.info, 'concorrente');
        });
        marker.setMap(map);
    }
}

function updateNegozi(list, map) {
    for (i = 0; i < list.length; i++) {
        var it = list[i];
        var location = {
            lat: it.recapito_latitudine,
            lng: it.recapito_longitudine
        };

        var marker;

        if (MarkerWithLabel) {

            var url = 'img/80x80/' + it.logo;
            $iconTemplate = $('<img/>').attr('src', url);

            marker = new MarkerWithLabel({
                position: location,
                map: map,
                //icon: {
                //    size: new google.maps.Size(60, 60)
                //},
                icon: " ",
                labelContent: $('<div/>').append($iconTemplate).html(),
                labelAnchor: new google.maps.Point(30, 30),
                labelClass: 'marker-logo'
            });
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map,
                title: it.nome,
                label: useLabel ? it.nome : null,
                icon: getLogo(it.logo, 60, 60) //to change
            });
        }

        var infowindow = new google.maps.InfoWindow({
            content: it.nome
        });
        //add to the list
        Markers.negozi.push({
            marker: marker,
            window: infowindow
        });
        marker.info = it;
        marker.addListener('click', function () {
            openInfoBox(this.info, 'negozio');
        });
        marker.setMap(map);

    }
}

function updateCartelli(list, map, fitbounds) {
    var bounds = new google.maps.LatLngBounds(); //update map bounds
    for (i = 0; i < list.length; i++) {
        var it = list[i];
        var location = {
            lat: it.recapito_latitudine,
            lng: it.recapito_longitudine
        };

        var markerSettings = {
            position: location,
            map: map,
            title: it.codice,
            label: useLabel ? it.codice : null,
        };
        if (it.abilitato) {
            //i cartelli temporanei non hanno icona
            markerSettings.icon = getIcon(it.tipo_cartello_icona);
        }
        var marker = new google.maps.Marker(markerSettings);
        var infowindow = new google.maps.InfoWindow({
            content: it.codice
        });
        //add to the list
        Markers.cartelli.push({
            marker: marker,
            window: infowindow
        });
        marker.info = it;
        marker.addListener('click', function () {
            openInfoBox(this.info, 'cartello');
        });
        bounds.extend(marker.getPosition());
        marker.setMap(map);
    }
    if (fitbounds)
        map.fitBounds(bounds);
}

//Marker clicks event handlers
function openInfoBox(data, type) {
    log('pressed marker');
    $container = $('#map-info-container');
    $container.find('.info-container').remove();
    $container.find('.info-container-image').remove();
    $container.show(100);
    var temporaneo = type == 'cartello' && !data.abilitato ? true : false;
    if (temporaneo)
        $container.find('.window-title').html(type + " temporaneo");
    else
        $container.find('.window-title').html(type);
    $divGeneral = buildContainerTable();
    $divIndirizzo = buildContainerTable();
    switch (type) {
        case 'concorrente':
        case'negozio':
            $divGeneral.find('table').append(getTableRow("Nome", data.nome));
            if (type === 'negozio')
                $divGeneral.find('table').append(getTableRow("Cliente", data.cliente_nome));
            else
                $divGeneral.find('table').append(getTableRow("Rivale", data.cliente_nome));
            $divGeneral.find('table').append(getTableRow("Descrizione", data.descrizione));

            $divIndirizzo.find('table').append(getTableRow("Provincia", data.recapito_provincia));
            $divIndirizzo.find('table').append(getTableRow("Comune", data.recapito_comune));
            $divIndirizzo.find('table').append(getTableRow("CAP", data.recapito_cap));
            $divIndirizzo.find('table').append(getTableRow("Via", data.recapito_via + " " + data.recapito_num_civico));
            $divIndirizzo.find('table').append(getTableRow("Latitudine", data.recapito_latitudine));
            $divIndirizzo.find('table').append(getTableRow("Longitudine", data.recapito_longitudine));
            $container.append($divGeneral);
            $container.append($divIndirizzo);
            $divImg = buildContainerImage(data.logo);
            $container.append($divImg);
            break;
        case 'cartello':
            $divGeneral.find('table').append(getTableRow("Codice", data.codice));
            $divGeneral.find('table').append(getTableRow("Cliente", data.cliente_nome));
            $divGeneral.find('table').append(getTableRow("Negozio", data.negozio_nome));
            if (data.fornitore_nome)
                $divGeneral.find('table').append(getTableRow("Fornitore", data.fornitore_nome));
            $divGeneral.find('table').append(getTableRow("Tipo", data.tipo_cartello_nome));
            $divGeneral.find('table').append(getTableRow("Data dal", data.data_dal));
            $divGeneral.find('table').append(getTableRow("Data al", data.data_al));
            $divIndirizzo.find('table').append(getTableRow("Provincia", data.recapito_provincia));
            $divIndirizzo.find('table').append(getTableRow("Comune", data.recapito_comune));
            $divIndirizzo.find('table').append(getTableRow("CAP", data.recapito_cap));
            $divIndirizzo.find('table').append(getTableRow("Via", data.recapito_via + " " + data.recapito_num_civico));
            $divIndirizzo.find('table').append(getTableRow("Ubicazione", data.recapito_ubicazione));
            $divIndirizzo.find('table').append(getTableRow("Latitudine", data.recapito_latitudine));
            $divIndirizzo.find('table').append(getTableRow("Longitudine", data.recapito_longitudine));
            $container.append($divGeneral);
            $container.append($divIndirizzo);
            var gallery = data.gallery;
            if (Array.isArray(gallery)) {
                gallery.unshift(data.foto);
            }
            else {
                gallery = [data.foto];
            }
            $divImg = buildContainerImage(data.foto, gallery);
            $container.append($divImg);
            break;
    }
}

function buildContainerTable() {
    $div = $('<div>').addClass('info-container');
    $div.append($('<table>'));
    return $div;
}

function buildContainerImage(file, list) {
    $div = $('<div>').addClass('info-container-image');
    if (file === null)
        file = 'default';
    //$div.css('background', 'url(img/320x240/' + file + ') no-repeat center');
    //$div.css('background-size', '320px 240px');
    $img = $('<img/>').attr('src', 'img/320x240/' + file );
    $div.append($img);
    if (list !== undefined && list !== false && file != 'default') {
        $div.css('cursor', 'pointer');
        $div.click(function () {

            $('<div/>').box({
                load: {
                    loadTemplate: {
                        template: 'gallery/show',
                        paramsTemplate: list[0]
                    }
                },
                success: function () {
                },
                overlay: true,
                classCss: 'layer-image'
            });

            setTimeout(function () {
                $(".window-content-viewer").imageViewer({
                    imgRef: list,
                    useImgList: true,
                    debug: false
                });
            }, 500);
        });
    }
    return $div;
}

function getTableRow(name, value) {
    var str = '<tr><td class="line">%name%</td><td>%value%</td></tr>';
    str = str.replace('%value%', value).replace('%name%', name);
    return str;
}

function getImageHtml(file) {
    return '<img src="img/80x80/' + file + '" style="{width:100px, height:100px}"></img>';
}

function askRemote(url, callback, data) {
    log('asking remote: ' + url);
    if (data === undefined) {
        data = getForm().serializeObject();
    }
    $.ajax({
        dataType: "json",
        url: url,
        data: data,
        success: function (result, status) {
            log(status);
            if (result.success) {
                callback(result.data);
            } else {
                console.log(result.message);
            }
        },
        error: function (error) {
            console.log('something happened');
            console.log(error);
        }
    });
}

function getForm() {
    return $('.action-filtro-map').closest('form');
}

function removeMarkers() {
    setMapOnAll(null, Markers.cartelli);
    setMapOnAll(null, Markers.rivali);
    setMapOnAll(null, Markers.negozi);
    Markers.cartelli = [];
    Markers.negozi = [];
    Markers.rivali = [];
    log('markers removed');
}

// Sets the map on all markers in the array.
function setMapOnAll(map, markers) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].marker.setMap(map);
    }
}

function toggleInfoBox(markers) {
    if (useLabel) {
        for (i = 0; i < markers.length; i++) {
            markers[i].window.open(map, markers[i].marker);
        }
    } else {
        for (i = 0; i < markers.length; i++) {
            markers[i].window.close();
        }
    }
}

//Init template
$(function () {

    $('body').on('click', '.action-filtro-map, .action-reset-filtro', function () {
        if (map !== undefined) {
            removeMarkers();
            init(false);
        }
    });
    $('body').on('click', '.action-chiudi-info', function () {
        $('#map-info-container').hide(500);
    });
    $('body').on('click', '.action-toggle-infobox', function () {
        useLabel = !useLabel;
        toggleInfoBox(Markers.cartelli);
        toggleInfoBox(Markers.rivali);
        toggleInfoBox(Markers.negozi);
    });

    $('body').on('click', '.action-cerca', function () {
        var value = $('input[name=ricerca]').val();
        findLocation(value);
    });

    $('body').on('keypress', 'input[name=ricerca]', function (e) {
        if (e.which == 13) {
            var value = $('input[name=ricerca]').val();
            findLocation(value);
        }
    });
});

//

function findLocation(text) {
    if (geocoder === undefined)
        return;
    geocoder.geocode({'address': text}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.fitBounds(results[0].geometry.viewport);
        } else {
            M.alert('Impossibile individuare la posizione indicata');
        }
    });
}

function getIcon(file) {
    return {
        url: 'img/80x80/' + file,
        size: new google.maps.Size(40, 40),
        scaledSize: new google.maps.Size(35, 35)
    };
}

function getLogo(file, width, height) {
    var url = 'img/80x80/' + file;
    //width = width > 100 ? 100 : width;
    //height = height > 100 ? 100 : height;
    //console.log(width, height, file);
    return {
        url: url,
        size: new google.maps.Size(width, height),
        scaledSize: new google.maps.Size(width, height)
    };
}

function log(str) {
    if (true) {
        //console.log(str);
    }
}

function printJson(value) {
    log(JSON.stringify(value));
}
