H.data.onLoaded('link', '.link',
	function($this) {
		if ($this.attr('href') == null || $this.attr('href') == '') {
			$this.attr('href', 'javascript:void(0)');
		}
		else {
			$this.on('click', function() {
				H.loading.show();
			});
		}
	});