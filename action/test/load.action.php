<?php

$user = H::session('user');
if ($user != null) {
	$sessionid = H::session()->sessionId();
	H::view('user', $user);
	H::view('sessionid', $sessionid);
}