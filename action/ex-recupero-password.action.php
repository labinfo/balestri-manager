<?php
H::session()->invalidate();

$e_mail = strtolower(H::input('e_mail', ''));

if ($e_mail == 'superadmin') {
	H::fire('view');
}

$params = [
	'e_mail' => $e_mail
];
$row = H::db()->select('utente', $params)->item();
if ($row) {
	$id = $row['id'];
	if ($row['abilitato']) {
		$e_mail = $row['e_mail'];
		$nome = $row['nome'];
		
		$random = HUtil::randomCode(7);
		$random2 = HUtil::randomCode(3);
		$cr = new HCryptNumber();
		$id_mask = $cr->encode($id);
		$token = sha1('reset-password' . $id . $id_mask . H::config('password_enc', '') . $random) . $random . $random2;
		$url = H::config('url');
		$url .= '/reset-password/' . $id_mask . '/' . $token;

		$params = [
			'data_recupero_password:_' => 'now()',
			'codice_recupero_password' => $token
		];
		H::db()->updateById('utente',
			$params,
			$id);
		
		Log::info('reset_password',
			'utente',
			$id);
		
		$params = [
				'nome' => $nome,
				'url' => $url,
			];
		H::mail()->send($e_mail, 'Recupero password JEAN', 'link_recupero_password', $params);
	}
	else {
		Log::error('reset_password',
			'utente',
			$id,
			'account disabilitato');
		H::hson()->error('Il tuo account risulta disabilitato, impossibile procedere con l\'operazione richiesta');
	}
}
else {
	Log::error('reset_password',
		'utente',
		0,
		'e_mail: ' . $e_mail);
}