<?php
$params = [
	'abilitato' => 1,
	'id' => H::session('user')['id']
];

$row = H::db()->select('utente', $params)->item();
if ($row) {
	$password = H::input('password');
	$conferma_password = H::input('conferma_password');
	if ($password == $conferma_password) { 
		H::lib('Utils');
		if (Utils::check_password($password)) {
			$id = $row['id'];

			H::lib('Utente');
			H::db()->beginTransaction();
			Utente::imposta_password($id, $password);
			H::db()->commit();

			Log::edit('update_password',
				'utente',
				$id);

			$user = H::session('user');
			$user['imposta_password'] = 0;
			H::session()->put('user', $user);
			$message = null;
		}
		else {
			$message = 'La password deve essere lunga almeno 8 caratteri, contenere almeno una lettera maiuscola, una lettera minuscola e un numero';
		}
	}
	else {
		$message = 'Conferma password non corrisponde';
	}
}
else {
	$message = 'Parametri non validi o utente disabilitato o non presente in archivio';
}

H::hson()->error($message);