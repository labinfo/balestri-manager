<?php
if (H::session()->isAuth()) {
	$id = H::session('user')['id'];
	$user = H::db()->selectOne('utente', $id)->item();
	$user_info = [
		'num_richieste_contatto_nuovo' => $user['num_richieste_contatto_nuovo'],
	];
	H::view('user_info', $user_info);
}