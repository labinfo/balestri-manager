<?php
$image = H::input('image', '');
$dim = H::input('dim', '');

$download_ctype = 'image/jpg';
$download_file_name = 'default.jpg';
$download_file_path = HSystem::path('img_thumbs') . '/default.jpg';
$download_content_disposition = 'inline';

if ($image != '' && $dim != '') {
	$path = HSystem::path('img_thumbs') . '/' . $dim . '/' . $image;
	if (file_exists($path)) {
		$download_file_path = $path;
		$download_file_name = $image;
		$php_info = pathinfo($image);
		$download_ctype = 'image/' . strtolower($php_info['extension']);
	}
}

H::view('download_ctype', $download_ctype);
H::view('download_file_name', $download_file_name);
H::view('download_file_path', $download_file_path);
H::view('download_content_disposition', $download_content_disposition);