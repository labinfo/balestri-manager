<?php
if (H::session()->isAuth()) {
	$ip = HUtil::get_client_ip();
	$file = HSystem::path('/temp/blocco_ip') . '/' . $ip . '.ip';
	if ($ip == 'UNKNOWN' || file_exists($file)) {
		H::fire('invalid-authentication');
	}
}
else {
	H::fire('invalid-authentication');
}