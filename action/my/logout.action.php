<?php
$id = 0;
H::lib('Log');
if (H::session()->isAuth()) {
	$id = H::session('user')['id'];
	Log::accesso('logout', $id);
}
Log::info('logout',
	'utente',
	$id);
H::session()->invalidate();