<?php
H::lib('Utils');
$file = Utils::check_download();
if ($file != null) {
	$file_name = $file['nome'];
	$file_path = HSystem::path('files/' . $file['path']) . '/' . $file['id'];
	if (file_exists($file_path)) {
		H::view('download_file_name', $file_name);
		H::view('download_file_path', $file_path);
	}
}