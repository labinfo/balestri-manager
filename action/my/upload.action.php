<?php
$file = H::input()->getFile('file');
$php_info = pathinfo($file['name']);
$estensione = strtolower(@$php_info['extension']);
if ($estensione == '') {
	H::hson()->error('Formato file non valido');
}
else {
	$file_name = 'file_' . uniqid() . '_' . time();
	$path_file = HSystem::path('temp/file', true) . '/' . $file_name;
	move_uploaded_file($file['tmp_name'], $path_file);
	H::view('fileName', $file['name']);
	H::view('tempFile', $file_name);
}