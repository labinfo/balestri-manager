<?php
$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	$type = $_('type');
	H::db()->deleteById($type, $id);
	Log::edit_delete_real($type, $id);	
	
	H::hson()->success('Eliminazione avvenuta correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}