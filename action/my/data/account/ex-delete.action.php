<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	H::lib('Utils');
	H::lib('Profile');

	$params = [
		'id_utente_creatore' => null
	];
	$where = [
		'id_utente_creatore' => $id
	];
	H::db()->update('cartello', $params, $where);

	$qryList = [
		'DELETE FROM cartello_preferito WHERE id_utente IN ([:id])',
		'DELETE FROM entita_preferito WHERE id_utente IN ([:id])',
		'DELETE FROM utente_negozio WHERE id_utente IN ([:id])',
		'DELETE FROM utente_cliente WHERE id_utente IN ([:id])',
		'DELETE FROM utente_accesso_check WHERE id_utente > 1 AND id_utente IN ([:id])',
		'DELETE FROM utente_accesso WHERE id_utente > 1 AND id_utente IN ([:id])',
		'DELETE FROM utente WHERE id > 1 AND id IN ([:id])',
		'DELETE FROM utente_session WHERE id_utente > 1 AND id_utente IN ([:id])',
	];
	$params = [
		':id' => $id
	];
				
	H::db()->beginTransaction();
	Profile::update_user_profile($id);	
	H::db()->queryList($qryList, $params);
	Log::edit_delete_real('utente', $id);
	H::db()->commit();
	
	H::hson()->success('Eliminazione avvenuta correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
