<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', 0);
$message = null;
if ($id == 1) {
	$message = 'Autorizzazioni non valide per gestire questo utente';
}
else {
	$password = H::input('password', '');
	$conf_password = H::input('conferma_password', '');
	if ($password == $conf_password) {
		H::lib('Utente');
		H::db()->beginTransaction();
		Utente::imposta_password($id, $password);
		H::db()->commit();

		H::lib('Log');
		Log::edit('update_password',
			'utente',
			$id);
		H::hson()->success('Password modificata correttamente');
	}
	else {
		$message = 'Conferma password non valida';
	}
}
H::hson()->error($message);