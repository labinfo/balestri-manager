<?php
$params = [
    'id:>' => 1,
    'id_cliente' => 0,
    'profile:_>' => 8 //Mostra solo profili admin
];

H::lib('Query');

$order_auth = ['nome', 'cognome', 'e_mail', 'profile', 'abilitato', 'ultimo_accesso', 'data_inserimento'];
$list_filter = ['abilitati', 'ultimo_accesso', 'data_inserimento', 'find', 'profile'];

Query::list_items($_,
    $list_filter,
    'utente',
    'e_mail:asc',
    $order_auth,
    $params);