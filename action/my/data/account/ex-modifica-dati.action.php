<?php
H::lib('Utils');

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::session('user')['id'];
$message = null;

$e_mail = strtolower(H::input('e_mail', ''));

$errore_email = false;
if ($id == 1) {
	$e_mail = 'superadmin';
}
else {
	$params = [
		'id:!=' => $id,
		'e_mail' => $e_mail
	];
	$res = H::db()->select('utente', $params);
	if ($res->is()) {
		$errore_email = true;
	}
}

if ($errore_email) {
	$message = 'E-mail già presente in archivio';
}
else {
	$nome = H::input('nome', '');
	$cognome = H::input('cognome', '');
				
	$find = [
		$e_mail,
		$nome,
		$cognome,
	];
	H::lib('Utils');
	$find = Utils::find_string($find);
				
	$params = [
		'e_mail' => $e_mail,
		'nome' => $nome,
		'cognome' => $cognome,
		'find' => $find,
	];
	H::db()->updateById('utente', $params, $id);
				
	H::lib('Log');
	Log::edit_update('utente', $id, false);
        $user = H::session('user');
	if ($user['e_mail'] != $e_mail) {
		Log::edit('update_e_mail', 'utente', $id);
	}
	
	$user['e_mail'] = $e_mail;
	$user['nome'] = $nome;
	$user['cognome'] = $cognome;
	H::session()->put('user', $user);
	H::hson()->success('Dati salvati correttamente');
}
H::hson()->error($message);