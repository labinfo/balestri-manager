<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', '');
$abilita = H::input('abilita', '') == '' ? 0 : 1;
if ($id != '') {
	$id = explode(',', $id);
	H::db()->beginTransaction();
	H::lib('Utente');
	if ($abilita) {
		Utente::abilita($id);
	}
	else {
		Utente::disabilita($id);
	}
	H::db()->commit();
	
	H::context()->put('id', $id);
}
H::hson()->success($abilita ? 'Abilitazione avvenuta correttamente' : 'Blocco avvenuto correttamente');