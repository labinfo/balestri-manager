<?php
$id = H::input('id', H::context('id', 0));

$params = [
    'u.id' => $id
];
$item = H::db()->selectView('u.*, c.nome as cliente_nome',
    'utente u left outer join cliente c on u.id_cliente = c.id',
    $params)->item('utente');

if ($item != null) {
    if ($item['profile'] == 6) {
        $list = H::db()->selectById('utente_negozio', $id, 'id_utente')->listItemsByKey('id_negozio');
        if ($list != null && count($list) > 0) {
            $item['list_id_negozio'] = implode(',', $list);
        }
    }
    if ($item['profile'] == 9) {
        $list = H::db()->selectById('utente_cliente', $id, 'id_utente')->listItemsByKey('id_cliente');
        if ($list != null && count($list) > 0) {
            $item['list_id_cliente'] = implode(',', $list);
        }
    }

}
H::view('item', $item);