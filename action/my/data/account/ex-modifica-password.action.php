<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::session('user')['id'];
if ($id == 1) {
	H::hson()->error('Non è possibile modificare la password del super user');
	H::fire('view');
}
$password = H::input('password', '');
$conf_password = H::input('conferma_password', '');
$message = null;
if ($password == $conf_password) {
	$row = H::db()->selectOne('utente_accesso_check', $id, 'id_utente')->item();
	if ($row) {
		$old_password = H::input('old_password', '');
		$old_password = sha1($id . $old_password . $row['check_string']);

		$row = H::db()->selectById('utente_accesso', $id, 'id_utente')->item();
		$message = 'Vecchia password non valida';
		if ($row) {
			if ($old_password == $row['password']) {
				H::lib('Utente');
				H::db()->beginTransaction();
				Utente::imposta_password($id, $password);
				H::db()->commit();

				H::lib('Log');
				Log::edit('update_password',
					'utente',
					$id);
				$message = null;	
				H::hson()->success('Password modificata correttamente');
			}
		}
	}
}
else {
	$message = 'Conferma password non valida';
}

H::hson()->error($message);