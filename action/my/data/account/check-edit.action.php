<?php
$id = H::input()->getArray('id');
if ($id == null) {
	$id = H::input('id', '');
}
if ($id != null) {
	if (is_string($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'id' => $id,
		'profile:>=' => H::session('user')['profile']
	];
	$res = H::db()->select('utente', $params);
	if ($res->is()) {
		H::hson()->error('Autorizzazioni non valide per gestire gli utenti selezionati');
		H::fire('view');
	}
}