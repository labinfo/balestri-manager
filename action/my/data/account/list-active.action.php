<?php
$params = [
	'id:>' => 0,
	'abilitato' => 1,
	'profile' => $_('profile'),
];
$orderBy = 'cognome asc, nome asc';
$list = H::db()->select('utente', $params, $orderBy)->listItems('utente');

H::view($_('list', 'list'), $list);