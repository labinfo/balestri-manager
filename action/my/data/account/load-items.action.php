<?php
if ($_('input', false)) {
	$id = H::input('id');
}
else {
	$id = H::context('id');
}
if ($id != null) {
	if (!is_array($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'u.id:>' => 1,
		'u.id' => $id,
	];
	$list = H::db()->selectView('u.*, c.nome as cliente_nome', 'utente u left outer join cliente c on u.id_cliente = c.id', $params)->listItems('utente');

	H::view('list', $list);
}