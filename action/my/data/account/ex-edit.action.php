<?php
$id = H::input('id', 0);
$message = null;

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

if ($id == 1) {
	$message = 'Autorizzazioni non valide per gestire questo utente';
}
else {
	$e_mail = strtolower(H::input('e_mail'));
		
	$params = [
		'id:!=' => $id,
		'e_mail' => $e_mail
	];
	$res = H::db()->select('utente', $params);

	if ($res->is()) {
		$message = 'E-mail già presente in archivio';
	}
	else {
		H::lib('Profile');
		H::lib('Utils');
		
		$profile = H::input('profile', 0);
		if (Profile::toName($profile) == '') {
			$message = 'Profilo non valido';
		}
		else {
			if ($profile >= H::session('user')['profile']) {
				Utils::auth_violation();
			}
			
			$nuovo = $id <= 0;
			
			if ($nuovo) {
				$password = H::input('password', '');
				$conferma_password = H::input('conferma_password', '');
				if ($password != $conferma_password) {
					$message = 'Conferma password non corrisponde';
				}
				elseif (!Utils::check_password($password)) {
					$message = 'La password deve essere lunga almeno 8 caratteri, contenere almeno una lettera maiuscola, una lettera minuscolae un numero';
				}
			}
			
			if ($message == null) {
				if ($profile == 9) {
					$list = H::input()->getArrayClean('id_cliente_array');
					if ($list == null || count($list) == 0) {
						$message = 'Per un "operatore clienti" è necessario selezionare almeno un cliente';
					}
				}
				elseif ($profile == 6) {
					$list = H::input()->getArrayClean('id_negozio');
					if ($list == null || count($list) == 0) {
						$message = 'Per un "operatore negozio" è necessario selezionare almeno un negozio';
					}
				}
				if ($message == null) {
					H::lib('Utente');
					$utente = new Utente();
					$utente->loadFromInput();
					H::db()->beginTransaction();
					$id = $utente->save();
					if ($id) {
						H::db()->commit();
						
						H::context()->put('id', $id);
						H::hson()->success('Dati salvati correttamente');
					}
					else {
						H::hson()->success('Errore in fase di salvataggio dati');
					}
				}
			}
		}
	}
}
H::hson()->error($message);