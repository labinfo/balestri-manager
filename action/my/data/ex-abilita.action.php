<?php
$id = H::input('id', '');
$abilita = H::input('abilita', '') == '' ? 0 : 1;
if ($id != '') {
	$id = explode(',', $id);

	$type = $_('type');
	$params = [
		'abilitato' => $abilita
	];
	H::db()->updateById($type, $params, $id);
	
	Log::edit_abilita($type, $id, false);
	
	H::context()->put('id', $id);
	H::hson()->success($abilita ? 'Abilitazione avvenuta correttamente' : 'Blocco avvenuto correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
