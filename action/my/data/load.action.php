<?php
$id = H::input('id', H::context('id', 0));

$type = $_('type');
$table = $_('table', $type);
$paramId = $_('paramId', 'id');

$item = H::db()->selectOne($table, $id, $paramId)->item($type);
H::view('item', $item);