<?php
if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}
$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);
				
	H::db()->deleteById('entita_documento', $id);
	Log::edit_delete_real('entita_documento', $id);
		
	H::hson()->success('Eliminazione avvenuta correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
