<?php
H::lib('Query');

$list_filter = null;
$order_auth = ['commento'];

$params = [
	'ed.id_risorsa_file:_' => 'rf.id',
	'ed.tipo_entita' => $_('type')
];
Query::list_items_type($_,
	$list_filter,
	'ed.*,
	rf.nome,
	rf.data_inserimento',
	'entita_documento ed,
	risorsa_file rf',
	'entita_documento',
	'rf.data_inserimento:desc',
	$order_auth,
	$params);