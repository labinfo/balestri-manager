<?php
$id = H::input('id', H::context('id', 0));

$params = [
	'ed.id_risorsa_file:_' => 'rf.id',
	'ed.id' => $id,
];
$item = H::db()->selectView('ed.*,
	rf.nome,
	rf.data_inserimento',
	'entita_documento ed,
	risorsa_file rf', 
	$params)->item('entita_documento');
H::view('item', $item);