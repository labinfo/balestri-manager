<?php
if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}
$id = H::input('id', 0);
$nuovo = $id <= 0;
	
$file_name = H::input('file_name', '');
if ($file_name == '') {
	H::hson()->error('Selezionare un file');				
}
else {
	$php_info = pathinfo($file_name);
	$estensione = strtolower(@$php_info['extension']);
	if ($estensione == '') {
		H::hson()->error('Estensione file non valida');
	}
	else {
		H::lib('Utils');
		$file = Utils::add_file();
		if ($file == null) {
			H::hson()->error('File non valido');
		}
		else {
			$file_id = $file['id'];
			
			$params = [
				'id_risorsa_file' => $file_id,
				'abilitato' => H::input('abilitato', 0),
				'commento' => H::input('commento', ''),
			];
			if ($nuovo) {
				$params['id_entita'] = H::input('id_entita', 0);
				$params['tipo_entita'] = $_('type');
				$id = H::db()->insert('entita_documento', $params);
			}
			else {
				H::db()->updateById('entita_documento', $params, $id);
			}
			
			H::context()->put('id', $id);
			Log::edit_update('entita_documento', $id, $nuovo);
			H::hson()->success('Dati salvati correttamente');
		}
	}
}
