<?php
if ($_('input', false)) {
	$id = H::input('id');
}
else {
	$id = H::context('id');
}
if ($id != null) {
	if (!is_array($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'ed.id_risorsa_file:_' => 'rf.id',
		'ed.id' => $id,
	];
	$list = H::db()->selectView('ed.*,
		rf.nome,
		rf.data_inserimento',
		'entita_documento ed,
		risorsa_file rf', 
		$params)->listItems('entita_documento');

	H::view('list', $list);
}