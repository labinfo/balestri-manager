<?php
$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	$params = [
		'eliminato' => 0
	];
	$type = $_('type');
	H::db()->updateById($type, $params, $id);

	Log::edit_restore($type, $id);
	
	H::context()->put('id', $id);
	H::hson()->success('Ripristino avvenuto correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
