<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', '');

$row = H::db()->selectById('blocco_ip', $id);

if ($row->is()) {
	H::hson()->error('IP già presente nell\'elenco blocco');
}
else {
	H::db()->beginTransaction();

	H::db()->deleteById('blocco_ip', $id);
	$params = [
		'ip' => $id,
		'data_inserimento:_' => 'now()',
	];
	H::db()->insert('blocco_ip', $params);

	$file = HSystem::path('/temp/blocco_ip', true) . '/' . $id . '.ip';
	file_put_contents($file, $id, LOCK_EX);

	H::context('id', $id);
	H::db()->commit();
	H::hson()->success('IP bloccato');
}
