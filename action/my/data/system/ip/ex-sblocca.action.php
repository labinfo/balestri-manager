<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', '');

if ($id != '') {
	$id = explode(',', $id);
	H::db()->deleteById('blocco_ip', $id, 'ip');

	foreach ($id as $i) {
		$file = HSystem::path('/temp/blocco_ip', true) . '/' . $i . '.ip';
		@unlink($file);
	}

	H::hson()->success('IP sbloccato');
}
else {
	H::hson()->error('nessun IP selezionato');
}