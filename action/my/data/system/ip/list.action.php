<?php
H::lib('Query');

$order_auth = ["ip", "data_inserimento"];
$list_filter = null;

Query::list_items($_,
	$list_filter,
	'blocco_ip',
	'data_inserimento:desc',
	$order_auth);