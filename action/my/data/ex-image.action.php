<?php

H::lib("ImageManager");
$id = H::input('id', -1);
$file = H::input()->getFile('file');
$php_info = pathinfo($file['name']);
$estensione = strtolower($php_info['extension']);

$params = [
	'estensione' => $estensione,
	'nome' => $file['name'],
	'data_inserimento:_' => 'now()',
];
$image_id = H::db()->insert('risorsa_immagine', $params);
$image_name = $image_id . '.' . $estensione;

$path_file = HSystem::path('data/img', true) . '/' . $image_id;
move_uploaded_file($file['tmp_name'], $path_file);

$key = $_('name', 'foto');
$type = $_('type');
switch ($key) {
	case 'icona':
		$imageDimList = [
			['width' => 320, 'height' => 240],
			['width' => 80, 'height' => 80]
		];
		break;
	case 'foto':
	case 'immagine':
	case 'gallery':
	case 'logo':
//	$imageDimList = [
//		['width' => 320, 'height' => 240],
//		['width' => 640, 'height' => 480],
//		['width' => 1280, 'height' => 960],
//	];
		$imageDimList = ImageManager::$imageDim;
		break;
}
HImage::createThumbs($path_file, $image_name, $imageDimList, HSystem::path('img_thumbs', true));

if ($key == 'gallery') {
	$row = H::db()->selectOne($type, $id)->item();
	if (isset($row['gallery'])) {
		$gallery = unserialize($row['gallery']);
		if (!is_array($gallery)) {
			$gallery = [];
		}
	} else {
		$gallery = [];
	}
	$foto = $image_name;
	$gallery[] = $image_name;
	$image_array = serialize($gallery);

	$params = [
		'id' => $image_id,
		'id_entita' => $id,
		'tipo_entita' => $type,
		'immagine' => $foto,
	];
	H::db()->insert('entita_gallery', $params);
	$params = [
		$key => $image_array
	];
} else {
	$params = [
		$key => $image_name
	];
}
H::context()->put('id', $id);
H::context()->put('image_name', $image_name);
H::db()->updateById($type, $params, $id);
Log::edit_update($type, $id, false);

H::hson()->success('Immagine caricata correttamente');
