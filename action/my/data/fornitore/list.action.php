<?php
H::lib('Query');

$params = [
	'id:_>' => 0
];

$order_auth = ['nome', 'descrizione'];
$list_filter = null;

Query::list_items($_,
	$list_filter,
	'fornitore',
	'nome:asc',
	$order_auth,
	$params);