<?php

$id = H::input('id', '');

if ($id != '') {
    $id = explode(',', $id);

    $type = 'fornitore';
    try {
        H::db()->deleteById($type, $id);
        Log::edit_delete_real($type, $id);
        H::hson()->success('Eliminazione avvenuta correttamente');
    } catch (Exception $e) {
        H::hson()->error('Impossibile eliminare un fornitore assegnato a qualche cartello.');
    }

    
} else {
    H::hson()->error('Nessuna voce selezionata');
}