<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', -1);
$nome = H::input('nome');
$descrizione = H::input('descrizione', '');

$params = [
	'nome' => $nome,
	'descrizione' => $descrizione
];

$nuovo = $id < 0;

if ($nuovo) {
	$id = H::db()->insert('fornitore', $params);
} else {
	H::db()->updateById('fornitore', $params, $id);
}

H::context()->put('id', $id);
Log::edit_update('fornitore', $id, $nuovo);
H::hson()->success('Dati salvati correttamente');