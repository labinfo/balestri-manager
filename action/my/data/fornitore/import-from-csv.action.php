<?php

//should load from a predefined folder data from a 
//specific csv file
H::lib('Utils');
H::lib('FtpClient');

//$logger = new Logger('./import.txt');
H::log('import')->debug('Called import for Fornitori.csv');

/*
//download del file da ftp
$ftp = new FtpClient(FtpClient::SERVER);
if (!$ftp->login(FtpClient::USER, FtpClient::PASSWORD)) {
    H::hson()->error('Impossibile collegarsi al server ftp');
    $logger->log('connection error');
    return;
}

$ftp->setLocalFolder(HSystem::path(Utils::getSharedFileFolder() . '/'));
if (!$ftp->download('Fornitori.csv')) {
    H::hson()->error('Impossibile scaricare file da ftp');
    $logger->log('download error');
    return;
}
*/

//import del materiale
$filePath = Utils::getSharedFileFolder() . "/Fornitori.csv";
//$filePath = HSystem::path($filePath, true);

if (!file_exists($filePath)) {
    H::hson()->error('File non trovato in locale');
    return;
}

$handle = fopen($filePath, 'r');
$query = '';
$ids = array();
$errore = FALSE;
while (($item = fgetcsv($handle, 1000, ';')) !== FALSE) {

    $id = intval($item[0]);
    $name = Utils::cleanString($item[1]);
    $query = "INSERT INTO fornitore (id, nome) VALUES('$id', \"$name\") "
            . "ON DUPLICATE KEY UPDATE nome=\"$name\";";
    try {
        H::db()->query($query);

    } catch (Exception $e) {
        H::log('import')->debug('Valore non importato: ' . $id . " " . $name);
        H::log('import')->debug('Error('.$e->getCode().'): ' . $e->getMessage());
        $errore = true;
    }
    $ids[] = $id;
}

if ($errore == true) {
    H::hson()->error('errore formato dati');
    return;
}

//H::db()->query($query);
///
//update del context
$id = implode(',', $ids);
H::context()->put('id', $id);
//Log::edit_update($type, $id, false);
H::hson()->success('Elenco caricato correttamente');

