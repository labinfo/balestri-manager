<?php
if (H::session('user')['sola_lettura']) {
    Utils::auth_violation();
}
$id = H::input('id', '');
if ($id != '') {
    $id = explode(',', $id);

    H::lib('Utils');
    $type = $_('type');

    $params_gallery = [
        'id' => $id
    ];
    $list_entita = H::db()->select('entita_gallery', $params_gallery)->listItemsByKey('id_entita');

    H::db()->beginTransaction();
    H::db()->deleteById('entita_gallery', $id);

    $params = [
        'tipo_entita' => $type,
    ];
    $orderBy = 'ordine desc';

    foreach ($list_entita as $id_entita) {

        $params['id_entita'] = $id_entita;
        $list = H::db()->select('entita_gallery', $params, $orderBy)->listItemsByKey('immagine');
        $gallery = [];

        foreach ($list as $img) {
            $gallery[] = $img;
        }

        $params_type = [
            'gallery' => serialize($gallery)
        ];

        H::db()->updateById($type, $params_type, $id_entita);
    }

    H::db()->commit();
    Log::edit_delete_real($type, $id);

    H::hson()->success('Eliminazione avvenuta correttamente');
} else {
    H::hson()->error('Nessuna voce selezionata');
}
