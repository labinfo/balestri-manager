<?php
H::lib('Query');

$order_auth = ['ordine'];
$list_filter = null;

$id_entita = H::input('id','');

$params = [
	'tipo_entita' => $_('type')
];

if($id_entita != ''){
	$params['id_entita'] = $id_entita;
}

Query::list_items($_,
	$list_filter,
	'entita_gallery',
	'ordine:desc',
	$order_auth,
	$params);