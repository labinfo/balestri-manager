<?php
$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	$type = $_('type');
	$params = [
		'eliminato' => 1
	];
	H::db()->updateById($type, $params, $id);
	Log::edit_delete($type, $id);	
	
	H::hson()->success('Eliminazione avvenuta correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
