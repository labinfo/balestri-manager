<?php
$id = H::input('id', 0);
$tipo_risorsa = H::input('type', '');

$params = [
	'ultimo' => 1,
	'id_risorsa' => $id,
	'tipo_risorsa' => $tipo_risorsa
];

$item = H::db()->select('log_risorsa', $params)->item('log_risorsa');
H::view('item', $item);