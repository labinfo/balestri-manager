<?php
H::lib('Query');

$params = [
	'id_risorsa' => H::input('id', 0),
	'tipo_risorsa' => H::input('type', '')
];
$order_auth = ['data_operazione'];
$list_filter = ['data_operazione'];

$list = Query::list_items_type_return($_,
	$list_filter,
	'*',
	'log_risorsa',
	'log_risorsa',
	'data_operazione:desc',
	$order_auth,
	$params);

foreach($list as &$item){
	if($item['dati'] != null){
		$dati = unserialize($item['dati']);
		$keys = array_keys($dati);
		foreach($keys as $key){
			$item[$key] = $dati[$key];
		}
		unset($item['dati']);
	}
}

H::view('list', $list);