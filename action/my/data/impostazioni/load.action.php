<?php
$def_max_tentativi_accesso = 5;
$def_void = '';
$settings = H::settings('*');
$params = [
	'max_tentativi_accesso' => $settings->get('max_tentativi_accesso', $def_max_tentativi_accesso),
	'email_notifiche_accessi' => $settings->get('email_notifiche_accessi', $def_void),
	'e_mail_supporto' => $settings->get('e_mail_supporto', $def_void),
];
H::view('item', $params);