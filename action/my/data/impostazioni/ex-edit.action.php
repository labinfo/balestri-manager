<?php
H::lib('Utils');

Utils::update_settings([
	'max_tentativi_accesso',
	'email_amministratore',
	'email_notifiche_accessi',
	'e_mail_supporto',
]);

H::lib('Log');
$id = 0;
Log::edit('update',
	'impostazioni',
	$id);
H::hson()->success('Impostazioni salvate correttamente');