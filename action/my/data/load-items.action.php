<?php
if ($_('input', false)) {
	$id = H::input('id');
}
else {
	$id = H::context('id');
}
if ($id != null) {
	if (!is_array($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'id' => $id,
	];
	$type = $_('type');
	$list = H::db()->select($type, $params)->listItems($type);

	H::view('list', $list);
}