<?php

H::lib('Query');

$order_auth = ['nome', 'numero_cartelli'];
$list_filter = null;

$profile = H::session('user')['profile'];

if (in_array($profile, [6, 8, 9])) {
    if ($profile == 9) {
        $id_utente = H::session('user')['id'];
        $id = H::db()->selectViewById('id_cliente', 'utente_cliente',
            $id_utente, 'id_utente')->listItemsByKey('id_cliente');
    } else {
        $id = H::session('user')['id_cliente'];
    }
    $params = [
        'id' => $id
    ];
} else {
    //caso export solo per >= 10
    $params = [
        'id:_>' => 0
    ];
    $id = H::input('id_cliente', '');
    if ($id != '') {
        if (!is_array($id)) {
            $id = explode(',', $id);
        }
        $params = [
            'id' => $id
        ];
    }
}

Query::list_items($_, $list_filter, 'cliente', 'nome:asc', $order_auth, $params);
