<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', 0);
$nome = H::input('nome');	
$logo = H::input('filename', '');
$export_id = H::input('export_id', '');
$params = [
	'id' => $id,
	'nome' => $nome
];
$res = H::db()->query('select id from cliente where upper(nome) = upper(:nome) and id != :id', $params);
if ($res->is()) {
	H::hson()->error('Valore già presente');
}
else {
	$nuovo = $id <= 0;
		
	$params = [
		'abilitato' => H::input('abilitato', 0),
		'nome' => $nome,
        'logo' => $logo
	];
	if($export_id != ''){
		$params['export_id'] = $export_id;
	}
	if ($nuovo) {
		$id = H::db()->insert('cliente', $params);
	}
	else {
		H::db()->updateById('cliente', $params, $id);
	}
	
	H::context()->put('id', $id);
	Log::edit_update('cliente', $id, $nuovo);
	H::hson()->success('Dati salvati correttamente');
}
