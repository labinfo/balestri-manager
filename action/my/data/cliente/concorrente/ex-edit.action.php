<?php

if (H::session('user')['sola_lettura']) {
    Utils::auth_violation();
}

$id = H::input('id', 0);
$nuovo = $id <= 0;

$params = [
    'nome' => H::input('nome', ''),
    'descrizione' => H::input('descrizione', ''),
    'logo' => H::input('filename', ''),
    'recapito_latitudine' => H::input('latitudine', 0),
    'recapito_longitudine' => H::input('longitudine', 0),
    'recapito_via' => H::input('recapito_via', ''),
    'recapito_num_civico' => H::input('recapito_num_civico', ''),
    'recapito_cap' => H::input('recapito_cap', ''),
    'recapito_comune' => H::input('recapito_comune', ''),
    'recapito_id_provincia' => H::input('recapito_id_provincia', 0)
];
H::db()->beginTransaction();
if ($nuovo) {
    $params['id_cliente'] = H::input('id_cliente', 0);
    $id = H::db()->insert('concorrente', $params);
} else {
    H::db()->updateById('concorrente', $params, $id);
}

$params = [
    'id_concorrente' => $id
];
H::db()->delete('concorrente_negozio', $params);

$list_negozio = H::input()->getArrayClean('id_negozio');
if ($list_negozio != null && count($list_negozio) > 0) {
    foreach ($list_negozio as $ids) {
        $params['id_negozio'] = $ids;
        H::db()->insert('concorrente_negozio', $params);
    }
}

H::db()->commit();

H::context()->put('id', $id);
Log::edit_update('concorrente', $id, $nuovo);
H::hson()->success('Dati salvati correttamente');
