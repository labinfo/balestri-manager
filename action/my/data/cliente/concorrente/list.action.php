<?php

H::lib('Query');

$order_auth = ['nome', 'descrizione'];
$list_filter = null;
$params = [
    'c.id_cliente' => H::input('id_cliente', 0),
    'c.id_cliente:_' => 'cl.id',
    'p.id:_' => 'c.recapito_id_provincia'
];

$profile = H::session('user')['profile'];
if ($profile == 6) {
    //vede solo quelli a cui è abilitato
    $p['un.id_utente'] = H::session('user')['id'];
    $p['cn.id_negozio:_'] = 'un.id_negozio';
    $list = H::db()->selectView('cn.id_concorrente', 'utente_negozio un, concorrente_negozio cn', $p)->listItemsByKey('id_concorrente');
    if (count($list) > 0) {
        $params['c.id'] = $list;
    }
    else{
        H::view('list', $list);
        return;
    }
}

//H::lib('Logger');
//Logger::main()->log(print_r($params, true));

Query::list_items_type($_,
	$list_filter,
	'c.*,'
        . 'cl.nome as cliente_nome,'
        . 'p.nome as recapito_provincia',
	'cliente cl, provincia p, concorrente c',
	'concorrente',
	'nome:asc',
	$order_auth,
	$params);
