<?php

if (H::session('user')['sola_lettura']) {
    Utils::auth_violation();
}

$id = H::input('id', '');
if ($id != '') {
    $params = [
        'id_concorrente' => $id
    ];

    H::db()->beginTransaction();

    H::db()->delete('concorrente_negozio', $params);
    H::db()->deleteById('concorrente', $id);
    Log::edit_delete_real('concorrente', $id);

    H::db()->commit();
    H::hson()->success('Eliminazione avvenuta correttamente');
} else {
    H::hson()->error('Nessuna voce selezionata');
}
