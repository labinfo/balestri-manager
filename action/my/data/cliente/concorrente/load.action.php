<?php
$id = H::input('id', H::context('id', 0));

$params = [
	'id' => $id
];
$item = H::db()->selectView('*',
	'concorrente', 
	$params)->item();

if ($item != null) {
	$list = H::db()->selectById('concorrente_negozio', $id, 'id_concorrente')->listItemsByKey('id_negozio');
	if ($list != null && count($list) > 0) {
		$item['list_id_negozio'] = implode(',', $list);
	}
}

H::view('item', $item);