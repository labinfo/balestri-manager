<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	H::lib('Utils');
	H::db()->beginTransaction();
	$list_id = H::db()->selectById('cartello', $id, 'id_cliente')->listItemsByKey('id');
	Utils::delete_cartello($list_id);
	
	$qryList = [
		'DELETE FROM utente_negozio WHERE id_negozio IN (SELECT id FROM negozio WHERE id_cliente IN ([:id]))',
		'DELETE FROM negozio WHERE id_cliente IN ([:id])',
		'DELETE FROM utente_cliente WHERE id_cliente IN ([:id])',
		'DELETE FROM utente_accesso_check WHERE id_utente IN (SELECT id FROM utente WHERE id_cliente IN ([:id]))',
		'DELETE FROM utente_accesso WHERE id_utente IN (SELECT id FROM utente WHERE id_cliente IN ([:id]))',
		'DELETE FROM utente_session WHERE id_utente IN (SELECT id FROM utente WHERE id_cliente IN ([:id]))',
		'DELETE FROM utente WHERE id_cliente IN ([:id])',
		'DELETE FROM cliente WHERE id IN ([:id])',
	];
	$params = [
		':id' => $id
	];
	H::db()->queryList($qryList, $params);
		
	Log::edit_delete_real('cliente', $id);
	H::db()->commit();
	
	H::hson()->success('Eliminazione avvenuta correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
