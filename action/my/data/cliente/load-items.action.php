<?php
if ($_('input', false)) {
	$id = H::input('id');
}
else {
	$id = H::context('id');
}
if ($id != null) {
	if (!is_array($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'id' => $id,
	];
	$list = H::db()->select('cliente', $params)->listItems();

	H::view('list', $list);
}