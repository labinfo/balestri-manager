<?php

//should load from a predefined folder data from a 
//specific csv file

H::lib('Utils');
H::lib('FtpClient');

H::log('import')->debug('Called import for Clienti.csv');


/*
//download del file da ftp
$ftp = new FtpClient(FtpClient::SERVER);
if (!$ftp->login(FtpClient::USER, FtpClient::PASSWORD)) {
    H::hson()->error('Impossibile collegarsi al server ftp');
    H::log('import')->error('connection error');
    H::fire('view');
}
$ftp->setLocalFolder(HSystem::path(Utils::getSharedFileFolder() . '/'));
if (!$ftp->download('Clienti.csv')) {
    H::hson()->error('Impossibile scaricare file da ftp');
    H::log('import')->error('download error');
    H::fire('view');
}
*/
//import del materiale

$filePath = Utils::getSharedFileFolder() . "/Clienti.csv";
//$filePath = HSystem::path($filePath, true);

if (!file_exists($filePath)) {
    H::log('import')->debug('file non trovato in locale');
    H::hson()->error('file non trovato in locale');
    H::fire('view');
}

$handle = fopen($filePath, 'r');
$query = '';
$ids = [];
$errore = FALSE;
while (($item = fgetcsv($handle, 1000, ';')) !== FALSE) {
    $id = intval($item[0]);
    $name = Utils::cleanString($item[1]);
    $query = "INSERT INTO cliente (export_id, abilitato, nome) VALUES('$id', '1', '$name')
                ON DUPLICATE KEY UPDATE nome='$name';";
    try {
        H::db()->query($query);
        $id = H::db()->lastInsertId();
        H::log('import')->debug('processed id '. $id);
        if($id != 0){
            $ids[] = $id;
        }
    } catch (Exception $e) {
        H::log('import')->error('Valore non importato: ' . $id . " " . $name);
        H::log('import')->error('Error('.$e->getCode().'): ' . $e->getMessage());
        $errore = true;
    }
}

if ($errore == true) {
    H::hson()->error('errore formato dati');
    H::fire('view');
}

//update del context
$ids = implode(',', $ids);
H::context()->put('id', $ids);

H::hson()->success('Elenco caricato correttamente');
