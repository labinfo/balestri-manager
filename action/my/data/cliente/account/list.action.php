<?php
$profile_list = [6,8];
$params = [
	'profile' => $profile_list,
	'id_cliente' => H::input('id_cliente', 0)
];

H::lib('Query');

$order_auth = ['nome','cognome','e_mail','cliente_nome','abilitato','ultimo_accesso','data_inserimento'];
$list_filter = ['abilitati', 'ultimo_accesso', 'data_inserimento', 'find'];

Query::list_items($_,
	$list_filter,
	'utente',
	'e_mail:asc',
	$order_auth,
	$params);