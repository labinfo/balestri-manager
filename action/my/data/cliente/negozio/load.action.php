<?php
$id = H::input('id', H::context('id', 0));

$params = [
	'p.id:_' => 'n.recapito_id_provincia',
	'n.id' => $id,
        'n.id_cliente:_' => 'c.id'
];
$item = H::db()->selectView('n.*,
	p.nome as provincia_nome,
        c.nome as cliente_nome',
	'negozio n,
	provincia p,
        cliente c
        ', 
	$params)->item();
H::view('item', $item);