<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', 0);

$nuovo = $id <= 0;

$params = [
    'nome' => H::input('nome', ''),
    'descrizione' => H::input('descrizione', ''),
    'recapito_latitudine' => H::input('latitudine', 0),
    'recapito_longitudine' => H::input('longitudine', 0),
    'recapito_via' => H::input('recapito_via', ''),
    'recapito_num_civico' => H::input('recapito_num_civico', ''),
    'recapito_cap' => H::input('recapito_cap', ''),
    'recapito_comune' => H::input('recapito_comune', ''),
    'recapito_id_provincia' => H::input('recapito_id_provincia', 0)
];
if ($nuovo) {
    $params['id_cliente'] = H::input('id_cliente', 0);
    $id = H::db()->insert('negozio', $params);
} else {
    H::db()->updateById('negozio', $params, $id);
}

H::context()->put('id', $id);
Log::edit_update('negozio', $id, $nuovo);
H::hson()->success('Dati salvati correttamente');
