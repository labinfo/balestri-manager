<?php

if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}

$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	H::lib('Utils');
	$list_check = [
		'cartello' => 'id_negozio',
	];
	if (Utils::check_uso($id, $list_check)) {
		H::hson()->error('Voce in uso impossibile eliminare');
	}
	else {
		$qryList = [
			'DELETE FROM utente_negozio WHERE id_negozio IN ([:id])',
			'DELETE FROM negozio WHERE id IN ([:id])',
		];
		$params = [
			':id' => $id
		];
					
		H::db()->beginTransaction();
		H::db()->queryList($qryList, $params);
		Log::edit_delete_real('negozio', $id);
		H::db()->commit();
	
		H::hson()->success('Eliminazione avvenuta correttamente');
	}
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
