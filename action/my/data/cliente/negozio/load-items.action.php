<?php
if ($_('input', false)) {
	$id = H::input('id');
}
else {
	$id = H::context('id');
}
if ($id != null) {
	if (!is_array($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'p.id:_' => 'n.recapito_id_provincia',
		'n.id' => $id,
	];
	$list = H::db()->selectView('n.*,
		p.nome as provincia_nome',
		'negozio n,
		provincia p', 
		$params)->listItems();

	H::view('list', $list);
}