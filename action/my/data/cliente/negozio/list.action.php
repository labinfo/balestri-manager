<?php
H::lib('Query');

$order_auth = ['nome','provincia_nome'];
$list_filter = null;
$params = [
	'p.id:_' => 'n.recapito_id_provincia',
	'n.id_cliente' => H::input('id_cliente', 0),
        'cl.id:_' => 'n.id_cliente'
];

$profile = H::session('user')['profile'];
if ($profile == 6) {
    //vede solo quelli a cui è abilitato
    $list = H::db()->selectById('utente_negozio',
            H::session('user')['id'], 'id_utente')->listItemsByKey('id_negozio');
    $params['n.id'] = $list;
}

Query::list_items_type($_,
	$list_filter,
	'n.*,
	p.nome as provincia_nome,
        cl.nome as cliente_nome',
	'negozio n,
	provincia p, cliente cl',
	'negozio',
	'nome:asc',
	$order_auth,
	$params);