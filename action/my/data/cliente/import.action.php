<?php

H::lib('Logger');

//$id = H::input('id', -1);
$file = H::input()->getFile('file');
$php_info = pathinfo($file['name']);
$estensione = strtolower($php_info['extension']);

$params = [
    'estensione' => $estensione,
    'nome' => $file['name'],
    'data_inserimento:_' => 'now()',
];
$type = 'cliente';

$_id = H::db()->insert('risorsa_file', $params);
$_name = $_id . '.' . $estensione;
//upload del file
$path_file = HSystem::path('data/csv', true) . '/' . $_id;
$success = move_uploaded_file($file['tmp_name'], $path_file);
if (!$success) {
    H::hson()->error('Errore durante il caricamento del file.');
    return;
}

//Fetch e update del db

$ps = H::db()->prepare('load data infile \'' . $path_file
        . '\' into table cliente '
        . ' fields terminated by \'|\' OPTIONALLY ENCLOSED BY \'"\' '
        . ' lines terminated by \'\n\' '
        . ' ignore 1 lines '
        . '(`id`,`abilitato`,`nome`)'
);

$params = [];

$list = $ps->exe($params)->ListItemsByKey('id');
//update del context
$id = implode(',', $list);
H::context()->put('id', $id);
Log::edit_update($type, $id, false);
H::hson()->success('Elenco caricato correttamente');