<?php
H::lib('Query');
H::lib('Logger');

$order_auth = ['cliente_nome', 'negozio_nome', 'provincia_nome',
    'codice_int', 'data_dal', 'data_al', 'fornitore_nome', 'tipo_cartello_nome', 'ubicazione'];
$list_filter = ['id_fornitore', 'id_negozio', 'data', 'id_tipo_cartello', 'e.recapito_id_provincia'];
$params = [
    'e.recapito_id_provincia:_' => 'p.id',
    'e.id_tipo_cartello:_' => 't.id',
    'e.id_negozio:_' => 'n.id',
    'e.id_cliente:_' => 'c.id',
    'e.id_fornitore:_' => 'f.id'
];

///
$profile = H::session('user')['profile'];
if ($profile == 8 || $profile == 6) {
    $id_cliente = H::session('user')['id_cliente'];
    $params['e.id_cliente'] = $id_cliente;
}
if ($profile == 6) {
    //vede solo quelli a cui è abilitato
    $list = H::db()->selectById('utente_negozio',
        H::session('user')['id'], 'id_utente')->listItemsByKey('id_negozio');
    $params['n.id'] = $list;
}
if($profile == 9){
    $id_utente = H::session('user')['id'];
    $params['e.id_cliente'] = H::db()->selectViewById('id_cliente', 'utente_cliente',
        $id_utente, 'id_utente')->listItemsByKey('id_cliente');
}

$id_cliente = H::input()->getArrayClean('id_cliente');
if ($id_cliente != null && count($id_cliente) > 0) {
    $params['e.id_cliente'] = $id_cliente;
}


///

//Logger::main()->log(print_r($params, true));

$list = Query::list_items_type_return($_,
    $list_filter,
    'e.*, e.recapito_ubicazione as ubicazione,
	t.nome as tipo_cartello_nome,
	c.nome as cliente_nome, c.export_id,
	n.nome as negozio_nome,
	p.nome as provincia_nome,
		f.nome as fornitore_nome',
    'cartello e,
		fornitore f,
	tipo_cartello t,
	cliente c,
	negozio n,
	provincia p',
    'cartello',
    'codice_int:asc',
    $order_auth,
    $params);
foreach ($list as &$item) {
    if ($item['recapito_ubicazione']) {
        $item['recapito_ubicazione'] = str_replace(["\r", "\n", "\t"], ' ', $item['recapito_ubicazione']);
    }
}
H::view('list', $list);
