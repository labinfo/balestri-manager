<?php
if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}
$id = H::input('id', 0);

$nuovo = $id <= 0;
$id_negozio = H::input('id_negozio', 0);
$latitudine = H::input('latitudine', '');
$longitudine = H::input('longitudine', '');

$params = [
	'id_negozio' => $id_negozio,
	'id_tipo_cartello' => H::input('id_tipo_cartello', 0), //def: NESSUN TIPO
	'codice' => H::input('codice', ''),
	'id_fornitore' => H::input('id_fornitore', 0), //def: NESSUN FORNITORE
	'recapito_via' => H::input('recapito_via', ''),
	'recapito_num_civico' => H::input('recapito_num_civico', ''),
	'recapito_cap' => H::input('recapito_cap', ''),
	'recapito_comune' => H::input('recapito_comune', ''),
	'recapito_id_provincia' => H::input('recapito_id_provincia', 0),
	'data_dal' => H::input()->getDate('data_dal', '0000-00-00'),
	'data_al' => H::input()->getDate('data_al', '0000-00-00'),
	'descrizione' => H::input('descrizione', ''),
	'recapito_latitudine' => $latitudine,
	'recapito_longitudine' => $longitudine,
	'recapito_ubicazione' => H::input('recapito_ubicazione', ''),
	'data_modifica:_' => 'now()',
	'num_contratto' => H::input('num_contratto', '')
];

H::log('i')->debug(json_encode(H::input()));
$abilitato = H::input('abilitato',0);
$data_disinstallazione = H::input()->getDate('data_disinstallazione','0000-00-00');
$disinstallato = H::input('disinstallato',0);

if ($disinstallato) {
    $params['abilitato'] = 1;
    $params['disinstallato'] = 1;
    if ($data_disinstallazione == '0000-00-00') {
        $params['data_disinstallazione'] == date('Y-m-d');
    }
    else {
        $params['data_disinstallazione'] = $data_disinstallazione;
    }
}
else {
    $params['abilitato'] = $abilitato;
    $params['disinstallato'] = 0;
    $params['data_disinstallazione'] = '0000-00-00';
}
H::log('c')->debug(json_encode($params));

if ($nuovo) {
	$id_cliente = H::input('id_cliente', -1);
	if($id_cliente < 0){
		H::hson()->error('Parametro id_cliente non valido');
		H::fire('view');
	}
	$params['id_cliente'] = $id_cliente;
	$params['data_inserimento:_'] = 'now()';
	$params['id_utente_creatore'] = H::session('user')['id'];

	$id = H::db()->insert('cartello', $params);

	$params = [
		'id_cliente' => $id_cliente,
	];
	$total_items = H::db()->queryCount('cartello', $params);
	$params = [
		'numero_cartelli' => $total_items,
	];
	H::db()->updateById('cliente', $params, $id_cliente);

	$params = [
		'id_negozio' => $id_negozio,
	];
	$total_items = H::db()->queryCount('cartello', $params);
	$params = [
		'numero_cartelli' => $total_items,
	];
	H::db()->updateById('negozio', $params, $id_negozio);

} else {
	H::db()->updateById('cartello', $params, $id);
}

H::context()->put('id', $id);
$data = [
	'latitudine' => $latitudine,
	'longitudine' => $longitudine,
	'id_negozio' => $id_negozio
];
Log::edit_update('cartello', $id, $nuovo, $data);
H::hson()->success('Dati salvati correttamente');
