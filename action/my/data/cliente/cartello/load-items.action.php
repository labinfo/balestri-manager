<?php
if ($_('input', false)) {
	$id = H::input('id');
}
else {
	$id = H::context('id');
}
if ($id != null) {
	if (!is_array($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'e.id_tipo_cartello:_' => 't.id',
		'e.recapito_id_provincia:_' => 'p.id',
		'e.id_negozio:_' => 'n.id',
		'e.id_cliente:_' => 'c.id',
		'e.id_fornitore:_' => 'f.id',
		'e.id' => $id,
	];
	$list = H::db()->selectView(	'e.*, e.recapito_ubicazione as ubicazione,
		t.nome as tipo_cartello_nome,
		c.nome as cliente_nome,
		n.nome as negozio_nome,
		p.nome as provincia_nome,
                f.nome as fornitore_nome',
		'cartello e,
		tipo_cartello t,
		cliente c,
		negozio n,
		provincia p,
                fornitore f',
		$params)->listItems('cartello');

	H::view('list', $list);
}