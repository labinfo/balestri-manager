<?php
if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}
$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	H::lib('Utils');
	
	$list = H::db()->selectById('cartello', $id)->listItems();
	H::db()->beginTransaction();
	Utils::delete_cartello($id);
	H::db()->commit();
	
	foreach ($list as $item) {
		$id_cliente = $item['id_cliente'];
		$id_negozio = $item['id_negozio'];
		
		$params = [
			'id_cliente' => $id_cliente,
		];
		$total_items = H::db()->queryCount('cartello', $params);
		$params = [
			'numero_cartelli' => $total_items,
		];
		H::db()->updateById('cliente', $params, $id_cliente);
		
		$params = [
			'id_negozio' => $id_negozio,
		];
		$total_items = H::db()->queryCount('cartello', $params);
		$params = [
			'numero_cartelli' => $total_items,
		];
		H::db()->updateById('negozio', $params, $id_negozio);
	}
	
	H::hson()->success('Eliminazione avvenuta correttamente');
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
