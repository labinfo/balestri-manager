<?php
H::lib('Query');

$order_auth = ['ip','stato','tipo_operazione','data_operazione','find_utente'];
$list_filter = ['ip','data_operazione', 'find_utente','stato', 'tipo_operazione','id_utente'];

Query::list_items($_,
	$list_filter,
	'log_utente',
	'data_operazione:desc',
	$order_auth,
	$params);