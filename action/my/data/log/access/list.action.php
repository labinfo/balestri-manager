<?php
H::lib('Query');

$order_auth = ['ip','tipo_operazione','data_operazione','find_utente','e_mail'];
$list_filter = ['ip','data_operazione', 'find_utente', 'tipo_operazione','id_utente'];

Query::list_items($_,
	$list_filter,
	'log_accesso',
	'data_operazione:desc',
	$order_auth,
	$params);