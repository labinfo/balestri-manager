<?php
$id = H::input('id', 0);

$row = H::db()->selectOne('richiesta_contatto', $id)->item();
if ($row) {
	$testo = H::input('testo', '');
	$oggetto = H::input('oggetto', '');
	
	$user = H::session('user');
	
	if (is_null($row['dettaglio'])) {
		$dettaglio = [];
	}
	else {
		$dettaglio = unserialize($row['dettaglio']);
	}
	if (!isset($dettaglio['elenco_risposte'])) {
		$dettaglio['elenco_risposte'] = [];
	}
	$dettaglio['elenco_risposte'][] = [
		'data_risposta' => date('Y-m-d H:i:s'),
		'oggetto' => $oggetto,
		'testo' => $testo,
		'mittente' => $user,
	];
	
	$params = [
		'dettaglio' => serialize($dettaglio),
		'numero_risposte:_' => 'numero_risposte + 1',
		'data_risposta:_' => 'now()'
	];
	H::db()->updateById('richiesta_contatto', $params, $id);
	
	Log::edit_response('richiesta_contatto', $id);
	
	$params = [
		'testo' => $testo
	];
	H::mail()->send($row['e_mail'], 
			$oggetto, 
			'risposta_contatto',
			$params);
	H::hson()->success('Messaggio inviato');
}
else {
	H::hson()->error('Richiesta non trovata in archivio');
}