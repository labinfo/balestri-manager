<?php
H::lib('Query');

$params = [];
if (H::input('con_risposta', '') != '') {
	$params['numero_risposte:>'] = 0;
}
if (H::input('senza_risposta', '') != '') {
	$params['numero_risposte'] = 0;
}

$order_auth = ['nome','cognome','e_mail','telefono','data_inserimento','data_risposta'];
$list_filter = ['eliminati', 'find', 'data_risposta', 'data_inserimento', 'stato'];

Query::list_items($_,
	$list_filter,
	'richiesta_contatto',
	'data_inserimento:desc',
	$order_auth,
	$params);