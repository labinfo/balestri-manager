<?php
H::lib('ImageManager');
$file = H::input()->getFile('file');
$php_info = pathinfo($file['name']);
$estensione = strtolower($php_info['extension']);
$exts = ["png", "jpg", "jpeg", "gif", "bmp"];
if (in_array($estensione, $exts)) {
    
    $params = [
        'data_inserimento:_' => 'now()',
        'estensione' => $estensione,
        'nome' => $file['name']
    ];
    $id = H::db()->insert('risorsa_file', $params);
    $nomeImmagine = "t_". $id . '.' . $estensione; // hanno un prefisso per distinguerle
                                                    //dagli id strutturati
    
    $filePath = HSystem::path('data/img', true) . '/' . $nomeImmagine;
    move_uploaded_file($file['tmp_name'], $filePath);

    $imageDim = ImageManager::$imageDim;
    
    HImage::createThumbs($filePath, $nomeImmagine, $imageDim, HSystem::path('img_thumbs', true));
    
    //View
    H::view('file', $nomeImmagine);
    $forza_inserimento = H::input('forza_inserimento', '');
    H::view('forza_inserimento', $forza_inserimento);
} else {
    H::hson()->error('Estensione non supportata');
}