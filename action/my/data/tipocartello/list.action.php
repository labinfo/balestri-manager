<?php
H::lib('Query');

$order_auth = ['nome'];
$list_filter = null;

$params = [
	'id:_>' => 0
];

Query::list_items($_,
	$list_filter,
	'tipo_cartello',
	'nome:asc',
	$order_auth, $params);