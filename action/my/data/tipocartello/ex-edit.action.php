<?php

if (H::session('user')['sola_lettura']) {
    Utils::auth_violation();
}
$id = H::input('id', 0);
$nome = H::input('nome');

$params = [
    'id' => $id,
    'nome' => $nome,
];
$res = H::db()->query('select id from tipo_cartello where upper(nome) = upper(:nome) and id != :id', $params);
if ($res->is()) {
    H::hson()->error('Valore già presente');
} else {
    $nuovo = $id <= 0;
    $codice = H::input('codice', '');
    $params = [
        'nome' => $nome,
        'icona' => H::input('filename', ''),
        'codice' => $codice
    ];
    if ($nuovo) {
        $id = H::db()->insert('tipo_cartello', $params);
    } else {
        H::db()->updateById('tipo_cartello', $params, $id);
    }

    H::context()->put('id', $id);
    Log::edit_update('tipo_cartello', $id, $nuovo);
    H::hson()->success('Dati salvati correttamente');
}
