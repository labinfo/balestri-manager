<?php
if (H::session('user')['sola_lettura']) {
	Utils::auth_violation();
}
$id = H::input('id', '');
if ($id != '') {
	$id = explode(',', $id);

	H::lib('Utils');
	$list_check = [
		'cartello' => 'id_tipo_cartello',
	];
	if (Utils::check_uso($id, $list_check)) {
		H::hson()->error('Voce in uso impossibile eliminare');
	}
	else {
		H::db()->deleteById('tipo_cartello', $id);
		Log::edit_delete_real('tipo_cartello', $id);	
		
		H::hson()->success('Eliminazione avvenuta correttamente');	
	}
}
else {
	H::hson()->error('Nessuna voce selezionata');
}
