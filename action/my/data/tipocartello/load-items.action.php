<?php
if ($_('input', false)) {
	$id = H::input('id');
}
else {
	$id = H::context('id');
}
if ($id != null) {
	if (!is_array($id)) {
		$id = explode(',', $id);
	}
	$params = [
		'id:>' => 0,
		'id' => $id,
	];
	$list = H::db()->select('tipo_cartello', $params)->listItems();

	H::view('list', $list);
}