<?php

//Ritorna la lista dei rivali presenti in base al profilo utente
// profilo >= 10 vede tutto
// profilo = 8 tutti per un cliente
// profilo = 6 solo alcuni

$user = getUser();
$profile = $user['profile'];
$params = [];
switch ($profile) {
    case 1000:
    case 10:
        break;
    case 9:
        $id_utente = $user['id'];
        $id_clienti = H::db()->selectViewById('id_cliente', 'utente_cliente',
            $id_utente, 'id_utente')->listItemsByKey('id_cliente');
        $params['c.id'] = getIdRivali($user['id'], $id_clienti);
        break;
    case 8:
        $params['c.id'] = getIdRivali($user['id'], $user['id_cliente']);
        break;
    case 6:
        $params['c.id'] = getIdRivali($user['id']);
        break;
}

$list = getRivali($params);

H::view('list', $list);
H::hson()->success("");

//////////////

function getIdRivali($id_utente, $id_cliente = null) {
    if ($id_cliente != null) {
        $id_negozi = H::db()->selectById('negozio', $id_cliente,
            'id_cliente')->listItemsByKey('id');
    } else {
        $id_negozi = getIdNegoziList($id_utente);
    }
    $params['id_negozio'] = $id_negozi;
    $list = H::db()->selectView('id_concorrente',
        'concorrente_negozio', $params)->listItemsByKey('id_concorrente');
    return $list;
}

function getRivali($params) {

    if (array_key_exists("c.id", $params)) {
        if (count($params['c.id']) == 0) {
            return [];
        }
    }
    $params['c.recapito_id_provincia:_'] = 'p.id';
    $params['c.id_cliente:_'] = 'cl.id';
    $params['c.id:_'] = 'cn.id_concorrente';

    filter($params);

    $list = H::db()->selectView('c.*, p.nome as recapito_provincia, cl.nome as cliente_nome',
        'concorrente c, provincia p, cliente cl, concorrente_negozio cn', $params)->listItems('concorrente');

    return $list;
}

function filter(&$params) {
    
    $id_cliente = H::input()->getArrayClean('id_cliente');
    if ($id_cliente != null && count($id_cliente) > 0) {
        $params['c.id_cliente'] = $id_cliente;
    }

    //concorrenti legati al filtro per negozi
    $id_negozio = H::input()->getArrayClean('id_negozio');
    if ($id_negozio != null && count($id_negozio) > 0) {
        $params['cn.id_negozio'] = $id_negozio;
    }
}

function getIdNegoziList($id_utente) {

    $list = H::db()->selectById('utente_negozio',
        $id_utente, 'id_utente')->listItemsByKey('id_negozio');
    return $list;
}

function getUser() {
    $id = H::session('user')['id'];
    $item = H::db()->selectOne('utente', $id)->item('utente');
    return $item;
}

function _rename_arr_key($oldkey, $newkey, array &$arr) {
    if (array_key_exists($oldkey, $arr)) {
        $arr[$newkey] = $arr[$oldkey];
        unset($arr[$oldkey]);
        return TRUE;
    } else {
        return FALSE;
    }
}
