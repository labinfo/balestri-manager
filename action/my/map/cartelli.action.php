<?php

//Ritorna la lista di cartelli presenti in base al profilo utente
// profilo >= 10 vede tutto
// profilo = 8 tutti per un cliente
// profilo = 6 solo alcuni

$user = getUser();
$profile = $user['profile'];
$params = [];
switch ($profile) {
	case 1000:
	case 10:
		break;
	case 9:
		$id_utente = $user['id'];
		$params['c.id_cliente'] = H::db()->selectViewById('id_cliente', 'utente_cliente',
			$id_utente, 'id_utente')->listItemsByKey('id_cliente');
		break;
	case 8:
		$params['c.id_cliente'] = $user['id_cliente'];
		break;
	case 6:
		$params['c.id_cliente'] = $user['id_cliente'];
		$params['c.id_negozio'] = getIdNegoziList($user['id']);
		break;
}

$list = getCartelli($params, $profile);

H::view('list', $list);
H::hson()->success("");

//////////////

function getCartelli($params, $profile)
{
	$params['c.id_tipo_cartello:_'] = 'tc.id';
	$params['c.id_cliente:_'] = 'cl.id';
	$params['c.recapito_id_provincia:_'] = 'p.id';
//    if (!array_key_exists("c.id_negozio", $params)) {
	$params['c.id_negozio:_'] = 'n.id';
//    }
	$tables = 'cartello c, tipo_cartello tc, negozio n, cliente cl, provincia p';
	$selection = 'c.*, tc.nome as tipo_cartello_nome,
					tc.icona as tipo_cartello_icona,
					n.nome as negozio_nome,
					cl.nome as cliente_nome,
					p.nome as recapito_provincia';
	if($profile > 8){
		$tables .= ', fornitore f';
		$selection .= ', f.nome as fornitore_nome';
		$params['c.id_fornitore:_'] = 'f.id';
	}
	filter($params);
	$list = H::db()->selectView($selection,
					$tables,
					$params)->listItems('cartello_c');
	return $list;
}

function filter(&$params)
{
	$id_cliente = H::input()->getArrayClean('id_cliente');
	if ($id_cliente != null && count($id_cliente) > 0) {
		$params['c.id_cliente'] = $id_cliente;
	}

	$id_tipo_cartello = H::input()->getArrayClean('id_tipo_cartello');
	if ($id_tipo_cartello != null && count($id_tipo_cartello) > 0) {
		$params['c.id_tipo_cartello'] = $id_tipo_cartello;
	}
	$id_fornitore = H::input()->getArrayClean('id_fornitore');
	if ($id_fornitore != null && count($id_fornitore) > 0) {
		$params['c.id_fornitore'] = $id_fornitore;
	}

	$id_negozio = H::input()->getArrayClean('id_negozio');
	if ($id_negozio != null && count($id_negozio) > 0) {
		$params['c.id_negozio'] = $id_negozio;
	}

	$data_dal = H::input('data_inizio_dal', '');
	$data_al = H::input('data_inizio_al', '');
	if ($data_dal != '') {
		$params['c.data_dal:>='] = $data_dal;
	}
	if ($data_al != '') {
		$params['c.data_dal:<='] = $data_al;
	}

	$data_dal = H::input('data_fine_dal', '');
	$data_al = H::input('data_fine_al', '');
	if ($data_dal != '') {
		$params['c.data_al:>='] = $data_dal;
	}
	if ($data_al != '') {
		$params['c.data_al:<='] = $data_al;
	}

	if (H::input('abilitati', '') != '') {
		$params['c.abilitato'] = 1;
	}
	if (H::input('non_abilitati', '') != '') {
		$params['c.abilitato'] = 0;
	}
}

function getIdNegoziList($id)
{
	$list = H::db()->selectById('utente_negozio', $id, 'id_utente')->listItemsByKey('id_negozio');
	return $list;
}

function getUser()
{
	$id = H::session('user')['id'];
	$item = H::db()->selectOne('utente', $id)->item('utente');
	return $item;
}

function _rename_arr_key($oldkey, $newkey, array &$arr)
{
	if (array_key_exists($oldkey, $arr)) {
		$arr[$newkey] = $arr[$oldkey];
		unset($arr[$oldkey]);
		return TRUE;
	} else {
		return FALSE;
	}
}
