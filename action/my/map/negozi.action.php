<?php

//Ritorna la lista di negozi presenti in base al profilo utente
// profilo >= 10 vede tutto
// profilo = 8 tutti per un cliente
// profilo = 6 solo alcuni

$user = getUser();
$profile = $user['profile'];
$params = [];
$single_cliente = false;
switch ($profile) {
    case 1000:
    case 10:
        break;
    case 9:
        $id_utente = $user['id'];
        $params['n.id_cliente'] = H::db()->selectViewById('id_cliente', 'utente_cliente',
            $id_utente, 'id_utente')->listItemsByKey('id_cliente');
        break;
    case 8:
        $params['n.id_cliente'] = $user['id_cliente'];
        $single_cliente = true;
        break;
    case 6:
        $params['n.id_cliente'] = $user['id_cliente'];
        $params['n.id'] = getIdNegoziList($user['id']);
        $single_cliente = true;
        break;
}

        
$list = getNegozi($params);

H::view('list', $list);

H::hson()->success("");

//////////////

function getNegozi($params) {

    $params['n.recapito_id_provincia:_'] = 'p.id';
    $params['n.id_cliente:_'] = 'c.id';

    filter($params);

    $list = H::db()->selectView('n.*, p.nome as recapito_provincia, c.nome as cliente_nome',
        'negozio n, provincia p, cliente c', $params)->listItems('negozio');

    return $list;
}

function filter(&$params) {

    $id_negozio = H::input()->getArrayClean('id_negozio');
    if ($id_negozio != null && count($id_negozio) > 0) {
        $params['n.id'] = $id_negozio;
    }
    
    $id_cliente = H::input()->getArrayClean('id_cliente');
    if ($id_cliente != null && count($id_cliente) > 0) {
        $params['n.id_cliente'] = $id_cliente;
    }
}

function getIdNegoziList($id) {
    $list = H::db()->selectById('utente_negozio', $id, 'id_utente')->listItemsByKey('id_negozio');
    return $list;
}

function getUser() {
    $id = H::session('user')['id'];
    $item = H::db()->selectOne('utente', $id)->item('utente');
    return $item;
}

function _rename_arr_key($oldkey, $newkey, array &$arr) {
    if (array_key_exists($oldkey, $arr)) {
        $arr[$newkey] = $arr[$oldkey];
        unset($arr[$oldkey]);
        return TRUE;
    } else {
        return FALSE;
    }
}
