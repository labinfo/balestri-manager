<?php

$name = H::input('template', 'dashboard/index');
if (HString::startsWith('_', $name)) {
    $name = 'data_not_found';
}
$params = [];
$userId = H::session('user')['id'];

switch ($name) {
    case 'account/index':
    case 'account/edit':
    case 'mappa/index':
    case 'export/edit':
    case 'cliente/cartello/index_all':
    case 'cliente/cartello/index':
        $orderBy = 'nome asc';
        $params_fornitore = [];
        $params['list_fornitore'] = H::db()->select('fornitore', $params_fornitore, $orderBy)->listItems();
        $params_tipocartello = [
            'id:_>' => 0
        ];
        $params['list_tipocartello'] = H::db()->select('tipo_cartello', $params_tipocartello, $orderBy)->listItems();

        if(H::session('user')['profile'] == 9){
            $id_utente = H::session('user')['id'];
            $params_cliente = [
                'uc.id_cliente:_' => 'c.id',
                'uc.id_utente' => $id_utente
            ];
            $params['list_cliente'] = H::db()->selectView('c.*',
                'cliente c, utente_cliente uc',
                $params_cliente, $orderBy)->listItems();
        }else{
            $params_cliente = [
                'id:_>' => 0
            ];
            $params['list_cliente'] = H::db()->select('cliente',
                $params_cliente, $orderBy)->listItems();
        }

        $params_provincia = [
            'id:_>' => 0
        ];
        $params['list_provincia'] = H::db()->select('provincia', $params_provincia, $orderBy)->listItems();
        //per negozio
        $id_cliente = H::input('id_cliente', H::session('user')['id_cliente']);
        $params_negozio = [];
        $profile = H::session('user')['profile'];

        if($profile == 9){
            $id_utente = H::session('user')['id'];
            $params_negozio = [
                'uc.id_cliente:_' => 'n.id_cliente',
                'uc.id_utente' => $id_utente
            ];
            $params['list_negozio'] = H::db()->selectView('n.*',
                'negozio n, utente_cliente uc', $params_negozio, $orderBy)->listItems();
        }else{
            if ($profile == 6) {
                $list = H::db()->selectById('utente_negozio',
                    H::session('user')['id'], 'id_utente')->listItemsByKey('id_negozio');
                $params['id'] = $list;
                $params_negozio['id'] = $list;
                $params['list_negozio'] = H::db()->select('negozio', $params_negozio, $orderBy)->listItems();
            }
            else {
                if ($id_cliente > 0) {
                    //case for not _all
                    $params_negozio['id_cliente'] = $id_cliente;
                }
                $params['list_negozio'] = H::db()->select('negozio', $params_negozio, $orderBy)->listItems();
                $params['id_cliente'] = H::session('user')['id_cliente'];
            }
        }
        break;
    case 'cliente/concorrente/edit':
        $params_provincia = [
            'id:_>' => 0
        ];
        $params['list_provincia'] = H::db()->select('provincia', $params_provincia, $orderBy)->listItems();
    case 'cliente/account/edit':
        $orderBy = 'nome asc';
        $params_negozio = [
            'id_cliente' => H::input('id_cliente', 0)
        ];
        $params['id_cliente'] = $params_negozio['id_cliente'];
        $params['list_negozio'] = H::db()->select('negozio', $params_negozio, $orderBy)->listItems();
//        H::lib('Logger');
//        Logger::main()->log(print_r($params, true));
        break;
    case 'cliente/cartello/edit':
        $orderBy = 'nome asc';
        $id_cliente = H::input('id_cliente', 0);
        $params_negozio = [];
        if ($id_cliente > 0) {
            $params_negozio['id_cliente'] = $id_cliente;
        }
        $params_fornitore = array();
        $params['list_fornitore'] = H::db()->select('fornitore', $params_fornitore, $orderBy)->listItems();
        $params['list_negozio'] = H::db()->select('negozio', $params_negozio, $orderBy)->listItems();
        $params_provincia = [
            'id:_>' => 0
        ];
        $params['list_provincia'] = H::db()->select('provincia', $params_provincia, $orderBy)->listItems();
        $params_tipo = null;
        $params['list_tipo_cartello'] = H::db()->select('tipo_cartello', $params_tipo, $orderBy)->listItems();
        break;
    case 'cliente/negozio/edit':
        $orderBy = 'nome asc';
        $params_provincia = [
            'id:_>' => 0
        ];
        $params['list_provincia'] = H::db()->select('provincia', $params_provincia, $orderBy)->listItems();
        break;
}
$data = [
    'params' => $params,
    'template' => [
        'my/template/header',
        'my/template/' . $name,
        'my/template/footer'
    ]
];
H::call('template', $data);
