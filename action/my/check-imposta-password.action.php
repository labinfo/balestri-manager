<?php
if (H::session()->isAuth()) {
	if (H::session('user')['imposta_password']) {
		H::redirect('imposta-password');
	}
}
else {
	H::redirect('index');
}