<?php
H::lib('Utils');
if (Utils::check_download() == null) {
	H::hson()->error('Risorsa non disponibile o accesso non autorizzato');
}