<?php
if (H::session()->checkProfile('super')) {
	H::session()->invalidate();
}
elseif (H::session()->isAuth()) {
	$user = H::session('user');
	if (isset($user['ricordami']) && !$user['ricordami']) {
		H::session()->invalidate();
	}
	else {
		H::redirect('my/index');
	}
}