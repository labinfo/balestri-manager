<?php
$nome = H::input('nome', '');
$cognome = H::input('cognome', '');
$e_mail = strtolower(H::input('e_mail', ''));

$find = [
	$nome,
	$cognome,
	$e_mail,
];
H::lib('Utils');
$find = Utils::find_string($find);

$params_contatto = [
	'find' => $find,
	'nome' => $nome,
	'cognome' => $cognome,
	'e_mail' => $e_mail,
	'telefono' => H::input('telefono', ''),
	'testo' => H::input('testo', ''),
	'preferenza_contatto' => 0,
	'data_inserimento:_' => 'now()',
];
			
$id = H::db()->insert('richiesta_contatto', $params_contatto);

$params = [
	'profile' => 10
];
$destinatari = H::db()->select('utente', $params)->listItemsByKey('e_mail');
if (count($destinatari) > 0) {
	$params = [
		'stato' => 0
	];
	$num = H::db()->queryCount('richiesta_contatto', $params, 'id');
	
	$params = [
		'num_richieste_contatto_nuovo' => $num
	];
	$where = [
		'profile' => 10
	];
	H::db()->update('utente', $params, $where);

	H::mail()->send($destinatari, 
			'Hai ricevuto una richiesta di contatto', 
			'richiesta_contatto', 
			$params_contatto);

	Log::edit_update('richiesta_contatto', $id, true);	
}