<?php
if ($_('invalidate-session', false)) {
	H::session()->invalidate();
	$session_fault = true;
	H::view('session_fault', $session_fault);
}
if (H::input('hson') != '' || H::context('sessionid_input') != '' ||
	H::context('request_service') != '') {
	H::fire('complete-hson');
}