<?php
H::session()->invalidate();

$ref_id = H::input('ref_id', '');
$ref_token = H::input('ref_token', '');

$c = new HCryptNumber();
$id = $c->decode($ref_id);

if ($id != null) {
	$params = [
		'abilitato' => 1,
		'id' => $id
	];
	$row = H::db()->select('utente', $params)->item();
	if ($row) {
		if ($ref_token == $row['codice_recupero_password'] &&
			strtotime('-1 days ' . $row['data_recupero_password']) < time()) {
			H::view('ref_token', $ref_token);
			H::view('ref_id', $ref_id);
		}
	}
}