<?php
H::session()->invalidate();

$e_mail = strtolower(H::input('e_mail', ''));

if ($e_mail == 'superadmin') {
	H::fire('view');
}

$ref_id = H::input('ref_id', '');
$ref_token = H::input('ref_token', '');

$c = new HCryptNumber();
$id = $c->decode($ref_id);
$message = null;
if ($id == null) {
	$message = 'Parametri non validi';
}
else {
	$params = [
		'id' => $id,
		'abilitato' => 1,
	];
	$row = H::db()->
			select('utente', $params)->
			item();
	$id = 0;
	if ($row) {
		$id = $row['id'];
		if ($ref_token == $row['codice_recupero_password'] && $e_mail == $row['e_mail']) {
			if (strtotime('-1 days ' . $row['data_recupero_password']) < time()) {
				$password = H::input('password');
				$conferma_password = H::input('conferma_password');
				if ($password == $conferma_password) { 
					H::lib('Utils');
					if (Utils::check_password($password)) {
						$nome = $row['nome'];

						H::lib('Utente');
						H::db()->beginTransaction();
						Utente::imposta_password($id, $password);
						H::db()->commit();

						Log::edit('reset_password',
							'utente',
							$id);
						
						$params = [
								'nome' => $nome,
							];
						H::mail()->send($e_mail, 'password modificata', 'password_modificata', $params);
						$message = null;
					}
					else {
						$message = 'La password deve essere lunga almeno 8 caratteri, contenere almeno una lettera maiuscola, una lettera minuscolae un numero';
					}
				}
				else {
					$message = 'Conferma password non corrisponde';
				}
			}
			else {
				$message = 'Link/codice scaduto, hai tempo 24 ore dopo la richiesta di recupero password per effettuare la modifica';
			}
		}
		else {
			$message = 'Parametri non validi o utente disabilitato o non presente in archivio';
		}
	}
	else {
		$message = 'Parametri non validi o utente disabilitato o non presente in archivio';
	}
}
if ($message != null) {
	Log::error('reset_password',
		'utente',
		$id,
		$message);
}
H::hson()->error($message);