<?php

$id = H::input('id', 0);

if($id > 0){

    $id_cliente = H::input('id_cliente', 0);

    $params = [];

    if($id_cliente){
        $params['id_cliente'] = $id_cliente;
    }

    H::db()->updateById('utente', $params, $id);
}

H::hson()->success('utente modificato');