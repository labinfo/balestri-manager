<?php

H::lib('Logger');
H::session()->invalidate();

$e_mail = strtolower(H::input('email', ''));

$message = 'credenziali non valide o utente disabilitato';
$password = H::input('password', '');
$sessionId = H::input('sessionid', '');

$params = [
	'abilitato' => 1,
	'e_mail' => $e_mail
];

$row = H::db()->
	selectById('utente', $params)->
	item();

if ($row) {
	//fetch utente
	$tentativi_accesso = $row['tentativi_accesso'];
	$profile = $row['profile'];
	$id = $row['id'];
	$imposta_password = $row['imposta_password'];
	$id_cliente = $row['id_cliente'];

	$user = [
		'id' => $id,
		'imposta_password' => $imposta_password,
		'profile' => $profile,
		'e_mail' => $row['e_mail'],
		'nome' => $row['nome'],
		'cognome' => $row['cognome'],
		'id_cliente' => $id_cliente,
		'sola_lettura' => $row['sola_lettura'] == 1 ? true : false
	];

	//Controllo della password
	$row = H::db()->
		selectById('utente_accesso_check', $id, 'id_utente')->
		item();

	if ($row) {
		$password = sha1($id . $password . $row['check_string']);
		$row = H::db()->
			selectById('utente_accesso', $id, 'id_utente')->
			item();
		if ($row) {
			if ($e_mail == 'superadmin') {
				H::hson()->error('Impossibile accedere con superadmin');
				return;
			} else {
				$password_check = $row['password'];
			}
			if ($password == $password_check) {

				H::session()->newSession('utente', true);
				H::session()->put('user', $user);
				$id = $user['id'];
				H::db()->beginTransaction();
				$params = [
					'tentativi_accesso' => 0,
					'ultimo_accesso:_' => 'now()',
					'codice_recupero_password' => null,
				];
				H::db()->updateById('utente', $params, $id);

				$params = [
					'data_inserimento:_' => 'now()',
					'id_utente' => $id,
					'sessionid' => H::session()->sessionId(),
				];

				H::db()->insert('utente_session', $params);
				H::db()->commit();

				Log::accesso('login', $id);
				Log::info('login', 'utente', $id, '', $id);
				$message = null;
			}
		}
	}
}

if ($message != null) {
	H::hson()->error($message);
}
