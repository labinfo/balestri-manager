<?php
if ($_('invalidate-session', false)) {
	H::session()->invalidate();
	$session_fault = true;
	H::view('session_fault', $session_fault);
}
H::hson()->error($_('message', ''));