<?php

$type = H::input('type', $_('type', ''));
$id_entita = H::input('entity_id', -1);
$userId = H::session('user')['id'];

$params = [
    'id_utente' => $userId,
//    'tipo' => $type,
//    'id_entita' => $id_entita
];

if ($type != '') {
    $params['tipo'] = $type;
}
if ($id_entita > -1) {
    $params['id_entita'] = $id_entita;
}

if ($_('by_key', false)) {
    $list = H::db()->selectView('id_entita, tipo',
        'entita_preferito', $params)->listItemsByKey('id_entita');
} else {

    $list = H::db()->selectView('id_entita, tipo',
        'entita_preferito', $params)->listItems();
}

$view_key_name = $_('view_key_name', 'list');
H::view($view_key_name, $list);
