<?php
$id_cartello = H::input('id', 0);
if ($id_cartello > 0) {
    $id_utente = H::session('user')['id'];

    $params = [
        'id' => $id_cartello,
        'id_utente_creatore' => $id_utente,
        'abilitato:_' => 0
    ];

    $res = H::db()->select('cartello', $params);
    if (!$res->is()) {
        H::hson()->error('L\'utente non ha i permessi per modificare il cartello');
        H::fire('view');
    }
}