<?php
$id = H::input('id');
$params = [
    'id:_' => $id
];
$row = H::db()->select('cartello', $params)->item();
$action_params = [
    'type' => 'cartello'
];
$input_params = [
    'id' => $id,
    'file' => H::input()->getFile('file')
];
if($row['foto'] == null){
    $action_params['name'] = 'foto';
}
else{
    $action_params['name'] = 'gallery';
}
$params = [
    '_input' => $input_params,
    '_params' => $action_params
];
H::call('my/data/ex-image', $params);
