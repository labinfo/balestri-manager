<?php

$id = H::input('id', 0);
$true = true;

if ($id > 0) {
    $item = H::db()->selectOne('cartello', $id)->item();
    if ($item) {
        if ($item['abilitato'] != 0) {
            H::view('billboard_locked', $true);
            H::hson()->error('Impossibile modificare un cartello non temporaneo.');
            H::fire('view');
        }
        $data_modifica = H::input()->getDateTime('data_modifica', false);
        if ($data_modifica) {
            $t1 = strtotime($data_modifica);
            if($item['data_modifica']){
                $t2 = strtotime($item['data_modifica']);
            } else {
                $t2 = strtotime($item['data_inserimento']);
            }
            if($t2 > $t1){
                H::view('billboard_more_recent', $true);
                H::hson()->success();
                H::fire('view');
            }
        }
    } else {
        H::view('billboard_not_found', $true);
        H::hson()->error('Cartello non esistente.');
        H::fire('view');
    }
}