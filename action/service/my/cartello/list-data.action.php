<?php

$user = H::session('user');
if ($user != null) {
	H::lib('ServiceView');
	$sola_lettura = $user['sola_lettura'];
	$bill_types = ServiceView::get_billboard_types();
	$providers = ServiceView::get_providers();
	$customers = ServiceView::get_customers($user);
	H::view('sola_lettura', $sola_lettura);
	H::view('id_utente', $user['id']);
	H::view('tipi_cartello', $bill_types);
	H::view('fornitori', $providers);
	H::view('clienti', $customers);
}

