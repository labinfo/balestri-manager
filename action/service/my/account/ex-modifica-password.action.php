<?php

$id = H::session('user')['id'];
$password = H::input('password_new', '');
$message = null;
$row = H::db()->selectOne('partecipante_accesso_check', $id, 'id_partecipante')->item();
if ($row) {
    $old_password = H::input('password_old', '');
    $old_password = sha1($id . $old_password . $row['check_string']);

    $row = H::db()->selectById('partecipante_accesso', $id, 'id_partecipante')->item();
    $message = 'Vecchia password non valida';
    if ($row) {
        if ($old_password == $row['password']) {
            H::lib('Partecipante');
            H::db()->beginTransaction();
            Partecipante::imposta_password($id, $password);
            H::db()->commit();

            H::lib('Log');
            Log::edit('update_password_partecipante', 'partecipante', $id);
            $message = null;
            H::hson()->success('Password modificata correttamente');
            return;
        }
    }
}


H::hson()->error($message);
