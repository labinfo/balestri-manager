<?php

$type = H::input('type', 'cartello');
$id_entita = H::input('entity_id', -1);
$fav = H::input('value', 'false');
$fav = $fav == 'false' ? false : true;

$userId = H::session('user')['id'];

$params = [
    'id_utente' => $userId,
    'tipo' => $type,
    'id_entita' => $id_entita
];
$row = H::db()->select('entita_preferito', $params)->item();
if ($fav && !$row) {
    //insert
    H::db()->insert('entita_preferito', $params);
    H::hson()->success('aggiunto ai preferiti');
    H::view('value', $fav);
}
if (!$fav && $row) {
    H::db()->delete('entita_preferito', $params);
    H::hson()->success('rimosso dai preferiti');
    H::view('value', $fav);
}

H::view('id', $id_entita);
H::view('type', $type);
