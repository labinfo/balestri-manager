<?php
H::session()->invalidate();

$e_mail = strtolower(H::input('e_mail', ''));

H::lib('Utils');
$id = 0;
$max_tentativi_accesso = H::settings('*')->get('max_tentativi_accesso', 5);

$ip = HUtil::get_client_ip();

$res = H::db()->selectById('blocco_ip', $ip, 'ip');

if ($res->is() || $ip == 'UNKNOWN') {
    $message = 'IP bloccato all\'accesso al sistema';
} else {
    $message = 'credenziali non valide o utente disabilitato';
    $password = H::input('password', '');

    $params = [
        'profile:>=' => 1,
        'id:>' => 0,
        'abilitato' => 1,
        'e_mail' => $e_mail
    ];
    $row = H::db()->
    selectById('utente',
        $params)->
    item();

    if ($row) {
        H::lib('Profile');

        $tentativi_accesso = $row['tentativi_accesso'];
        $profile = $row['profile'];
        $id = $row['id'];
        $imposta_password = $row['imposta_password'];
        $profile_name = Profile::toName($profile);
        $id_cliente = $row['id_cliente'];

        $user = [
            'id' => $id,
            'imposta_password' => $imposta_password,
            'profile' => $profile,
            'e_mail' => $row['e_mail'],
            'nome' => $row['nome'],
            'cognome' => $row['cognome'],
            'id_cliente' => $id_cliente,
            'sola_lettura' => $row['sola_lettura'] == 1 ? true : false
        ];

        $procedi = true;
        switch ($profile) {
            case 1:
            case 2:
            case 6:
            case 8:
            case 9:
            case 10:
            case 1000:
                break;
            default:
                $procedi = false;
                $message = 'Accesso non autorizzato';
        }

        if ($procedi && $profile_name != '' &&
            ($e_mail == 'superadmin' || $tentativi_accesso < $max_tentativi_accesso)
        ) {
            if ($id_cliente > 0) {
                $row = H::db()->selectById('cliente', $id_cliente)->item();
                if ($row) {
                    $procedi = $row['abilitato'];
                } else {
                    $procedi = false;
                    $message = 'Accesso non autorizzato';
                }
            }
            if ($procedi) {
                $row = H::db()->selectById('utente_accesso_check', $id,
                    'id_utente')->item();

                if ($row) {
                    $password = sha1($id . $password . $row['check_string']);
                    $row = H::db()->selectById('utente_accesso', $id,
                        'id_utente')->item();

                    if ($row) {
                        if ($e_mail == 'superadmin') {
                            $password_check = H::config('password_super', '');
                        } else {
                            $password_check = $row['password'];
                        }
                        if ($password == $password_check) {
                            $login = true;
                            H::lib('Profile');
                            if (Profile::is($profile, 'super') && $e_mail != 'superadmin') {
                                $login = false;
                            }
                            if ($login) {
                                if (H::context('request_service', false)) {
                                    $ricordami = true;
                                } else {
                                    $ricordami = H::input('ricordami', '') == '' ? false : true;
                                }
                                $user['ricordami'] = $ricordami;
                                H::session()->newSession($profile_name, $ricordami);
                                H::session()->put('user', $user);
                                H::db()->beginTransaction();
                                $params = [
                                    'tentativi_accesso' => 0,
                                    'ultimo_accesso:_' => 'now()',
                                    'codice_recupero_password' => null,
                                ];
                                H::db()->updateById('utente',
                                    $params,
                                    $id);

                                $params = [
                                    'data_inserimento:_' => 'now()',
                                    'id_utente' => $id,
                                    'sessionid' => H::session()->sessionId(),
                                ];
                                H::db()->insert('utente_session', $params);
                                H::db()->commit();

                                $open = H::input('open', '');
                                if ($open != '') {
                                    $open = H::config('url') .
                                        $open;
                                    H::view('redirect_to_url', $open);
                                }

                                Log::accesso('login', $id);
                                Log::info('login',
                                    'utente',
                                    $id,
                                    '',
                                    $id);
                                $message = null;
                            }
                        }
                    }
                }
            }
        }
        if ($message != null) {
            Log::accesso('login_err', $id);
            $tentativi_accesso++;

            $params = [
                'tentativi_accesso:_' => 'tentativi_accesso + 1',
                'ultimo_tentativo:_' => 'now()'
            ];
            H::db()->updateById('utente',
                $params,
                $id);

            if ($tentativi_accesso == $max_tentativi_accesso) {
                $dest = H::settings('*')->get('email_notifiche_accessi', '');
                $params = [
                    'messaggio' => 'Sono stati riscontrati più tentativi di accesso non valido consecutivi dall\'account dell\'utente:',
                    'utente' => $user
                ];
                H::mail()->send($dest,
                    'tentativo di accesso',
                    'notifica_super_accesso',
                    $params);
            }
        }
    }
}

if ($message != null) {
    Log::error('login',
        'utente',
        $id,
        $message);
}
H::hson()->error($message);