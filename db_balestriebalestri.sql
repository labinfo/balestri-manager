CREATE TABLE IF NOT EXISTS `log_risorsa` (
	`id_risorsa` int unsigned NOT NULL,
	`tipo_risorsa` varchar(50) NOT NULL,
	`tipo_operazione` varchar(50) NOT NULL,
	`data_operazione` datetime NOT NULL default '0000-00-00 00:00:00',
	`id_utente` int unsigned NOT NULL,
	`utente` text NULL,
	`ip` varchar(50) NOT NULL,
	`ultimo` tinyint NOT NULL default false,
	INDEX (`ip`),
	INDEX (`id_risorsa`),
	INDEX (`tipo_risorsa`),
	INDEX (`tipo_operazione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `log_utente` (
	`stato` int unsigned NOT NULL,
	`id_risorsa` int unsigned NOT NULL,
	`tipo_risorsa` varchar(50) NOT NULL,
	`tipo_operazione` varchar(50) NOT NULL,
	`data_operazione` datetime NOT NULL default '0000-00-00 00:00:00',
	`find_utente` varchar(500) NULL,
	`id_utente` int unsigned NOT NULL,
	`utente` text NULL,
	`ip` varchar(50) NOT NULL,
	`request_activity` varchar(200) NOT NULL,
	`dettaglio` text NULL,
	INDEX (`ip`),
	INDEX (`id_risorsa`),
	INDEX (`tipo_risorsa`),
	INDEX (`tipo_operazione`),
	INDEX (`request_activity`),
	INDEX (`stato`),
	INDEX (`data_operazione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `log_accesso` (
	`tipo_operazione` varchar(50) NOT NULL,
	`data_operazione` datetime NOT NULL default '0000-00-00 00:00:00',
	`id_utente` int unsigned NOT NULL,
	`e_mail` varchar(150) NOT NULL,
	`find_utente` varchar(500) NULL,
	`utente` text NULL,
	`ip` varchar(50) NOT NULL,
	INDEX (`ip`),
	INDEX (`id_utente`),
	INDEX (`e_mail`),
	INDEX (`tipo_operazione`),
	INDEX (`data_operazione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `blocco_ip` (
	`ip` varchar(50) NOT NULL,
	`data_inserimento` datetime NOT NULL default '0000-00-00 00:00:00',
	INDEX(`data_inserimento`),
	PRIMARY KEY  (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `utente` (
	`id` int unsigned NOT NULL auto_increment,
	`find` varchar(500) NULL,
	`abilitato` tinyint NOT NULL default false,
	`imposta_password` tinyint NOT NULL default false,
	`e_mail` varchar(150) NOT NULL,
	`nome` varchar(100) NOT NULL,
	`cognome` varchar(100) NULL,
	`logo` varchar(100) NULL,
	`profile` int unsigned NOT NULL,
	`codice_recupero_password` varchar(100) NULL,
	`data_recupero_password` datetime NULL default '0000-00-00 00:00:00',
	`tentativi_accesso` int unsigned NOT NULL default 0,
	`ultimo_tentativo` datetime NULL default '0000-00-00 00:00:00',
	`ultimo_accesso` datetime NULL default '0000-00-00 00:00:00',
	`data_inserimento` datetime NULL default '0000-00-00 00:00:00',
	`num_richieste_contatto_nuovo` int unsigned NOT NULL default 0,
	`id_cliente` int unsigned NOT NULL default 0,
	`sola_lettura` TINYINT(4) NOT NULL DEFAULT 1,
	INDEX (`e_mail`),
	INDEX (`nome`),
	INDEX (`cognome`),
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `utente_accesso` (
	`id_utente` int unsigned NOT NULL,
	`password` varchar(100) NOT NULL,
	PRIMARY KEY  (`id_utente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `utente_accesso_check` (
	`id_utente` int unsigned NOT NULL,
	`check_string` varchar(50) NOT NULL,
	PRIMARY KEY  (`id_utente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `utente_session` (
	`id_utente` int unsigned NOT NULL,
	`sessionid` varchar(250) NULL,
	`data_inserimento` datetime NOT NULL default '0000-00-00 00:00:00',
	PRIMARY KEY  (`id_utente`,`sessionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `risorsa_file` (
	`id` int unsigned NOT NULL auto_increment,
	`estensione` varchar(10) NOT NULL,
	`nome` varchar(300) NOT NULL,
	`data_inserimento` datetime NOT NULL default '0000-00-00 00:00:00',
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `risorsa_immagine` (
	`id` int unsigned NOT NULL auto_increment,
	`estensione` varchar(10) NOT NULL,
	`nome` varchar(300) NOT NULL,
	`data_inserimento` datetime NOT NULL default '0000-00-00 00:00:00',
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `richiesta_contatto` (
	`id` int unsigned NOT NULL auto_increment,
	`eliminato` tinyint NOT NULL default false,
	`find` varchar(500) NULL,
	`e_mail` varchar(150) NOT NULL,
	`telefono` varchar(50) NOT NULL,
	`nome` varchar(150) NOT NULL,
	`cognome` varchar(150) NOT NULL,
	`preferenza_contatto` int unsigned NOT NULL default 0,
	`testo` text NULL,
	`dettaglio` longtext NULL,
	`stato` int unsigned NOT NULL default 0,
	`numero_risposte` int unsigned NOT NULL default 0,
	`data_inserimento` datetime NOT NULL default '0000-00-00 00:00:00',
	`data_risposta` datetime NOT NULL default '0000-00-00 00:00:00',
	INDEX(`nome`),
	INDEX(`cognome`),
	INDEX(`e_mail`),
	INDEX(`data_inserimento`),
	INDEX(`data_risposta`),
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `cliente` (
	`id` int unsigned NOT NULL auto_increment,
	`abilitato` tinyint NOT NULL default false,
	`nome` varchar(100) NOT NULL,
	`numero_cartelli` int unsigned NOT NULL default 0,
	`logo` varchar(100) NOT NULL,
	`num_contratto` VARCHAR(200) NULL,
	INDEX (`nome`),
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `provincia` (
	`id` int unsigned NOT NULL,
	`nome` varchar(100) NOT NULL,
	INDEX (`nome`),
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `negozio` (
	`id` int unsigned NOT NULL auto_increment,
	`id_cliente` int unsigned NOT NULL,
	`nome` varchar(100) NOT NULL,
	`descrizione` longtext NULL,
	`numero_cartelli` int unsigned NOT NULL default 0,
	`recapito_latitudine` float(10,7) NOT NULL default 0,
	`recapito_longitudine` float(10,7) NOT NULL default 0,
	`logo` VARCHAR(100) NULL DEFAULT NULL AFTER `recapito_longitudine`,
 `recapito_via` VARCHAR(100) NOT NULL AFTER `logo`,
 `recapito_num_civico` VARCHAR(10) NOT NULL AFTER `recapito_via`,
 `recapito_cap` VARCHAR(10) NOT NULL AFTER `recapito_num_civico`,
 `recapito_comune` VARCHAR(100) NOT NULL AFTER `recapito_cap`,
 `recapito_id_provincia` INT(10) UNSIGNED NOT NULL AFTER `recapito_comune`,

	FOREIGN KEY (id_cliente) REFERENCES cliente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (id_provincia) REFERENCES provincia(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `tipo_cartello` (
	`id` int unsigned NOT NULL auto_increment,
	`nome` varchar(100) NOT NULL,
	`icona` VARCHAR(300) NOT NULL DEFAULT '',
	`codice` VARCHAR(300) NOT NULL DEFAULT '',
	INDEX (`nome`),
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `cartello` (
	`id` int unsigned NOT NULL auto_increment,
	`abilitato` tinyint NOT NULL default false,
	`id_cliente` int unsigned NOT NULL,
	`id_negozio` int unsigned NOT NULL,
	`id_tipo_cartello` int unsigned NOT NULL,
	`data_inserimento` datetime NOT NULL default '0000-00-00 00:00:00',
	`data_modifica` datetime NOT NULL default '0000-00-00 00:00:00',
	`data_dal` date NOT NULL default '0000-00-00',
	`data_al` date NOT NULL default '0000-00-00',
	`codice` varchar(50) NOT NULL,
	`descrizione` longtext NULL,
	`foto` varchar(100) NULL,
	`gallery` longtext NULL,
	`recapito_latitudine` float(10,7) NOT NULL default 0,
	`recapito_longitudine` float(10,7) NOT NULL default 0,
	`recapito_via` varchar(100) NOT NULL,
	`recapito_num_civico` varchar(10) NOT NULL,
	`recapito_cap` varchar(10) NOT NULL,
	`recapito_comune` varchar(100) NOT NULL,
	`recapito_id_provincia` int unsigned NOT NULL,
	`recapito_ubicazione` TEXT NOT NULL,
	`id_fornitore` INT(10) UNSIGNED NOT NULL,
	`num_contratto` VARCHAR(200) NOT NULL,
	INDEX(`codice`),
	FOREIGN KEY (id_tipo_cartello) REFERENCES tipo_cartello(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (id_cliente) REFERENCES cliente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (id_negozio) REFERENCES negozio(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (recapito_id_provincia) REFERENCES provincia(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (`id_fornitore`) REFERENCES `fornitore` (`id`)  ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `concorrente` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `descrizione` LONGTEXT NULL DEFAULT NULL,
  `recapito_latitudine` FLOAT(10,7) NOT NULL DEFAULT '0',
  `recapito_longitudine` FLOAT(10,7) NOT NULL DEFAULT '0',
  `id_cliente` INT(10) UNSIGNED NOT NULL,
  `logo` VARCHAR(100) NULL DEFAULT NULL,
  `recapito_via` VARCHAR(100) NOT NULL ,
`recapito_num_civico` VARCHAR(10) NOT NULL ,
`recapito_cap` VARCHAR(10) NOT NULL ,
`recapito_comune` VARCHAR(100) NOT NULL ,
`recapito_id_provincia` INT(10) UNSIGNED NOT NULL,
  FOREIGN KEY (id_cliente) REFERENCES cliente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY (`id`));

  CREATE TABLE `concorrente_negozio` (
  `id_concorrente` INT(10) UNSIGNED NOT NULL,
  `id_negozio` INT(10) UNSIGNED NOT NULL,
    FOREIGN KEY (`id_concorrente`)
    REFERENCES `concorrente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (`id_negozio`)
    REFERENCES `negozio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
  
CREATE TABLE IF NOT EXISTS `entita_gallery` (
	`id` int unsigned NOT NULL,
	`id_entita` int unsigned NOT NULL,
	`tipo_entita` varchar(50) NOT NULL,
	`ordine` int unsigned NOT NULL default 0,
	`immagine` varchar(100) NULL,
	INDEX(`id_entita`),
	INDEX(`tipo_entita`),
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fornitore` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `descrizione` LONGTEXT NULL,
  PRIMARY KEY (`id`));
							
CREATE TABLE IF NOT EXISTS `entita_documento` (
	`id` int unsigned NOT NULL auto_increment,
	`abilitato` tinyint NOT NULL default false,
	`id_risorsa_file` int unsigned NOT NULL,
	`id_entita` int unsigned NOT NULL,
	`tipo_entita` varchar(50) NOT NULL,
	`commento` varchar(500) NULL,
	FOREIGN KEY (id_risorsa_file) REFERENCES risorsa_file(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `utente_cliente` (
	`id_utente` int unsigned NOT NULL,
	`id_cliente` int unsigned NOT NULL,
	FOREIGN KEY (id_cliente) REFERENCES cliente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (id_utente) REFERENCES utente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY  (`id_utente`,`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `utente_negozio` (
	`id_utente` int unsigned NOT NULL,
	`id_negozio` int unsigned NOT NULL,
	FOREIGN KEY (id_negozio) REFERENCES negozio(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (id_utente) REFERENCES utente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY  (`id_utente`,`id_negozio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*CREATE TABLE IF NOT EXISTS `cartello_preferito` (
	`id_utente` int unsigned NOT NULL,
	`id_cartello` int unsigned NOT NULL,
	FOREIGN KEY (id_cartello) REFERENCES cartello(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY (id_utente) REFERENCES utente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY  (`id_utente`,`id_cartello`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/

CREATE TABLE IF NOT EXISTS `entita_preferito` (
  `id_utente` INT(10) UNSIGNED NOT NULL,
  `id_entita` INT(10) UNSIGNED NOT NULL,
  `tipo` VARCHAR(50) NOT NULL,
  FOREIGN KEY (id_utente) REFERENCES utente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY (`id_utente`, `id_entita`, `tipo`));

insert into provincia set id = 1, nome = 'Chieti';
insert into provincia set id = 2, nome = 'L''Aquila';
insert into provincia set id = 3, nome = 'Pescara';
insert into provincia set id = 4, nome = 'Teramo';
insert into provincia set id = 5, nome = 'Matera';
insert into provincia set id = 6, nome = 'Potenza';
insert into provincia set id = 7, nome = 'Catanzaro';
insert into provincia set id = 8, nome = 'Cosenza';
insert into provincia set id = 9, nome = 'Crotone';
insert into provincia set id = 10, nome = 'Reggio Calabria';
insert into provincia set id = 11, nome = 'Vibo Valentia';
insert into provincia set id = 12, nome = 'Avellino';
insert into provincia set id = 13, nome = 'Benevento';
insert into provincia set id = 14, nome = 'Caserta';
insert into provincia set id = 15, nome = 'Napoli';
insert into provincia set id = 16, nome = 'Salerno';
insert into provincia set id = 17, nome = 'Bologna';
insert into provincia set id = 18, nome = 'Ferrara';
insert into provincia set id = 19, nome = 'Forlì-Cesena';
insert into provincia set id = 20, nome = 'Modena';
insert into provincia set id = 21, nome = 'Parma';
insert into provincia set id = 22, nome = 'Piacenza';
insert into provincia set id = 23, nome = 'Ravenna';
insert into provincia set id = 24, nome = 'Reggio Emilia';
insert into provincia set id = 25, nome = 'Rimini';
insert into provincia set id = 26, nome = 'Gorizia';
insert into provincia set id = 27, nome = 'Pordenone';
insert into provincia set id = 28, nome = 'Trieste';
insert into provincia set id = 29, nome = 'Udine';
insert into provincia set id = 30, nome = 'Frosinone';
insert into provincia set id = 31, nome = 'Latina';
insert into provincia set id = 32, nome = 'Rieti';
insert into provincia set id = 33, nome = 'Roma';
insert into provincia set id = 34, nome = 'Viterbo';
insert into provincia set id = 35, nome = 'Genova';
insert into provincia set id = 36, nome = 'Imperia';
insert into provincia set id = 37, nome = 'La Spezia';
insert into provincia set id = 38, nome = 'Savona';
insert into provincia set id = 39, nome = 'Bergamo';
insert into provincia set id = 40, nome = 'Brescia';
insert into provincia set id = 41, nome = 'Como';
insert into provincia set id = 42, nome = 'Cremona';
insert into provincia set id = 43, nome = 'Lecco';
insert into provincia set id = 44, nome = 'Lodi';
insert into provincia set id = 45, nome = 'Mantova';
insert into provincia set id = 46, nome = 'Milano';
insert into provincia set id = 47, nome = 'Monza e della Brianza';
insert into provincia set id = 48, nome = 'Pavia';
insert into provincia set id = 49, nome = 'Sondrio';
insert into provincia set id = 50, nome = 'Varese';
insert into provincia set id = 51, nome = 'Ancona';
insert into provincia set id = 52, nome = 'Ascoli Piceno';
insert into provincia set id = 53, nome = 'Fermo';
insert into provincia set id = 54, nome = 'Macerata';
insert into provincia set id = 55, nome = 'Pesaro e Urbino';
insert into provincia set id = 56, nome = 'Campobasso';
insert into provincia set id = 57, nome = 'Isernia';
insert into provincia set id = 58, nome = 'Alessandria';
insert into provincia set id = 59, nome = 'Asti';
insert into provincia set id = 60, nome = 'Biella';
insert into provincia set id = 61, nome = 'Cuneo';
insert into provincia set id = 62, nome = 'Novara';
insert into provincia set id = 63, nome = 'Torino';
insert into provincia set id = 64, nome = 'Verbano-Cusio-Ossola';
insert into provincia set id = 65, nome = 'Vercelli';
insert into provincia set id = 66, nome = 'Bari';
insert into provincia set id = 67, nome = 'Barletta-Andria-Trani';
insert into provincia set id = 68, nome = 'Brindisi';
insert into provincia set id = 69, nome = 'Foggia';
insert into provincia set id = 70, nome = 'Lecce';
insert into provincia set id = 71, nome = 'Taranto';
insert into provincia set id = 72, nome = 'Cagliari';
insert into provincia set id = 73, nome = 'Carbonia-Iglesias';
insert into provincia set id = 74, nome = 'Medio Campidano';
insert into provincia set id = 75, nome = 'Nuoro';
insert into provincia set id = 76, nome = 'Ogliastra';
insert into provincia set id = 77, nome = 'Olbia-Tempio';
insert into provincia set id = 78, nome = 'Oristano';
insert into provincia set id = 79, nome = 'Sassari';
insert into provincia set id = 80, nome = 'Agrigento';
insert into provincia set id = 81, nome = 'Caltanissetta';
insert into provincia set id = 82, nome = 'Catania';
insert into provincia set id = 83, nome = 'Enna';
insert into provincia set id = 84, nome = 'Messina';
insert into provincia set id = 85, nome = 'Palermo';
insert into provincia set id = 86, nome = 'Ragusa';
insert into provincia set id = 87, nome = 'Siracusa';
insert into provincia set id = 88, nome = 'Trapani';
insert into provincia set id = 89, nome = 'Bolzano';
insert into provincia set id = 90, nome = 'Trento';
insert into provincia set id = 91, nome = 'Arezzo';
insert into provincia set id = 92, nome = 'Firenze';
insert into provincia set id = 93, nome = 'Grosseto';
insert into provincia set id = 94, nome = 'Livorno';
insert into provincia set id = 95, nome = 'Lucca';
insert into provincia set id = 96, nome = 'Massa-Carrara';
insert into provincia set id = 97, nome = 'Pisa';
insert into provincia set id = 98, nome = 'Pistoia';
insert into provincia set id = 99, nome = 'Prato';
insert into provincia set id = 100, nome = 'Siena';
insert into provincia set id = 101, nome = 'Perugia';
insert into provincia set id = 102, nome = 'Terni';
insert into provincia set id = 103, nome = 'Aosta';
insert into provincia set id = 104, nome = 'Belluno';
insert into provincia set id = 105, nome = 'Padova';
insert into provincia set id = 106, nome = 'Rovigo';
insert into provincia set id = 107, nome = 'Treviso';
insert into provincia set id = 108, nome = 'Venezia';
insert into provincia set id = 109, nome = 'Verona';
insert into provincia set id = 110, nome = 'Vicenza';

INSERT INTO utente SET abilitato = true, 
	e_mail = 'superadmin', nome = 'Super', cognome = 'Amministratore',
	profile = 1000, data_inserimento = now(), sola_lettura = 0;
INSERT INTO utente_accesso SET id_utente = 1, password = '-';
INSERT INTO utente_accesso_check SET id_utente = 1, check_string = '1';

INSERT INTO utente SET abilitato = true, 
	e_mail = 'admin@balestriebalestri.it', nome = 'Amministratore', cognome = 'Sistema',
	profile = 10, data_inserimento = now();
INSERT INTO utente_accesso SET id_utente = 2, password = sha1('2adminasivwOIoiw23084JLgKBgt6EWKseyf');
INSERT INTO utente_accesso_check SET id_utente = 2, check_string = 'asivwOIoiw23084JLgKBgt6EWKseyf';

INSERT INTO `fornitore` (`id`, `nome`, `descrizione`) VALUES ('0', 'NESSUN FORNITORE', '');
