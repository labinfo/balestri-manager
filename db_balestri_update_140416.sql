-- fix cliente

ALTER TABLE `cliente` 
CHANGE COLUMN `export_id` `export_id` CHAR(7) NULL DEFAULT NULL ;

delete from cliente where id > 0 and numero_cartelli = 0;
update cliente set export_id = null where id > 0;
update cliente set export_id = id where id > 0;


-- structure


ALTER TABLE `cliente` 
CHANGE COLUMN `export_id` `export_id` CHAR(7) NULL DEFAULT NULL ,
ADD UNIQUE INDEX `unique_id_export` (`export_id` ASC);

alter table log_risorsa add column dati TEXT NULL DEFAULT NULL;
